// $(document).ready(function () {
//
//     /********************/
//     // script for student
//     /********************/
//
//     // script for student => header
//
//     $(document).on("click", ".headerStudent .user", function () {
//         $(".headerStudent .menuNotification").removeClass("active");
//         $(".headerStudent .menuUser").toggleClass("active");
//     })
//
//     $(document).on("click", ".headerStudent .notification", function () {
//         $(".headerStudent .menuUser").removeClass("active");
//         $(".headerStudent .menuNotification").toggleClass("active");
//     })
//
//     $(document).mousedown(function (e) {
//         var container = $(".headerStudent .menuUser");
//         if (!container.is(e.target) && container.has(e.target).length === 0) {
//             $(".headerStudent .menuUser").removeClass("none");
//         }
//     });
//
//     $(document).mousedown(function (e) {
//         var container = $(".headerStudent .menuNotification");
//         if (!container.is(e.target) && container.has(e.target).length === 0) {
//             $(".headerStudent .menuNotification").removeClass("none");
//         }
//     });
//
//     $(document).on("click", ".sideRightStudent .keyList", function () {
//         $(".sideRightStudent .listSideRight").slideToggle(400)
//     })
//
//     $(document).on("click", ".headerStudent .menuMobile .icon", function () {
//         $(".sideRightStudent").toggleClass("active");
//     })
//
//     // script for student => contentStudent
//     $(document).on("click", ".contentStudent .textInfo label", function () {
//         $(this).siblings("input").removeAttr("disabled");
//         $(this).siblings("select").removeAttr("disabled");
//         $(this).siblings("textarea").removeAttr("disabled");
//         $(".contentStudent .secInfo .btnsForm").removeClass("d-none");
//     })
//
//     $(document).mousedown(function (e) {
//         var container = $(".contentStudent .textInfo .info label, .contentStudent .textInfo .info input, .contentStudent .textInfo .info textarea, .contentStudent .textInfo .info .select2-search__field, .contentStudent .textInfo .info .select2, .contentStudent .secInfo .btnsForm, .contentStudent .educationSelect2");
//         if (!container.is(e.target) && container.has(e.target).length === 0) {
//             container.siblings("input").attr("disabled", "disabled");
//             container.siblings("select").attr("disabled", "disabled");
//             container.siblings("textarea").attr("disabled", "disabled");
//         }
//     });
//
//     $(document).on("click", ".contentStudent .secInfo .btnsForm>*", function () {
//         $(".contentStudent .secInfo .btnsForm").addClass("d-none");
//         $($(".contentStudent .btnEdit").parents(".title")).removeClass("barEdit");
//         $($(".contentStudent .btnEdit").parents(".box")).removeClass("boxEdit");
//         $($($(".contentStudent .btnEdit").parents(".title")).siblings(".show")).addClass("active");
//         $($($(".contentStudent .btnEdit").parents(".title")).siblings(".edit")).removeClass("active");
//     })
//
//     $(document).on("click", ".contentStudent .btnEdit", function () {
//         $($(this).parents(".title")).addClass("barEdit");
//         $($(this).parents(".box")).addClass("boxEdit");
//         $($($(this).parents(".title")).siblings(".show")).removeClass("active");
//         $($($(this).parents(".title")).siblings(".edit")).addClass("active");
//         $(".btnsForm").removeClass("d-none");
//     })
//
//     // $(document).on("click", ".contentStudent .btnsForm>*", function () {
//     //     $($(this).parents(".title")).removeClass("barEdit");
//     //     $($(this).parents(".box")).removeClass("boxEdit");
//     //     $($($(this).parents(".title")).siblings(".show")).addClass("active");
//     //     $($($(this).parents(".title")).siblings(".edit")).removeClass("active");
//     // })
//
//     $(document).on("click", ".contentStudent .nameEducation i", function () {
//         // $($(this).parents(".nameEducation")).remove();
//     })
//
//     // script for student => completeDataStudent
//
//     for (var i = 0; i < $(".completeDataStudent .input[requiredX]").length; i++) {
//         $($($(".completeDataStudent .input[requiredX]")[i]).parent(".divInput")).after('<small class="error"></small>');
//     }
//
//     function testData(xl) {
//         if ($(xl).attr("requiredX") == "yes") {
//             if (xl.nodeName.toLowerCase() == "input" && $(xl).attr("type") != "radio" && $(xl).attr(
//                 "type") != "checkbox") {
//                 if ($(xl).val().length == 0) {
//                     $(xl).addClass("error").removeClass("correct");
//                     $($($(xl).parent(".divInput")).next("small.error")).show();
//                     $($($(xl).parent(".divInput")).next("small.error")).text($(xl).attr("errorEmpty"));
//                     $(xl).attr("true", "no");
//                 } else if ($(xl).val().length < $(xl).attr("min")) {
//                     if ($(xl).attr("min")) {
//                         $(xl).addClass("error").removeClass("correct");
//                         $($($(xl).parent(".divInput")).next("small.error")).show();
//                         $($($(xl).parent(".divInput")).next("small.error")).text($(xl).attr(
//                             "errorMin"));
//                         $(xl).attr("true", "no");
//                     }
//                 } else if ($(xl).val().length > $(xl).attr("max")) {
//                     if ($(xl).attr("max")) {
//                         $(xl).addClass("error").removeClass("correct");
//                         $($($(xl).parent(".divInput")).next("small.error")).show();
//                         $($($(xl).parent(".divInput")).next("small.error")).text($(xl).attr(
//                             "errorMax"));
//                         $(xl).attr("true", "no");
//                     }
//                 } else {
//                     $(xl).addClass("correct").removeClass("error");
//                     $($($(xl).parent(".divInput")).next("small.error")).hide();
//                     $($($(xl).parent(".divInput")).next("small.error")).text();
//                     $(xl).attr("true", "yes");
//                 }
//             } else if (xl.nodeName.toLowerCase() == "select") {
//                 if ($(xl).val() == 0) {
//                     $(xl).addClass("error").removeClass("correct");
//                     $($($(xl).parent(".divInput")).next("small.error")).show();
//                     $($($(xl).parent(".divInput")).next("small.error")).text($(xl).attr("errorEmpty"));
//                     $(xl).attr("true", "no");
//                 } else if ($(xl).val() != 0) {
//                     $(xl).addClass("correct").removeClass("error");
//                     $($($(xl).parent(".divInput")).next("small.error")).hide();
//                     $($($(xl).parent(".divInput")).next("small.error")).text();
//                     $(xl).attr("true", "yes");
//                 }
//             }
//
//             if ($(xl).attr("type") == "email") {
//                 if (IsEmail($(xl).val()) == false) {
//                     $(xl).addClass("error").removeClass("correct");
//                     $($($(xl).parent(".divInput")).next("small.error")).show();
//                     $($($(xl).parent(".divInput")).next("small.error")).text($(xl).attr("errorType"));
//                     $(xl).attr("true", "no");
//                     return false;
//                 } else {
//                     $(xl).addClass("correct").removeClass("error");
//                     $($($(xl).parent(".divInput")).next("small.error")).hide();
//                     $($($(xl).parent(".divInput")).next("small.error")).text();
//                     $(xl).attr("true", "yes");
//                     return true;
//                 }
//             }
//
//             if ($(xl).attr("type") == "checkbox") {
//                 if ($(xl).is(':checked')) {
//                     $(xl).attr("true", "yes");
//                     $("[name='" + $(xl).attr("name") + "']").addClass("correct").removeClass("error");
//                     $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .hide();
//                     $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .text();
//                 } else {
//                     $(xl).attr("true", "no");
//                     $("[name='" + $(xl).attr("name") + "']").addClass("error").removeClass("correct");
//                     $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .show();
//                     $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .text($(xl).attr("errorEmpty"));
//                 }
//             }
//
//             if ($(xl).attr("type") == "radio") {
//                 if ($("[name='" + $(xl).attr("name") + "']:checked").length == 1) {
//                     $("[name='" + $(xl).attr("name") + "']").attr("true", "yes");
//                     $("[name='" + $(xl).attr("name") + "']").addClass("correct").removeClass("error");
//                     $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .hide();
//                     $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .text();
//                 } else {
//                     $("[name='" + $(xl).attr("name") + "']").attr("true", "no");
//                     $("[name='" + $(xl).attr("name") + "']").addClass("error").removeClass("correct");
//                     $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .show();
//                     $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .text($(xl).attr("errorEmpty"));
//                 }
//             }
//
//         } else {
//             $(xl).attr("true", "yes")
//         }
//     }
//
//     setTimeout(function () {
//         for (var i = 0; i < $($(".completeDataStudent .input")).length; i++) {
//             if (($(".completeDataStudent .input")[i]).nodeName.toLowerCase() == "input" && $($(".completeDataStudent .input")[i]).attr(
//                 "type") != "radio" && $($(".completeDataStudent .input")[i]).attr("type") != "checkbox") {
//                 if ($($(".completeDataStudent .input")[i]).val() != "" && $($(".completeDataStudent .input")[i]).val() != 0) {
//                     $($(".completeDataStudent .input")[i]).trigger("keyup")
//                 }
//             }
//         }
//
//         for (var i = 0; i < $($(".completeDataStudent .input")).length; i++) {
//             if (($(".completeDataStudent .input")[i]).nodeName.toLowerCase() == "input" && $($(".completeDataStudent .input")[i]).attr(
//                 "type") == "checkbox") {
//                 $($(".completeDataStudent .input")[i]).trigger("keyup")
//             }
//         }
//     }, 500)
//
//     $(document).on("click", ".submit", function () {
//         $($($(this).parents("form")).find("input:not(#video), select, textarea")).trigger("change")
//         $($($(this).parents("form")).find("input, select, textarea")).trigger("keydown")
//         $($($(this).parents("form")).find("input, select, textarea")).trigger("keyup")
//         $($($(this).parents("form")).find("input, select, textarea")).trigger("blur")
//         for (var a = 0; a < 15; a++) {
//             testData(xl = $($(this).parents("form").find(".input"))[a])
//             testData(xl = $($(this).parents(".formPublic").find("input"))[a])
//         }
//     })
//
//     $(document).on("keyup change blur", ".completeDataStudent .input[requiredX]:not(#phone2)", function () {
//         testData(xl = this)
//     })
//
//     $(document).on("change keyup", ".completeDataStudent  .input:not(#phone2)", function () {
//         for (var i = 0; i < $(".completeDataStudent .input:not(#phone2)").length; i++) {
//             if ($($(".completeDataStudent .input:not(#phone2)")[i]).attr("true") != "yes") {
//                 x++;
//             }
//             if (i == $(".completeDataStudent .input:not(#phone2)").length - 1) {
//
//                 if (x != 0 && phoneStep2 == 0) {
//                     $($(".completeDataStudent").find(".submit")).attr("type", "button");
//                 } else if (x == 0 && phoneStep2 == 1) {
//                     $($(".completeDataStudent").find(".submit")).attr("type", "submit");
//                 }
//                 testData(xl = this)
//                 x = 0;
//             }
//         }
//     })
//
//     var phoneStep2 = 0;
//     var valid205 = 0;
//
//     $(document).on("keyup", "#phone2", function () {
//
//         if ($(this).val().split("")[0] == 0 && $(this).val().split("")[1] == 5) {
//             $(this).addClass("correct").removeClass("error");
//             $($($(this).parent(".divInput")).next("small.error")).hide();
//             $($($(this).parent(".divInput")).next("small.error")).text();
//             $(this).attr("true", "yes");
//             valid205 = 1;
//         } else {
//             $(this).addClass("error").removeClass("correct");
//             $($($(this).parent(".divInput")).next("small.error")).show();
//             $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("error05"));
//             $(this).attr("true", "no");
//             phoneStep2 = 0;
//             valid205 = 0;
//         }
//
//         if (($(this).val().length == 10) && ($(this).val().split("")[0] == 0) && ($(this).val().split("")[1] == 5)) {
//             $(this).addClass("correct").removeClass("error");
//             $($($(this).parent(".divInput")).next("small.error")).hide();
//             $($($(this).parent(".divInput")).next("small.error")).text();
//             $(this).attr("true", "yes");
//             phoneStep2 = 1;
//         } else {
//             if (valid205 == 1) {
//                 $(this).addClass("error").removeClass("correct");
//                 $($($(this).parent(".divInput")).next("small.error")).show();
//                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMin"));
//                 $(this).attr("true", "no");
//             } else {
//                 $(this).addClass("error").removeClass("correct");
//                 $($($(this).parent(".divInput")).next("small.error")).show();
//                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("error05"));
//                 $(this).attr("true", "no");
//             }
//             phoneStep2 = 0;
//         }
//
//         for (var i = 0; i < $(".completeDataStudent .input").length; i++) {
//             if ($($(".completeDataStudent .input")[i]).attr("true") != "yes") {
//                 x++;
//             }
//             if (i == $(".completeDataStudent .input").length - 1) {
//
//                 if (x != 0 && phoneStep2 == 0) {
//                     $($(".completeDataStudent").find(".submit").remove()).attr("type", "button");
//
//                 } else if (x == 0 && phoneStep2 == 1) {
//                     $($(".completeDataStudent").find(".submit")).attr("type", "submit");
//                     // var element = document.getElementById("step1");
//                     // element.classList.remove("submit");
//                 }
//                 testData(xl = this)
//                 x = 0;
//             }
//         }
//
//         for (var i = 0; i < $(".formPublic input").length; i++) {
//             if ($($(".formPublic input")[i]).attr("true") != "yes") {
//                 x++;
//             }
//             if (i == $(".formPublic input[requiredX]").length - 1) {
//
//                 if (x != 0 && phoneStep2 == 0) {
//                     $($(".formPublic").find(".submit")).attr("type", "button");
//                 } else if (x == 0 && phoneStep2 == 1) {
//                     $($(".formPublic").find(".submit")).attr("type", "submit");
//                 }
//                 testData(xl = this)
//                 x = 0;
//             }
//         }
//
//     })
//
//     var x = 0;
//
//     for (var i = 0; i < $(".formPublic input").length; i++) {
//
//         if ($($(".formPublic input")[i]).attr("requiredX") == "yes") {
//             $($(".formPublic input")[i]).attr("true", "no");
//         } else {
//             $($(".formPublic input")[i]).attr("true", "yes");
//         }
//     };
//
//     $(document).on("keyup change blur", ".formPublic input[requiredX]:not(#phone2)", function () {
//         testData(xl = this)
//     })
//
//     $(document).on("change keyup", ".formPublic  input", function () {
//         for (var i = 0; i < $(".formPublic input").length; i++) {
//             if ($($(".formPublic input")[i]).attr("true") != "yes") {
//                 x++;
//             }
//             if (i == $(".formPublic input[requiredX]").length - 1) {
//
//                 if (x != 0 && phoneStep2 == 0) {
//                     $($(".formPublic").find(".submit")).attr("type", "submit");
//                 } else if (x == 0 && phoneStep2 == 1) {
//                     $($(".formPublic").find(".submit")).attr("type", "submit");
//                 }
//                 testData(xl = this)
//                 x = 0;
//             }
//         }
//     })
//
//     for (var i = 0; i < $(".formPublic input[requiredX]").length; i++) {
//         $($($(".formPublic input[requiredX]")[i]).parent(".divInput")).after('<small class="error"></small>');
//     }
//
//     // ************
//
//
//
//     // ************
//
//     // for (var i = 0; i < $(".formPublic .input").length; i++) {
//
//     //     if ($($(".formPublic .input")[i]).attr("requiredX") == "yes") {
//     //         $($(".formPublic .input")[i]).attr("true", "no");
//     //     } else {
//     //         $($(".formPublic .input")[i]).attr("true", "yes");
//     //     }
//     // };
//
//     // for (var i = 0; i < $(".formPublic .input[requiredX]").length; i++) {
//     //     $($($(".formPublic .input[requiredX]")[i]).parent(".divInput")).after('<small class="error"></small>');
//     // }
//
//     // $(document).on("change keyup", ".formPublic  .input", function () {
//     //     for (var i = 0; i < $(".formPublic .input").length; i++) {
//     //         if ($($(".formPublic .input")[i]).attr("true") != "yes") {
//     //             x++;
//     //         }
//     //         if (i == $(".formPublic .input").length - 1) {
//
//     //             if (x != 0) {
//     //                 $($(".formPublic").find(".submit")).attr("type", "button");
//     //             } else if (x == 0) {
//     //                 $($(".formPublic").find(".submit")).attr("type", "submit");
//     //             }
//     //             testData(xl = this)
//     //             console.log(x)
//     //             x = 0;
//     //         }
//     //     }
//     // })
//
//     $(document).on("change", ".imgFile", function () {
//         readURL(this)
//     })
//
//     function readURL(input) {
//         if (input.files && input.files[0]) {
//             var reader = new FileReader();
//             reader.onload = function (e) {
//                 $('img.imgFile').attr('src', e.target.result);
//             };
//             reader.readAsDataURL(input.files[0]);
//         }
//     }
//
//     /********************/
//     // script for teacher
//     /********************/
//
//     // script for teacher => header
//
//     $(document).on("click", ".headerTeacher .user", function () {
//         $(".headerTeacher .menuNotification").removeClass("active");
//         $(".headerTeacher .menuUser").toggleClass("active");
//     })
//
//     $(document).on("click", ".headerTeacher .notification", function () {
//         $(".headerTeacher .menuUser").removeClass("active");
//         $(".headerTeacher .menuNotification").toggleClass("active");
//     })
//
//     $(document).mousedown(function (e) {
//         var container = $(".headerTeacher .menuUser");
//         if (!container.is(e.target) && container.has(e.target).length === 0) {
//             $(".headerTeacher .menuUser").removeClass("none");
//         }
//     });
//
//     $(document).mousedown(function (e) {
//         var container = $(".headerTeacher .menuNotification");
//         if (!container.is(e.target) && container.has(e.target).length === 0) {
//             $(".headerTeacher .menuNotification").removeClass("none");
//         }
//     });
//
//     // script for teacher => alert
//
//     $(document).on("click", ".textAlertData", function () {
//         if ($(window).outerWidth(true) <= 991.9) {
//             $(".over").addClass("active");
//         }
//     })
//
//     $(document).mousedown(function (e) {
//         var container = $(".alertUser");
//         if (!container.is(e.target) && container.has(e.target).length === 0) {
//             if ($(window).outerWidth(true) <= 991.9) {
//                 setTimeout(function () {
//                     $(".alertUser").removeClass("active");
//                 }, 200)
//             } else {
//                 $(".alertUser").removeClass("active");
//             }
//         }
//     });
//
//     $(document).on("click", ".over.active,.over.show", function () {
//         setTimeout(function () {
//             $(".alertUser").removeClass("active");
//             $(".over").removeClass("active show");
//         }, 200)
//     })
//
//     // script for teacher => content
//
//     $(document).on("click", ".contentTeacher .keyList", function () {
//         $(".sideRightTeacher .listSideRight").slideToggle(400)
//     })
//
//     $(document).on("click", ".headerTeacher .menuMobile .icon", function () {
//         $(".sideRightTeacher").toggleClass("active");
//     })
//
//     $(document).on("click", ".contentTeacher .textInfo label", function () {
//         $(this).siblings("input").removeAttr("disabled");
//         $(this).siblings("textarea").removeAttr("disabled");
//     })
//
//     $(document).mousedown(function (e) {
//         var container = $(".contentTeacher .textInfo label, .contentTeacher .textInfo input, .contentTeacher .textInfo textarea");
//         if (!container.is(e.target) && container.has(e.target).length === 0) {
//             container.siblings("input").attr("disabled", "disabled");
//             container.siblings("textarea").attr("disabled", "disabled");
//         }
//     });
//
//     $(document).on("click", ".contentTeacher .btnShowHide.less", function () {
//         $($(this).next("p.less")).removeClass("less").addClass("more");
//         $(this).removeClass("less").addClass("more");
//     })
//
//     $(document).on("click", ".contentTeacher .btnShowHide.more", function () {
//         $($(this).next("p.more")).removeClass("more").addClass("less");
//         $(this).removeClass("more").addClass("less");
//     })
//
//     var myVideo = document.getElementById("video1");
//
//     function playPause() {
//         if (myVideo.paused) {
//             myVideo.play();
//         } else {
//             myVideo.pause();
//         }
//     }
//
//     $(document).on("click", ".contentTeacher #video1", function () {
//         playPause();
//     })
//
//     $(document).on("click", ".contentTeacher .iconPlay", function () {
//         $(this).hide();
//         $(".contentTeacher .coverVideo").hide();
//         playPause();
//     })
//
//     $(document).on("click", ".contentTeacher .btnEdit", function () {
//         $($(this).parents(".title")).addClass("barEdit");
//         $($(this).parents(".box")).addClass("boxEdit");
//         $($($(this).parents(".title")).siblings(".show")).removeClass("active");
//         $($($(this).parents(".title")).siblings(".edit")).addClass("active");
//     })
//
//     $(document).on("click", ".contentTeacher .btns>*", function () {
//         $($(this).parents(".title")).removeClass("barEdit");
//         $($(this).parents(".box")).removeClass("boxEdit");
//         $($($(this).parents(".title")).siblings(".show")).addClass("active");
//         $($($(this).parents(".title")).siblings(".edit")).removeClass("active");
//     })
//
//
//     var i = 2;
//     var eduHtml = $(".dataTeacher .edit .eduCol").html();
//
//     $(document).on("click", ".dataTeacher .addEduCol>a", function () {
//         $(".dataTeacher .eduCol").append(eduHtml.replaceAll('edu1-01', ('edu' + i + '-01')).replaceAll('edu1-02', ('edu' + i + '-02')).replaceAll('edu1-03', ('edu' + i + '-03')))
//         i++;
//     })
//
//
//     // new delete
//     $(document).on("click", ".completeDataTeacher .Delete", function () {
//         $(this).parents(".rowTitleTeacher").remove()
//         console.log($(this).parents(".rowTitleTeacher"))
//     })
//
//     $(document).on("click", ".completeDataTeacher .addSubjectBtn", function () {
//         $(".popUpAddSubject").addClass("show")
//     })
//
//     $(document).on("click", ".completeDataTeacher .noActive input", function () {
//         if ($(this).is(':checked')) {
//             $(".completeDataTeacher td." + $(this).attr("id") + " input").attr("disabled", "disabled");
//         } else {
//             $(".completeDataTeacher td." + $(this).attr("id") + " input").removeAttr("disabled");
//         }
//     })
//
//     $(document).on("click", ".completeDataTeacher .addHours td span", function () {
//         $('<tr class="rowFrom">' + $(".rowFrom").html() + '</tr>').insertBefore(".completeDataTeacher tr.addHours");
//         $('<tr class="rowTo">' + $(".rowTo").html() + '</tr>').insertBefore(".completeDataTeacher tr.addHours");
//         // rowFrom();
//         // rowTo();
//     })
//
//     // new img
//
//     $(document).on("change", ".imgFile", function () {
//         readURL(this)
//     })
//
//     // change img
//     function readURL(input) {
//         if (input.files && input.files[0]) {
//             var reader = new FileReader();
//             reader.onload = function (e) {
//                 $('img.imgFile').attr('src', e.target.result);
//             };
//             reader.readAsDataURL(input.files[0]);
//         }
//     }
//
//     $(document).on("change", ".contentTeacher #city", function () {
//
//         for (var i = 0; i < 100; i++) {
//             if ($(this).val() == $($(".contentTeacher .nameCountry")[i]).text()) {
//                 $($(".contentTeacher .nameCountry")[i]).remove()
//             }
//         }
//
//         $(".edit .country .listCountry").append('<span class="nameCountry">' + $(this).val() + '<i class="fal fa-times-circle"></i>' + '</span>');
//
//         for (var i = 0; i < 100; i++) {
//             if ($(this).val() == $($(".contentTeacher .nameCountry")[i]).text()) {
//                 $($(".contentTeacher .nameCountry")[i]).remove()
//             }
//         }
//
//         // $(this).attr("data-value", $($($(this).next("datalist")).find("option[value='" + $(this).val() + "']")).attr("data-value"))
//
//         $(this).val("")
//
//     })
//
//     $(document).on("click", ".nameCountry2 i", function () {
//         $($(this).parents(".nameCountry2")).remove();
//     })
//
//     $(document).on("change", "#city2", function () {
//
//         for (var i = 0; i < 10; i++) {
//             if ($(this).val() == $($(".nameCountry2")[i]).text()) {
//                 $($(".nameCountry2")[i]).remove()
//             }
//         }
//
//         $(".country2 .listCountry2").append('<span class="nameCountry2">' + $(this).val() + '<i class="fal fa-times-circle"></i>' + '</span>');
//
//         $(this).val("");
//         testStep3();
//
//     })
//
//     $(document).on("click", ".nameCountry2 i", function () {
//         $($(this).parents(".nameCountry2")).remove();
//         testStep3();
//     })
//
//     $(document).on("change", ".contentTeacher #education", function () {
//
//         for (var i = 0; i < 10; i++) {
//             if ($(this).val() == $($(".contentTeacher .nameEducation")[i]).text()) {
//                 $($(".contentTeacher .nameEducation")[i]).remove()
//             }
//         }
//
//         $(".contentTeacher .edit .education .listEducation").append('<span class="nameEducation">' + $(this).val() + '<i class="fal fa-times-circle"></i>' + '</span>');
//
//         $(this).val("")
//
//     })
//
//     $(document).on("click", ".go_for_higher", function () {
//         $("html, body").animate({
//             scrollTop: 0
//         }, 1000);
//     })
//
//     $(document).mousedown(function (e) {
//         var container = $(".overUserChat");
//         if (!container.is(e.target) && container.has(e.target).length === 0) {
//             $(".listUsers li .icon").removeClass("active");
//             $(".overUserChat").removeClass("active");
//         }
//     });
//
//     $(document).on("click", ".listUsers li .icon", function () {
//         $(this).toggleClass("active");
//         $($(this).find(".overUserChat")).toggleClass("active");
//     })
//
//     $(document).on("click", ".listUsers li", function (e) {
//         $(".listUsers li").removeClass("active");
//         $(this).addClass("active");
//         showChat();
//         if ($(window).outerWidth(true) <= 800) {
//             if ($($(e.target).parents(".icon")).length !== 1) {
//                 $(".colList").removeClass("activeMob");
//                 $(".btnBack").css("display", "inline-flex");
//             } else {
//                 $($(this).find(".icon")).toggleClass("active");
//                 $($($(this).parents(".icon")).find(".overUserChat")).toggleClass("active");
//             }
//         }
//     })
//
//     $(document).on("click", ".boxChat .btnBack", function () {
//         if ($(window).outerWidth(true) <= 800) {
//             $(".colList").addClass("activeMob")
//             $(".btnBack").css("display", "none")
//         }
//     })
//
//     function showChat() {
//         for (var i = 0; i < $(".listUsers li").length; i++) {
//             if ($($(".listUsers li")[i]).hasClass("active")) {
//                 $(".chatPerson").removeClass("active");
//                 $(".chatPerson." + $($(".listUsers li")[i]).attr("data-chat")).addClass("active")
//             }
//         }
//     }
//
//     showChat()
//
//     function boxLoginTeacher() {
//         if ($(window).outerWidth(true) >= 992) {
//             $(".boxLoginTeacher .col2").height($(".boxLoginTeacher .col1>div").outerHeight(true))
//         } else {
//             $(".boxLoginTeacher .col2").css("height", "auto");
//         }
//     }
//
//     boxLoginTeacher();
//
//     $(window).resize(function () {
//         boxLoginTeacher();
//     })
//
//     $(document).on("click", ".numsMobile .num1", function () {
//         $(".numsMobile").attr("data-active", "1")
//     })
//     $(document).on("click", ".numsMobile .num2", function () {
//         $(".numsMobile").attr("data-active", "2")
//     })
//     $(document).on("click", ".numsMobile .num3", function () {
//         $(".numsMobile").attr("data-active", "3")
//     })
//     $(document).on("click", ".numsMobile .num4", function () {
//         $(".numsMobile").attr("data-active", "4")
//     })
//
//     var x = 0;
//
//     for (var i = 0; i < $(".formToJoinTeacher .input").length; i++) {
//
//         if ($($(".formToJoinTeacher .input")[i]).attr("requiredX") == "yes") {
//             $($(".formToJoinTeacher .input")[i]).attr("true", "no");
//         } else {
//             $($(".formToJoinTeacher .input")[i]).attr("true", "yes");
//         }
//     };
//
//     /**********************/
//     /* login Teacher */
//     /**********************/
//
//     for (var i = 0; i < $(".boxLoginTeacher .input[requiredX]").length; i++) {
//         $($($(".boxLoginTeacher .input[requiredX]")[i]).parent(".divInput")).after('<small class="error"></small>');
//     }
//
//     var phoneStep3 = 0;
//     var valid305 = 0;
//
//     $(document).on("keyup", "#teacher_mobile", function () {
//
//         if ($(this).val().split("")[0] == 0 && $(this).val().split("")[1] == 5) {
//             $(this).addClass("correct").removeClass("error");
//             $($($(this).parent(".divInput")).next("small.error")).hide();
//             $($($(this).parent(".divInput")).next("small.error")).text();
//             $(this).attr("true", "yes");
//             valid305 = 1;
//         } else {
//             $(this).addClass("error").removeClass("correct");
//             $($($(this).parent(".divInput")).next("small.error")).show();
//             $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("error05"));
//             $(this).attr("true", "no");
//             phoneStep3 = 0;
//             valid305 = 0;
//         }
//
//         if (($(this).val().length == 10) && ($(this).val().split("")[0] == 0) && ($(this).val().split("")[1] == 5)) {
//             $(this).addClass("correct").removeClass("error");
//             $($($(this).parent(".divInput")).next("small.error")).hide();
//             $($($(this).parent(".divInput")).next("small.error")).text();
//             $(this).attr("true", "yes");
//             phoneStep3 = 1;
//         } else {
//             if (valid305 == 1) {
//                 $(this).addClass("error").removeClass("correct");
//                 $($($(this).parent(".divInput")).next("small.error")).show();
//                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMin"));
//                 $(this).attr("true", "no");
//             } else {
//                 $(this).addClass("error").removeClass("correct");
//                 $($($(this).parent(".divInput")).next("small.error")).show();
//                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("error05"));
//                 $(this).attr("true", "no");
//             }
//             phoneStep3 = 0;
//         }
//
//         for (var i = 0; i < $(".boxLoginTeacher form .input").length; i++) {
//             if ($($(".boxLoginTeacher form .input")[i]).attr("true") != "yes") {
//                 x++;
//             }
//             if (i == $(".boxLoginTeacher form .input").length - 1) {
//
//                 if (x != 0 && phoneStep3 == 0) {
//                     $($(".boxLoginTeacher form").find(".submit")).attr("type", "button");
//                 } else if (x == 0 && phoneStep3 == 1) {
//                     $($(".boxLoginTeacher form").find(".submit")).attr("type", "submit");
//                 }
//                 x = 0;
//             }
//         }
//
//
//     })
//
//
//     $(document).on("keyup change blur", ".boxLoginTeacher form .input[requiredX]:not(#teacher_mobile)", function () {
//
//         if ($(this).attr("requiredX") == "yes") {
//             if ($(this).val().length == 0) {
//                 $(this).addClass("error").removeClass("correct");
//                 $($($(this).parent(".divInput")).next("small.error")).show();
//                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorEmpty"));
//                 $(this).attr("true", "no");
//             } else if ($(this).val().length < $(this).attr("min")) {
//                 if ($(this).attr("min")) {
//                     $(this).addClass("error").removeClass("correct");
//                     $($($(this).parent(".divInput")).next("small.error")).show();
//                     $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMin"));
//                     $(this).attr("true", "no");
//                 }
//             } else if ($(this).val().length > $(this).attr("max")) {
//                 if ($(this).attr("max")) {
//                     $(this).addClass("error").removeClass("correct");
//                     $($($(this).parent(".divInput")).next("small.error")).show();
//                     $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMax"));
//                     $(this).attr("true", "no");
//                 }
//             } else {
//                 $(this).addClass("correct").removeClass("error");
//                 $($($(this).parent(".divInput")).next("small.error")).hide();
//                 $($($(this).parent(".divInput")).next("small.error")).text();
//                 $(this).attr("true", "yes");
//             }
//
//             if ($(this).attr("type") == "email") {
//                 if (IsEmail($(this).val()) == false) {
//                     $(this).addClass("error").removeClass("correct");
//                     $($($(this).parent(".divInput")).next("small.error")).show();
//                     $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorType"));
//                     $(this).attr("true", "no");
//                     return false;
//                 } else {
//                     $(this).addClass("correct").removeClass("error");
//                     $($($(this).parent(".divInput")).next("small.error")).hide();
//                     $($($(this).parent(".divInput")).next("small.error")).text();
//                     $(this).attr("true", "yes");
//                     return true;
//                 }
//             }
//
//             if ($(this).attr("type") == "checkbox") {
//                 if ($(this).is(':checked')) {
//                     $(this).attr("true", "yes");
//                 } else {
//                     $(this).attr("true", "no");
//                 }
//             }
//         } else {
//             $(this).attr("true", "yes")
//         }
//
//         for (var i = 0; i < $(".boxLoginTeacher form .input:not(#teacher_mobile)").length; i++) {
//             if ($($(".boxLoginTeacher form .input")[i]).attr("true") != "yes") {
//                 x++;
//             }
//             if (i == $(".boxLoginTeacher form .input").length - 1) {
//
//                 if (x != 0) {
//                     $($(".boxLoginTeacher form").find(".submit")).attr("type", "button");
//                 } else {
//                     $($(".boxLoginTeacher form").find(".submit")).attr("type", "submit");
//                 }
//                 x = 0;
//             }
//         }
//
//     })
//     var x = 0;
//
//     for (var i = 0; i < $(".boxLoginTeacher form .input").length; i++) {
//
//         if ($($(".boxLoginTeacher form .input")[i]).attr("requiredX") == "yes") {
//             $($(".boxLoginTeacher form .input")[i]).attr("true", "no");
//         } else {
//             $($(".boxLoginTeacher form .input")[i]).attr("true", "yes");
//         }
//     };
//
//     /**********************/
//     /* complete Data Teacher Step 1 to 5 */
//     /**********************/
//
//
//     function IsEmail(email) {
//         var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//         if (!regex.test(email)) {
//             return false;
//         } else {
//             return true;
//         }
//     }
//
//     setTimeout(function () {
//         for (var i = 0; i < $(".completeDataTeacher .input").length; i++) {
//             $($($(".completeDataTeacher .input")[i]).parents(".divInput")).after(
//                 '<small class="error"></small>');
//         }
//     }, 250)
//
//     $(document).on("keyup change blur", ".completeDataTeacher .input[requiredX]", function () {
//
//         if ($(this).attr("requiredX") == "yes") {
//             if (this.nodeName.toLowerCase() == "input" && $(this).attr("type") != "radio" && $(this).attr(
//                 "type") != "checkbox") {
//                 if ($(this).val().length == 0) {
//                     $(this).addClass("error").removeClass("correct");
//                     $($($(this).parent(".divInput")).next("small.error")).show();
//                     $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorEmpty"));
//                     $(this).attr("true", "no");
//                     console.log("1")
//                 } else if ($(this).val().length < $(this).attr("min")) {
//                     if ($(this).attr("min")) {
//                         $(this).addClass("error").removeClass("correct");
//                         $($($(this).parent(".divInput")).next("small.error")).show();
//                         $($($(this).parent(".divInput")).next("small.error")).text($(this).attr(
//                             "errorMin"));
//                         $(this).attr("true", "no");
//                     }
//                     console.log("2")
//                 } else if ($(this).attr("name") == "discount_percentage") {
//                     if ($(this).val() < $(this).attr("min")) {
//                         if ($(this).attr("min")) {
//                             $(this).addClass("error").removeClass("correct");
//                             $($($(this).parent(".divInput")).next("small.error")).show();
//                             $($($(this).parent(".divInput")).next("small.error")).text($(this).attr(
//                                 "errorMin"));
//                             $(this).attr("true", "no");
//                         }
//                         console.log("02")
//                     } else {
//                         $(this).addClass("correct").removeClass("error");
//                         $($($(this).parent(".divInput")).next("small.error")).hide();
//                         $($($(this).parent(".divInput")).next("small.error")).text();
//                         $(this).attr("true", "yes");
//                     }
//                 } else if ($(this).val().length > $(this).attr("max")) {
//                     if ($(this).attr("max")) {
//                         $(this).addClass("error").removeClass("correct");
//                         $($($(this).parent(".divInput")).next("small.error")).show();
//                         $($($(this).parent(".divInput")).next("small.error")).text($(this).attr(
//                             "errorMax"));
//                         $(this).attr("true", "no");
//                     }
//                     console.log("3")
//                 } else {
//                     $(this).addClass("correct").removeClass("error");
//                     $($($(this).parent(".divInput")).next("small.error")).hide();
//                     $($($(this).parent(".divInput")).next("small.error")).text();
//                     $(this).attr("true", "yes");
//                     console.log("4")
//                 }
//             } else if (this.nodeName.toLowerCase() == "select") {
//                 if ($(this).val() == 0) {
//                     $(this).addClass("error").removeClass("correct");
//                     $($($(this).parent(".divInput")).next("small.error")).show();
//                     $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorEmpty"));
//                     $(this).attr("true", "no");
//                     console.log("5")
//                 } else if ($(this).val() != 0) {
//                     $(this).addClass("correct").removeClass("error");
//                     $($($(this).parent(".divInput")).next("small.error")).hide();
//                     $($($(this).parent(".divInput")).next("small.error")).text();
//                     $(this).attr("true", "yes");
//                     console.log("6")
//                 }
//             }
//
//             if ($(this).attr("type") == "email") {
//                 if (IsEmail($(this).val()) == false) {
//                     $(this).addClass("error").removeClass("correct");
//                     $($($(this).parent(".divInput")).next("small.error")).show();
//                     $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorType"));
//                     $(this).attr("true", "no");
//                     console.log("7")
//                     return false;
//                 } else {
//                     $(this).addClass("correct").removeClass("error");
//                     $($($(this).parent(".divInput")).next("small.error")).hide();
//                     $($($(this).parent(".divInput")).next("small.error")).text();
//                     $(this).attr("true", "yes");
//                     console.log("8")
//                     return true;
//                 }
//             }
//
//             if ($(this).attr("type") == "checkbox") {
//                 if ($(this).is(':checked')) {
//                     $(this).attr("true", "yes");
//                     $("[name='" + $(this).attr("name") + "']").addClass("correct").removeClass("error");
//                     $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .hide();
//                     $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .text();
//                     console.log("9")
//                 } else {
//                     $(this).attr("true", "no");
//                     $("[name='" + $(this).attr("name") + "']").addClass("error").removeClass("correct");
//                     $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .show();
//                     $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .text($(this).attr("errorEmpty"));
//                     console.log("10")
//                 }
//             }
//
//             if ($(this).attr("type") == "radio") {
//                 if ($(this).is(':checked')) {
//                     $("[name='" + $(this).attr("name") + "']").attr("true", "yes");
//                     $("[name='" + $(this).attr("name") + "']").addClass("correct").removeClass("error");
//                     $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .hide();
//                     $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .text();
//                     console.log("11")
//                 } else {
//                     $("[name='" + $(this).attr("name") + "']").attr("true", "no");
//                     $("[name='" + $(this).attr("name") + "']").addClass("error").removeClass("correct");
//                     $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .show();
//                     $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
//                         .text($(this).attr("errorEmpty"));
//                     console.log("12")
//                 }
//             }
//
//         } else {
//             $(this).attr("true", "yes")
//         }
//
//         for (var i = 0; i < $(".completeDataTeacher form.formToJoinTeacher input").length; i++) {
//             if ($($(".completeDataTeacher form.formToJoinTeacher input")[i]).attr("true") != "yes") {
//                 x++;
//             }
//             if (i == $(".completeDataTeacher form.formToJoinTeacher input").length - 1) {
//
//                 if (x != 0) {
//                     $($(".completeDataTeacher form.formToJoinTeacher").find(".submit")).attr("type",
//                         "button");
//                 } else {
//                     $($(".completeDataTeacher form.formToJoinTeacher").find(".submit")).attr("type",
//                         "submit");
//
//                 }
//                 x = 0;
//             }
//         }
//
//     })
//
//     var x = 0;
//
//     $(document).on("change keyup", ".step1 .input", function () {
//         for (var i = 0; i < $(".completeDataTeacher .dataTeacher .step1 .input:not(#phone)").length; i++) {
//             if ($($(".completeDataTeacher .dataTeacher .step1 .input:not(#phone)")[i]).attr("true") != "yes") {
//                 x++;
//             }
//             if (i == $(".completeDataTeacher .dataTeacher .step1 .input:not(#phone)").length - 1) {
//
//                 if (x != 0) {
//                     $($(".completeDataTeacher .dataTeacher .step1").find(".submit")).attr("type",
//                         "button");
//
//                 } else if (x == 0 && phoneStep1 == 1) {
//                     $($(".completeDataTeacher .dataTeacher .step1").find(".submit")).attr("type",
//                         "submit");
//
//                 }
//                 x = 0;
//             }
//         }
//     })
//
//     var phoneStep1 = 0;
//     var valid05 = 0;
//
//     $(document).on("keyup", "#phone", function () {
//
//         if ($(this).val().split("")[0] == 0 && $(this).val().split("")[1] == 5) {
//             $(this).addClass("correct").removeClass("error");
//             $($($(this).parent(".divInput")).next("small.error")).hide();
//             $($($(this).parent(".divInput")).next("small.error")).text();
//             $(this).attr("true", "yes");
//             valid05 = 1;
//         } else {
//             $(this).addClass("error").removeClass("correct");
//             $($($(this).parent(".divInput")).next("small.error")).show();
//             $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("error05"));
//             $(this).attr("true", "no");
//             phoneStep1 = 0;
//             valid05 = 0;
//         }
//
//         if ($(this).val().length == 10) {
//             $(this).addClass("correct").removeClass("error");
//             $($($(this).parent(".divInput")).next("small.error")).hide();
//             $($($(this).parent(".divInput")).next("small.error")).text();
//             $(this).attr("true", "yes");
//         } else {
//             if (valid05 == 1) {
//                 $(this).addClass("error").removeClass("correct");
//                 $($($(this).parent(".divInput")).next("small.error")).show();
//                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMin"));
//             }
//         }
//     })
//
//     for (var i = 0; i < $(".completeDataTeacher .dataTeacher .input").length; i++) {
//
//         if ($($(".completeDataTeacher .dataTeacher .input")[i]).attr("requiredX") == "yes") {
//             $($(".completeDataTeacher .dataTeacher .input")[i]).attr("true", "no");
//         } else {
//             $($(".completeDataTeacher .dataTeacher .input")[i]).attr("true", "yes");
//         }
//     };
//
//     //////////////////Step 2///////////////////
//
//     var x2 = 0;
//
//     $(document).on("change keyup", ".step2 .input", function () {
//         for (var i = 0; i < $(".completeDataTeacher .dataTeacher .step2 .input").length; i++) {
//             if ($($(".completeDataTeacher .dataTeacher .step2 .input")[i]).attr("true") != "yes") {
//                 x2++;
//             }
//             if (i == $(".completeDataTeacher .dataTeacher .step2 .input").length - 1) {
//
//                 if (x2 != 0) {
//                     $($(".completeDataTeacher .dataTeacher .step2").find(".submit")).attr("type", "button");
//                 } else {
//                     $($(".completeDataTeacher .dataTeacher .step2").find(".submit")).attr("type", 'submit');
//                 }
//                 x2 = 0;
//             }
//         }
//     })
//
//     /////////////////////////////step3///////////////////////////
//
//     var y = 0;
//
//     // $(document).find(".completeDataTeacher .step3 .input[requiredx='yes']").trigger("change")
//
//     $(document).on("change keyup", ".completeDataTeacher .step3 .input[requiredx='yes']", function () {
//
//         for (var i = 0; i < $(".completeDataTeacher .dataTeacher .step3 .input[type='text']").length; i++) {
//             if ($($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).val() == '') {
//                 $($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).attr("true",
//                     "no");
//                 y++;
//             } else {
//                 $($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).attr("true",
//                     "yes");
//             }
//             if (i == $(".completeDataTeacher .dataTeacher .step3 .input[type='text']").length - 1) {
//                 if (y != 0) {
//                     $($(".completeDataTeacher .dataTeacher .step3").find(".submit")).attr("type",
//                         "button");
//                 } else if (y == 0) {
//                     $($(".completeDataTeacher .dataTeacher .step3").find(".submit")).attr("type",
//                         "submit");
//                 }
//                 y = 0;
//             }
//         }
//
//     })
//
//     $(document).on("change keyup", ".completeDataTeacher .step3 .input[requiredx='yes']", function () {
//         if ($(this).val() == '') {
//             $($(this).parents(".inputPrice")).removeClass("bcGreen").addClass("bcRed");
//             $($($(this).parents(".divInput")).next("small")).show()
//             $($(this).parents(".divInput ")).next("small").text($(this).attr("errorempty"))
//         } else {
//             $($(this).parents(".inputPrice")).removeClass("bcRed").addClass("bcGreen");
//             $($($(this).parents(".divInput")).next("small")).hide()
//         }
//     })
//
//     setTimeout(function () {
//
//         for (var i = 0; i < $(".completeDataTeacher .step3 input[type='checkbox']").length; i++) {
//             if ($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).is(':checked')) {
//                 $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
//                     "input[type='text']")).attr("true", "no");
//                 $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
//                     "input[type='text']")).attr("requiredX", "yes");
//             } else {
//                 $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
//                     "input[type='text']")).attr("true", "yes");
//                 $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
//                     "input[type='text']")).attr("requiredX", "no");
//             }
//         };
//
//     }, 100);
//
//     $(document).on("change", ".completeDataTeacher .step3 input[type='checkbox']", function () {
//
//         for (var i = 0; i < $(".completeDataTeacher .step3 input[type='checkbox']").length; i++) {
//             if ($($(".step3 input[type='checkbox']")[i]).is(':checked')) {
//                 if ($($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
//                     "input[type='text']")).val() == "") {
//
//                     $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
//                         "input[type='text']")).attr("true", "no");
//
//                     $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
//                         "input[type='text']")).attr("requiredX", "yes");
//                 }
//             } else {
//                 $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
//                     "input[type='text']")).attr("true", "yes");
//                 $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
//                     "input[type='text']")).attr("requiredX", "no");
//             }
//         };
//
//         for (var i = 0; i < $(".completeDataTeacher .dataTeacher .step3 .input[type='text']").length; i++) {
//             if ($($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).val() == '') {
//                 $($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).attr("true",
//                     "no");
//                 y++;
//             } else {
//                 $($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).attr("true",
//                     "yes");
//             }
//             if (i == $(".completeDataTeacher .dataTeacher .step3 .input[type='text']").length - 1) {
//                 if (y != 0) {
//                     $($(".completeDataTeacher .dataTeacher .step3").find(".submit")).attr("type",
//                         "button");
//                 } else if (y == 0) {
//                     $($(".completeDataTeacher .dataTeacher .step3").find(".submit")).attr("type",
//                         "submit");
//                 }
//                 y = 0;
//             }
//         }
//     });
//
//     //////////////////////////////Step4///////////////////////
//
//     $(document).on("keyup blur", ".completeDataTeacher .textMarketStep4", function () {
//         $(".completeDataTeacher .step4 .limitText b").text($(this).val().length)
//         if ($(this).val().length == 0) {
//             check2Step4 = 0;
//             $(".step4 .error2").show()
//             $(this).addClass("bcRed").removeClass("bcGreen")
//         } else {
//             check2Step4 = 1;
//             $(".step4 .error2").hide()
//             $(this).addClass("bcGreen").removeClass("bcRed")
//         }
//     })
//
//     var z = 0;
//
//     $(document).on("change", ".completeDataTeacher .step4 .exp .input", function () {
//
//         if ($($(".completeDataTeacher .step4 .exp .input")).is(':checked')) {
//             check1Step4 = 1;
//             $(".step4 .error1").hide()
//         } else {
//             check1Step4 = 0;
//             $(".step4 .error1").show()
//         }
//
//     });
//
//     $(document).on("keyup blur", ".completeDataTeacher .aboutMe textarea", function () {
//         if ($(this).val().length == 0) {
//             check4Step4 = 0;
//             $(".step4 .error4").show()
//             $(this).addClass("bcRed").removeClass("bcGreen")
//         } else {
//             check4Step4 = 1;
//             $(".step4 .error4").hide()
//             $(this).addClass("bcGreen").removeClass("bcRed")
//         }
//     })
//
//     $(document).on("change", ".completeDataTeacher .video input", function (e) {
//
//         var $source = $('.completeDataTeacher #video_here video');
//         $source[0].src = URL.createObjectURL(this.files[0]);
//
//         $(".completeDataTeacher #video_here i").hide();
//         $('.completeDataTeacher #video_here video').show();
//
//         if ($($(this)[0].files).length == 0) {
//             check3Step4 = 1; // video validation
//             $(".step4 .error3").hide()
//         } else {
//             check3Step4 = 1;
//             $(".step4 .error3").hide()
//         }
//     })
//
//     var check1Step4 = 0;
//     var check2Step4 = 0;
//     var check3Step4 = 1;
//     var check4Step4 = 0;
//
//     $(document).on("mousemove", ".completeDataTeacher .step4 ", function () {
//         if (check1Step4 == 1 && check2Step4 == 1 && check3Step4 == 1 && check4Step4 == 1) {
//             $($(".completeDataTeacher .dataTeacher .step4").find(".submit")).attr("type", "submit")
//         } else {
//             $($(".completeDataTeacher .dataTeacher .step4").find(".submit")).attr("type", "button")
//         }
//     })
//
//     ///////////////////////////////
//
//     $(document).on("click", ".completeDataTeacher .dataTeacher .step1 .ok .submit[type='submit']", function () {
//         $(".completeDataTeacher .rowStep").attr("step", "2");
//         $('html,body').animate({
//             scrollTop: 0
//         }, 'slow');
//     })
//
//     $(document).on("click", ".completeDataTeacher .dataTeacher .step2 .ok .submit[type='submit']", function () {
//         $(".completeDataTeacher .rowStep").attr("step", "3");
//         $('html,body').animate({
//             scrollTop: 0
//         }, 'slow');
//     })
//
//     $(document).on("click", ".completeDataTeacher .dataTeacher .step3 .ok .submit[type='submit']", function () {
//         $(".completeDataTeacher .rowStep").attr("step", "4");
//         $('html,body').animate({
//             scrollTop: 0
//         }, 'slow');
//     })
//
//     $(document).on("click", ".completeDataTeacher .dataTeacher .step4 .ok.submit[type='submit']", function () {
//         $(".completeDataTeacher .rowStep").attr("step", "5");
//         $('html,body').animate({
//             scrollTop: 0
//         }, 'slow');
//     })
//
//     var x = 0;
//
//     $(document).on("change keyup", ".step1 .input", function () {
//         for (var i = 0; i < $(".completeDataTeacher .dataTeacher .step1 .input").length; i++) {
//             if ($($(".completeDataTeacher .dataTeacher .step1 .input")[i]).attr("true") != "yes") {
//                 x++;
//             }
//             if (i == $(".completeDataTeacher .dataTeacher .step1 .input").length - 1) {
//
//                 if (x != 0) {
//                     $($(".completeDataTeacher .dataTeacher .step1").find(".submit")).attr("type", "button");
//                 } else {
//                     $($(".completeDataTeacher .dataTeacher .step1").find(".submit")).attr("type", "submit");
//                     document.getElementById("step1").classList.remove('submit');
//
//                 }
//                 x = 0;
//             }
//         }
//     })
//
//     $(document).on("click", ".completeDataTeacher .dataTeacher .step1 .ok .submit[type='submit']", function () {
//         $(".completeDataTeacher .rowStep").attr("step", "2");
//         $('html,body').animate({
//             scrollTop: 0
//         }, 'slow');
//     })
//
//     // $(document).on("click", ".submit", function () {
//     //     // $($($(this).parents("form")).find("input:not(#video), select, textarea")).trigger("change")
//     //     // $($($(this).parents("form")).find("input, select, textarea")).trigger("keydown")
//     //     // $($($(this).parents("form")).find("input, select, textarea")).trigger("keyup")
//     //     // $($($(this).parents("form")).find("input, select, textarea")).trigger("blur")
//     //     testData(xl = $($(this).parents("form")).find("input:not(#video), select, textarea"))
//     //     console.log(xl)
//     // })
//
//     setTimeout(function () {
//         for (var i = 0; i < $($(".step1 .input")).length; i++) {
//             if (($(".step1 .input")[i]).nodeName.toLowerCase() == "input" && $($(".step1 .input")[i]).attr(
//                 "type") != "radio" && $($(".step1 .input")[i]).attr("type") != "checkbox") {
//                 if ($($(".step1 .input")[i]).val() != "" && $($(".step1 .input")[i]).val() != 0) {
//                     $($(".step1 .input")[i]).trigger("keyup")
//                 }
//             }
//         }
//
//         for (var i = 0; i < $($(".step1 .input")).length; i++) {
//             if (($(".step1 .input")[i]).nodeName.toLowerCase() == "input" && $($(".step1 .input")[i]).attr(
//                 "type") == "checkbox") {
//                 $($(".step1 .input")[i]).trigger("keyup")
//             }
//         }
//     }, 500)
//
//     var inputSub = $(".inputSub .divInput").html();
//
//     $(document).on("click", ".addAnotherInputSub", function () {
//         $(".inputSub").append('<div class="divInput"> ' + inputSub + '</div>')
//     })
//
//     $(document).on("click", ".btnDeleteHour", function () {
//         $($($(this).parents("tr.rowTo")[0]).prev()).remove();
//         $($(this).parents("tr.rowTo")[0]).remove();
//     })
//
//     function preventNumberInput(e) {
//         var keyCode = (e.keyCode ? e.keyCode : e.which);
//         if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107) {
//             e.preventDefault();
//         }
//     }
//
//     $(document).ready(function () {
//         $(document).on("keypress", "td input", function (e) {
//             preventNumberInput(e);
//         });
//     })
//
//     /********************************************/
//     /* script for visitor */
//     /********************************************/
//
//     // click User Menu
//     $(document).on("click", ".headerVisitor .addToUs", function () {
//         $(".headerVisitor .menuAddToUs").toggleClass("active");
//     })
//
//     $(document).on("click", ".headerVisitor .joinAsStudent", function () {
//         $(".popUpJoinAsTeacher.popUpFromVisitor1").addClass("show");
//     })
//
//     $(document).on("click", ".popUpJoinAsTeacher .closeX", function () {
//         $(".popUpJoinAsTeacher.popUpFromVisitor1").removeClass("show");
//     })
//
//     $(document).mousedown(function (e) {
//         var container = $(".popUpJoinAsTeacher.popUpFromVisitor1>div");
//         if (!container.is(e.target) && container.has(e.target).length === 0) {
//             $(".popUpJoinAsTeacher.popUpFromVisitor1").removeClass("show");
//         }
//     });
//
//     function IsEmail(email) {
//         var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//         if (!regex.test(email)) {
//             return false;
//         } else {
//             return true;
//         }
//     }
//
//     // for (var i = 0; i < $(".popUpJoinAsTeacher.formPublic input[requiredX]").length; i++) {
//     //     $($($("input[requiredX]")[i]).parent(".divInput")).after('<small class="error"></small>');
//     // }
//
//     // $(document).on("keyup change blur", ".popUpJoinAsTeacher.formPublic input[requiredX]", function () {
//
//     //     if ($(this).attr("requiredX") == "yes") {
//     //         if ($(this).val().length == 0) {
//     //             $(this).addClass("error").removeClass("correct");
//     //             $($($(this).parent(".divInput")).next("small.error")).show();
//     //             $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorEmpty"));
//     //             $(this).attr("true", "no");
//     //         } else if ($(this).val().length < $(this).attr("min")) {
//     //             if ($(this).attr("min")) {
//     //                 $(this).addClass("error").removeClass("correct");
//     //                 $($($(this).parent(".divInput")).next("small.error")).show();
//     //                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMin"));
//     //                 $(this).attr("true", "no");
//     //             }
//     //         } else if ($(this).val().length > $(this).attr("max")) {
//     //             if ($(this).attr("max")) {
//     //                 $(this).addClass("error").removeClass("correct");
//     //                 $($($(this).parent(".divInput")).next("small.error")).show();
//     //                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMax"));
//     //                 $(this).attr("true", "no");
//     //             }
//     //         } else {
//     //             $(this).addClass("correct").removeClass("error");
//     //             $($($(this).parent(".divInput")).next("small.error")).hide();
//     //             $($($(this).parent(".divInput")).next("small.error")).text();
//     //             $(this).attr("true", "yes");
//     //         }
//
//     //         if ($(this).attr("type") == "email") {
//     //             if (IsEmail($(this).val()) == false) {
//     //                 $(this).addClass("error").removeClass("correct");
//     //                 $($($(this).parent(".divInput")).next("small.error")).show();
//     //                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorType"));
//     //                 $(this).attr("true", "no");
//     //                 return false;
//     //             } else {
//     //                 $(this).addClass("correct").removeClass("error");
//     //                 $($($(this).parent(".divInput")).next("small.error")).hide();
//     //                 $($($(this).parent(".divInput")).next("small.error")).text();
//     //                 $(this).attr("true", "yes");
//     //                 return true;
//     //             }
//     //         }
//
//     //         if ($(this).attr("type") == "checkbox") {
//     //             if ($(this).is(':checked')) {
//     //                 $(this).attr("true", "yes");
//     //             } else {
//     //                 $(this).attr("true", "no");
//     //             }
//     //         }
//     //     } else {
//     //         $(this).attr("true", "yes")
//     //     }
//
//     //     for (var i = 0; i < $(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input").length; i++) {
//     //         if ($($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input")[i]).attr("true") != "yes") {
//     //             x++;
//     //         }
//     //         if (i == $(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input").length - 1) {
//
//     //             if (x != 0) {
//     //                 $($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin").find(".submit")).attr("disabled", "disabled");
//     //             } else {
//     //                 $($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin").find(".submit")).removeAttr("disabled");
//     //             }
//     //             x = 0;
//     //         }
//     //     }
//
//     // })
//     // var x = 0;
//
//     // for (var i = 0; i < $(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input").length; i++) {
//
//     //     if ($($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input")[i]).attr("requiredX") == "yes") {
//     //         $($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input")[i]).attr("true", "no");
//     //     } else {
//     //         $($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input")[i]).attr("true", "yes");
//     //     }
//     // };
//
// })
//
// /* Home */
// /* Home */
// /* Home */
// /* Home */
//
// $(document).ready(function () {
//
//     $('.slick').slick({
//         infinite: true,
//         speed: 10000,
//         fade: true,
//         rtl: true,
//         pauseOnHover: false,
//         cssEase: 'linear',
//         autoplay: true,
//         autoplaySpeed: 10000,
//         prevArrow: false,
//         nextArrow: false,
//     });
//
//     function ratingSkills() {
//         if ($(window).outerWidth(true) >= 992) {
//             $('.ratings_slick').not('.slick-initialized').slick({
//                 infinite: true,
//                 speed: 500,
//                 fade: false,
//                 rtl: true,
//                 pauseOnHover: false,
//                 cssEase: 'linear',
//                 autoplay: false,
//                 autoplaySpeed: 2000,
//                 prevArrow: false,
//                 nextArrow: false,
//                 slidesToShow: 3,
//                 slidesToScroll: 3,
//                 centerMode: false,
//                 responsive: [{
//                     breakpoint: 400,
//                     settings: {
//                         fade: false,
//                         autoplay: true,
//                         slidesToShow: 1,
//                         slidesToScroll: 1,
//                         centerMode: false,
//                         /* set centerMode to false to show complete slide instead of 3 */
//                         pauseOnHover: true,
//                     }
//                 },
//                     {
//                         breakpoint: 768,
//                         settings: {
//                             fade: false,
//                             autoplay: true,
//                             slidesToShow: 2,
//                             slidesToScroll: 2,
//                             centerMode: false,
//                             /* set centerMode to false to show complete slide instead of 3 */
//                             pauseOnHover: true,
//                         }
//                     },
//                     {
//                         breakpoint: 1200,
//                         settings: {
//                             fade: false,
//                             autoplay: true,
//                             slidesToShow: 2,
//                             slidesToScroll: 2,
//                             centerMode: false,
//                             /* set centerMode to false to show complete slide instead of 3 */
//                             pauseOnHover: true,
//                         }
//                     },
//                 ]
//             });
//         } else if ($(window).outerWidth(true) >= 768) {
//             $('.ratings_slick').not('.slick-initialized').slick({
//                 infinite: true,
//                 speed: 500,
//                 fade: false,
//                 rtl: true,
//                 pauseOnHover: false,
//                 cssEase: 'linear',
//                 autoplay: false,
//                 autoplaySpeed: 2000,
//                 prevArrow: false,
//                 nextArrow: false,
//                 slidesToShow: 2,
//                 slidesToScroll: 2,
//                 centerMode: false,
//                 responsive: [{
//                     breakpoint: 400,
//                     settings: {
//                         fade: false,
//                         autoplay: true,
//                         slidesToShow: 1,
//                         slidesToScroll: 1,
//                         centerMode: false,
//                         /* set centerMode to false to show complete slide instead of 3 */
//                         pauseOnHover: true,
//                     }
//                 },
//                     {
//                         breakpoint: 768,
//                         settings: {
//                             fade: false,
//                             autoplay: true,
//                             slidesToShow: 2,
//                             slidesToScroll: 2,
//                             centerMode: false,
//                             /* set centerMode to false to show complete slide instead of 3 */
//                             pauseOnHover: true,
//                         }
//                     }
//                 ]
//             });
//         } else {
//             $('.ratings_slick').not('.slick-initialized').slick({
//                 infinite: true,
//                 speed: 500,
//                 fade: false,
//                 rtl: true,
//                 pauseOnHover: false,
//                 cssEase: 'linear',
//                 autoplay: false,
//                 autoplaySpeed: 2000,
//                 prevArrow: false,
//                 nextArrow: false,
//                 slidesToShow: 1,
//                 slidesToScroll: 1,
//                 centerMode: false,
//                 responsive: [{
//                     breakpoint: 400,
//                     settings: {
//                         fade: false,
//                         autoplay: true,
//                         slidesToShow: 1,
//                         slidesToScroll: 1,
//                         centerMode: false,
//                         /* set centerMode to false to show complete slide instead of 3 */
//                         pauseOnHover: true,
//                     }
//                 }]
//             });
//         }
//     }
//
//     ratingSkills()
//
//     $(window).resize(function () {
//         ratingSkills()
//     })
//
//     $('.timer').each(function () {
//         var $this = $(this);
//         jQuery({
//             Counter: 0
//         }).animate({
//             Counter: $this.text()
//         }, {
//             duration: 5000,
//             easing: 'swing',
//             step: function () {
//                 $this.text(Math.ceil(this.Counter));
//             }
//         });
//
//     });
//
//     $('.new_teacher_slick').slick({
//         infinite: true,
//         speed: 500,
//         fade: false,
//         rtl: true,
//         pauseOnHover: false,
//         cssEase: 'linear',
//         autoplay: false,
//         autoplaySpeed: 2000,
//         prevArrow: false,
//         nextArrow: false,
//         slidesToShow: 5,
//         slidesToScroll: 5,
//         centerMode: false,
//         responsive: [{
//             breakpoint: 400,
//             settings: {
//                 fade: false,
//                 autoplay: true,
//                 slidesToShow: 1,
//                 slidesToScroll: 1,
//                 centerMode: false,
//                 /* set centerMode to false to show complete slide instead of 3 */
//                 pauseOnHover: true,
//
//             }
//         },
//             {
//                 breakpoint: 768,
//                 settings: {
//                     fade: false,
//                     autoplay: true,
//                     slidesToShow: 2,
//                     slidesToScroll: 2,
//                     centerMode: false,
//                     /* set centerMode to false to show complete slide instead of 3 */
//                     pauseOnHover: true,
//                 }
//             },
//             {
//                 breakpoint: 900,
//                 settings: {
//                     fade: false,
//                     autoplay: true,
//                     slidesToShow: 3,
//                     slidesToScroll: 3,
//                     centerMode: false,
//                     /* set centerMode to false to show complete slide instead of 3 */
//                     pauseOnHover: true,
//                 }
//             },
//             {
//                 breakpoint: 1200,
//                 settings: {
//                     fade: false,
//                     autoplay: true,
//                     slidesToShow: 4,
//                     slidesToScroll: 4,
//                     centerMode: false,
//                     /* set centerMode to false to show complete slide instead of 3 */
//                     pauseOnHover: true,
//                 }
//             },
//         ]
//
//     });
//
//     $('#Basic_topics_slider').slick({
//         infinite: true,
//         speed: 500,
//         fade: false,
//         rtl: true,
//         pauseOnHover: false,
//         cssEase: 'linear',
//         autoplay: false,
//         autoplaySpeed: 2000,
//         prevArrow: false,
//         nextArrow: false,
//         slidesToShow: 4,
//         slidesToScroll: 4,
//         centerMode: false,
//         responsive: [{
//             breakpoint: 400,
//             settings: {
//                 fade: false,
//                 autoplay: true,
//                 slidesToShow: 1,
//                 slidesToScroll: 1,
//                 centerMode: false,
//                 /* set centerMode to false to show complete slide instead of 3 */
//                 pauseOnHover: true,
//
//             }
//         },
//             {
//                 breakpoint: 768,
//                 settings: {
//                     fade: false,
//                     autoplay: true,
//                     slidesToShow: 2,
//                     slidesToScroll: 2,
//                     centerMode: false,
//                     /* set centerMode to false to show complete slide instead of 3 */
//                     pauseOnHover: true,
//                 }
//             },
//             {
//                 breakpoint: 900,
//                 settings: {
//                     fade: false,
//                     autoplay: true,
//                     slidesToShow: 3,
//                     slidesToScroll: 3,
//                     centerMode: false,
//                     /* set centerMode to false to show complete slide instead of 3 */
//                     pauseOnHover: true,
//                 }
//             },
//             {
//                 breakpoint: 1200,
//                 settings: {
//                     fade: false,
//                     autoplay: true,
//                     slidesToShow: 3,
//                     slidesToScroll: 3,
//                     centerMode: false,
//                     /* set centerMode to false to show complete slide instead of 3 */
//                     pauseOnHover: true,
//                 }
//             },
//         ]
//
//     });
//
//     $('#blog_slider').slick({
//         infinite: true,
//         speed: 500,
//         fade: false,
//         rtl: true,
//         pauseOnHover: false,
//         cssEase: 'linear',
//         autoplay: false,
//         autoplaySpeed: 2000,
//         prevArrow: false,
//         nextArrow: false,
//         slidesToShow: 3,
//         slidesToScroll: 3,
//         centerMode: false,
//         responsive: [{
//             breakpoint: 400,
//             settings: {
//                 fade: false,
//                 autoplay: true,
//                 slidesToShow: 1,
//                 slidesToScroll: 1,
//                 centerMode: false,
//                 /* set centerMode to false to show complete slide instead of 3 */
//                 pauseOnHover: true,
//
//             }
//         },
//             {
//                 breakpoint: 768,
//                 settings: {
//                     fade: false,
//                     autoplay: true,
//                     slidesToShow: 1,
//                     slidesToScroll: 1,
//                     centerMode: false,
//                     /* set centerMode to false to show complete slide instead of 3 */
//                     pauseOnHover: true,
//                 }
//             },
//             {
//                 breakpoint: 1200,
//                 settings: {
//                     fade: false,
//                     autoplay: true,
//                     slidesToShow: 2,
//                     slidesToScroll: 2,
//                     centerMode: false,
//                     /* set centerMode to false to show complete slide instead of 3 */
//                     pauseOnHover: true,
//                 }
//             },
//         ]
//
//     });
//     $('#bild_teacher_profile').slick({
//         infinite: false,
//         speed: 500,
//         fade: false,
//         rtl: true,
//         pauseOnHover: false,
//         cssEase: 'linear',
//         autoplay: false,
//         autoplaySpeed: 2000,
//         prevArrow: $('.prev'),
//         nextArrow: $('.next'),
//         slidesToShow: 1,
//         slidesToScroll: 1,
//         centerMode: false,
//
//     });
//
//     var myVideo3 = document.getElementById("video3");
//
//     function playPause() {
//         if (myVideo3.paused) {
//             myVideo3.play();
//         } else {
//             myVideo3.pause();
//         }
//     }
//
//     $(document).on("click", ".about_as #video3", function () {
//         playPause();
//     })
//
//     $(document).on("click", ".about_as .iconPlay", function () {
//         $(this).hide();
//         $(".about_as .coverVideo").hide();
//         playPause();
//     })
//
//     $(document).on("click", ".have_question .head", function () {
//         $(".have_question").toggleClass("open")
//     })
//
//     $(document).on("click", ".linkLogin", function () {
//         $(".loginHome").addClass("show");
//     })
//
//     $(document).on("click", ".loginHome .closeX", function () {
//         $(".loginHome").removeClass("show");
//     })
//
//     $(document).mousedown(function (e) {
//         var container = $(".loginHome>div");
//         if (!container.is(e.target) && container.has(e.target).length === 0) {
//             $(".loginHome").removeClass("show");
//         }
//     });
//
// });
//
// /******************************************************************/
// // rate star
//
// $(document).on("mouseover", ".rateStar:not('.click') i", function () {
//     if ($(this).attr("num") == 1) {
//         $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
//     } else if ($(this).attr("num") == 2) {
//         $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
//         $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
//     } else if ($(this).attr("num") == 3) {
//         $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
//         $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
//         $($($(this).parents(".rateStar")).find($("i"))[2]).addClass("active")
//     } else if ($(this).attr("num") == 4) {
//         $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
//         $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
//         $($($(this).parents(".rateStar")).find($("i"))[2]).addClass("active")
//         $($($(this).parents(".rateStar")).find($("i"))[3]).addClass("active")
//     } else if ($(this).attr("num") == 5) {
//         $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
//         $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
//         $($($(this).parents(".rateStar")).find($("i"))[2]).addClass("active")
//         $($($(this).parents(".rateStar")).find($("i"))[3]).addClass("active")
//         $($($(this).parents(".rateStar")).find($("i"))[4]).addClass("active")
//     }
// })
//
// $(document).on("click", ".rateStar:not('.click') i", function () {
//     $($(this).parents(".rateStar")).addClass("click");
//     $($($(this).parents(".rateStar")).find("input")).val($(this).attr("num"))
// })
//
// $(document).on("click", ".rateStar.click i", function () {
//     $(this).removeClass("active")
//     $($(this).siblings("i")).removeClass("active")
//     $($(this).parents(".rateStar")).removeClass("click")
// })
//
// $(document).on("mouseout", ".rateStar:not('.click') i", function () {
//     $(this).removeClass("active")
//     $($(this).siblings("i")).removeClass("active")
// })
//
// $(document).on("click", ".dropHour ul li", function () {
//     $($($(this).parent("ul")).find("li")).removeClass("select")
//     $(this).addClass("select");
//     $($($(this).parents(".dropHour")).next("input")).val($($($(this).parents(".dropHour")).find(".ulHour li.select")).text().replace(/ /g, '') + ":" + $($($(this).parents(".dropHour")).find(".ulMin li.select")).text().replace(/ /g, ''))
// })
//
// $(document).on("focus", "td input", function () {
//     $($($(this).parents("td")).find(".dropHour")).addClass("show")
// })
//
// $(document).mousedown(function (e) {
//     var container = $(".dropHour");
//     if (!container.is(e.target) && container.has(e.target).length === 0) {
//         $(".dropHour").removeClass("show");
//     }
// });
//
// $(document).on("click", ".forgetPass", function () {
//     $(".loginHome").removeClass("login").addClass("remmber")
// })
//
// $(document).on("click", ".btnLog", function () {
//     $(".loginHome").removeClass("remmber").addClass("login")
// })
//
// $(document).on("click", ".btnJoin", function () {
//     $(".popJoinX").addClass("show")
// })
//
// $(document).on("click", ".popJoinX .closeX", function () {
//     $(".popJoinX").removeClass("show")
// })
//
// $(document).mousedown(function (e) {
//     var container = $(".popJoinX>div");
//     if (!container.is(e.target) && container.has(e.target).length === 0) {
//         $(".popJoinX").removeClass("show");
//     }
// });
//
// $(document).on("click", ".addRateTeacher a", function () {
//     $(".popUpRateTeacher").addClass("show")
// })
//
// $(document).on("click", ".popUpRateTeacher .closeX", function () {
//     $(".popUpRateTeacher").removeClass("show")
// })
//
// $(document).mousedown(function (e) {
//     var container = $(".popUpRateTeacher>div");
//     if (!container.is(e.target) && container.has(e.target).length === 0) {
//         $(".popUpRateTeacher").removeClass("show");
//     }
// });
//
// $(document).on("click", ".deleteAccount", function () {
//     $(".popUpDelete").addClass("show")
// })
//
// $(document).on("click", ".popUpDelete .closeX", function () {
//     $(".popUpDelete").removeClass("show")
// })
//
// $(document).mousedown(function (e) {
//     var container = $(".popUpDelete>div");
//     if (!container.is(e.target) && container.has(e.target).length === 0) {
//         $(".popUpDelete").removeClass("show");
//     }
// });
//
// $(window).scroll(function () {
//     var scroll = $(window).scrollTop();
//     if (scroll >= 50) {
//         $(".fixedInfoTeacher").addClass("down");
//     } else {
//         $(".fixedInfoTeacher").removeClass("down");
//     }
// });
//
// $(document).on("click", ".fixedInfoTeacher .rate", function () {
//     $(".popUpAddRate").addClass("show")
// })
//
// $(document).on("click", ".popUpAddRate .closeX", function () {
//     $(".popUpAddRate").removeClass("show")
// })
//
// $(document).mousedown(function (e) {
//     var container = $(".popUpAddRate>div");
//     if (!container.is(e.target) && container.has(e.target).length === 0) {
//         $(".popUpAddRate").removeClass("show");
//     }
// });
//
//
// /*************************/
// var phone_span_contantX;
// var phone_Number;
// $('#show_phone_Number').click(function () {
//     if ($(this).hasClass("numberShowing")) {
//         phone_Number = $($(this).find(".phone_Number"));
//         $($($(this).find(".phone_Number")).siblings(".phone_span_contant")).fadeToggle(10)
//         $($(this).find(".phone_Number")).fadeToggle(10)
//         $(this).removeClass("numberShowing")
//     } else {
//         phone_span_contantX = $($(this).find(".phone_span_contant"));
//         $($(this).find(".phone_span_contant")).fadeToggle(10)
//         $($(this).find(".phone_span_contant")).parents("#show_phone_Number").addClass("noVisible")
//         setTimeout(function () {
//             phone_span_contantX.parents("#show_phone_Number").removeClass("noVisible")
//
//
//             phone_span_contantX.parents("#show_phone_Number").addClass("numberShowing")
//             $(phone_span_contantX.siblings(".phone_Number")).fadeToggle(10)
//         }, 100)
//     }
// });
//
//
//
// // 2-1-2020
//
// $(document).on("keyup", "input", function () {
//     if ($(this).siblings(".listAutoSearch").length == 1) {
//         if ($(this).val().length > 0) {
//             $($(this).siblings(".listAutoSearch")).removeClass("none")
//         } else {
//             $($(this).siblings(".listAutoSearch")).addClass("none")
//         }
//     }
// })


/*********************************/
$(document).ready(function () {

    /********************/
    // script for student
    /********************/

    // script for student => header

    $(document).on("click", ".headerStudent .user", function () {
        $(".headerStudent .menuNotification").removeClass("active");
        $(".headerStudent .menuUser").toggleClass("active");
    })

    $(document).on("click", ".headerStudent .notification", function () {
        $(".headerStudent .menuUser").removeClass("active");
        $(".headerStudent .menuNotification").toggleClass("active");
    })

    $(document).mousedown(function (e) {
        var container = $(".headerStudent .menuUser");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".headerStudent .menuUser").removeClass("none");
        }
    });

    $(document).mousedown(function (e) {
        var container = $(".headerStudent .menuNotification");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".headerStudent .menuNotification").removeClass("none");
        }
    });

    $(document).on("click", ".sideRightStudent .keyList", function () {
        $(".sideRightStudent .listSideRight").slideToggle(400)
    })

    $(document).on("click", ".headerStudent .menuMobile .icon", function () {
        $(".sideRightStudent").toggleClass("active");
    })

    // script for student => contentStudent
    $(document).on("click", ".contentStudent .textInfo label", function () {
        $(this).siblings("input").removeAttr("disabled");
        $(this).siblings("select").removeAttr("disabled");
        $(this).siblings("textarea").removeAttr("disabled");
        $(".contentStudent .secInfo .btnsForm").removeClass("d-none");
    })

    // $(document).mousedown(function (e) {
    //     var container = $(".contentStudent .secInfo .textInfo .info label, .contentStudent .secInfo .textInfo .info input, .contentStudent .secInfo .textInfo .info textarea, .contentStudent .secInfo .textInfo .info .select2-search__field, .contentStudent .secInfo .textInfo .info .select2, .contentStudent .secInfo .btnsForm, .contentStudent .secInfo .educationSelect2");
    //     if (!container.is(e.target) && container.has(e.target).length === 0) {
    //         container.siblings("input").attr("disabled", "disabled");
    //         container.siblings("select").attr("disabled", "disabled");
    //         container.siblings("textarea").attr("disabled", "disabled");
    //     }
    // });


    $(document).on("click", ".contentStudent .secInfo .btnsForm>*", function () {
        $(".contentStudent .secInfo .btnsForm").addClass("d-none");
        $($(".contentStudent .btnEdit").parents(".title")).removeClass("barEdit");
        $($(".contentStudent .btnEdit").parents(".box")).removeClass("boxEdit");
        $($($(".contentStudent .btnEdit").parents(".title")).siblings(".show")).addClass("active");
        $($($(".contentStudent .btnEdit").parents(".title")).siblings(".edit")).removeClass("active");
        //
        // var container = $(".contentStudent .secInfo .textInfo .info label, .contentStudent .secInfo .textInfo .info input, .contentStudent .secInfo .textInfo .info textarea, .contentStudent .secInfo .textInfo .info .select2-search__field, .contentStudent .secInfo .textInfo .info .select2, .contentStudent .secInfo .btnsForm, .contentStudent .secInfo .educationSelect2");
        //         container.siblings("input").attr("disabled", "disabled");
        //         container.siblings("select").attr("disabled", "disabled");
        //         container.siblings("textarea").attr("disabled", "disabled");

    })

    $(document).on("click", ".contentStudent .btnEdit", function () {
        $($(this).parents(".title")).addClass("barEdit");
        $($(this).parents(".box")).addClass("boxEdit");
        $($($(this).parents(".title")).siblings(".show")).removeClass("active");
        $($($(this).parents(".title")).siblings(".edit")).addClass("active");
        $(".btnsForm").removeClass("d-none");
    })

    //
    // $(document).on("click", ".completeDataTeacher .completeDataPart .Delete", function () {
    //     $(this).parents(".rowTitleTeacher").remove()
    // })
    //
    // $(document).on("click", ".completeDataTeacher .addSubjectBtn", function () {
    //     $(".popUpAddSubject").addClass("show")
    // })
    //
    // $(document).on("click", ".popUpAddSubject .closeX, .popUpAddSubject .ok .submit", function () {
    //     $(".popUpAddSubject").removeClass("show")
    // })


    /*********************/
    var inputSub = $(".inputSub .divInput").html();
    var numClickAddAnotherInputSub = 0;
    $(document).on("click", ".addAnotherInputSub", function () {
        if (numClickAddAnotherInputSub < 2) {
            $(".inputSub").append('<div class="divInput"> ' + inputSub + '</div>');
            numClickAddAnotherInputSub = numClickAddAnotherInputSub + 1;
        }
    })

    $(document).on("click", ".popUpAddSubject .Delete", function() {
        $($(this).parents(".divInput")).remove();
        numClickAddAnotherInputSub = numClickAddAnotherInputSub - 1;
    })


    // $(document).on("click", ".contentStudent .btnsForm>*", function () {
    //     $($(this).parents(".title")).removeClass("barEdit");
    //     $($(this).parents(".box")).removeClass("boxEdit");
    //     $($($(this).parents(".title")).siblings(".show")).addClass("active");
    //     $($($(this).parents(".title")).siblings(".edit")).removeClass("active");
    // })

    $(document).on("click", ".contentStudent .nameEducation i", function () {
        // $($(this).parents(".nameEducation")).remove();
    })

    // script for student => completeDataStudent

    for (var i = 0; i < $(".completeDataStudent .input[requiredX]").length; i++) {
        $($($(".completeDataStudent .input[requiredX]")[i]).parent(".divInput")).after('<small class="error"></small>');
    }

    function testData(xl) {
        if ($(xl).attr("requiredX") == "yes") {
            if (xl.nodeName.toLowerCase() == "input" && $(xl).attr("type") != "radio" && $(xl).attr(
                "type") != "checkbox") {
                if ($(xl).val().length == 0) {
                    $(xl).addClass("error").removeClass("correct");
                    $($($(xl).parent(".divInput")).next("small.error")).show();
                    $($($(xl).parent(".divInput")).next("small.error")).text($(xl).attr("errorEmpty"));
                    $(xl).attr("true", "no");
                } else if ($(xl).val().length < $(xl).attr("min")) {
                    if ($(xl).attr("min")) {
                        $(xl).addClass("error").removeClass("correct");
                        $($($(xl).parent(".divInput")).next("small.error")).show();
                        $($($(xl).parent(".divInput")).next("small.error")).text($(xl).attr(
                            "errorMin"));
                        $(xl).attr("true", "no");
                    }
                } else if ($(xl).val().length > $(xl).attr("max")) {
                    if ($(xl).attr("max")) {
                        $(xl).addClass("error").removeClass("correct");
                        $($($(xl).parent(".divInput")).next("small.error")).show();
                        $($($(xl).parent(".divInput")).next("small.error")).text($(xl).attr(
                            "errorMax"));
                        $(xl).attr("true", "no");
                    }
                } else {
                    $(xl).addClass("correct").removeClass("error");
                    $($($(xl).parent(".divInput")).next("small.error")).hide();
                    $($($(xl).parent(".divInput")).next("small.error")).text();
                    $(xl).attr("true", "yes");
                }
            } else if (xl.nodeName.toLowerCase() == "select") {
                if ($(xl).val() == 0) {
                    $(xl).addClass("error").removeClass("correct");
                    $($($(xl).parent(".divInput")).next("small.error")).show();
                    $($($(xl).parent(".divInput")).next("small.error")).text($(xl).attr("errorEmpty"));
                    $(xl).attr("true", "no");
                } else if ($(xl).val() != 0) {
                    $(xl).addClass("correct").removeClass("error");
                    $($($(xl).parent(".divInput")).next("small.error")).hide();
                    $($($(xl).parent(".divInput")).next("small.error")).text();
                    $(xl).attr("true", "yes");
                }
            }

            if ($(xl).attr("type") == "email") {
                if (IsEmail($(xl).val()) == false) {
                    $(xl).addClass("error").removeClass("correct");
                    $($($(xl).parent(".divInput")).next("small.error")).show();
                    $($($(xl).parent(".divInput")).next("small.error")).text($(xl).attr("errorType"));
                    $(xl).attr("true", "no");
                    return false;
                } else {
                    $(xl).addClass("correct").removeClass("error");
                    $($($(xl).parent(".divInput")).next("small.error")).hide();
                    $($($(xl).parent(".divInput")).next("small.error")).text();
                    $(xl).attr("true", "yes");
                    return true;
                }
            }

            if ($(xl).attr("type") == "checkbox") {
                if ($(xl).is(':checked')) {
                    $(xl).attr("true", "yes");
                    $("[name='" + $(xl).attr("name") + "']").addClass("correct").removeClass("error");
                    $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .hide();
                    $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .text();
                } else {
                    $(xl).attr("true", "no");
                    $("[name='" + $(xl).attr("name") + "']").addClass("error").removeClass("correct");
                    $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .show();
                    $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .text($(xl).attr("errorEmpty"));
                }
            }

            if ($(xl).attr("type") == "radio") {
                if ($("[name='" + $(xl).attr("name") + "']:checked").length == 1) {
                    $("[name='" + $(xl).attr("name") + "']").attr("true", "yes");
                    $("[name='" + $(xl).attr("name") + "']").addClass("correct").removeClass("error");
                    $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .hide();
                    $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .text();
                } else {
                    $("[name='" + $(xl).attr("name") + "']").attr("true", "no");
                    $("[name='" + $(xl).attr("name") + "']").addClass("error").removeClass("correct");
                    $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .show();
                    $($($("[name='" + $(xl).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .text($(xl).attr("errorEmpty"));
                }
            }

        } else {
            $(xl).attr("true", "yes")
        }
    }

    setTimeout(function () {
        for (var i = 0; i < $($(".completeDataStudent .input")).length; i++) {
            if (($(".completeDataStudent .input")[i]).nodeName.toLowerCase() == "input" && $($(".completeDataStudent .input")[i]).attr(
                "type") != "radio" && $($(".completeDataStudent .input")[i]).attr("type") != "checkbox") {
                if ($($(".completeDataStudent .input")[i]).val() != "" && $($(".completeDataStudent .input")[i]).val() != 0) {
                    $($(".completeDataStudent .input")[i]).trigger("keyup")
                }
            }
        }

        for (var i = 0; i < $($(".completeDataStudent .input")).length; i++) {
            if (($(".completeDataStudent .input")[i]).nodeName.toLowerCase() == "input" && $($(".completeDataStudent .input")[i]).attr(
                "type") == "checkbox") {
                $($(".completeDataStudent .input")[i]).trigger("keyup")
            }
        }
    }, 500)

    $(document).on("click", ".submit", function () {
        $($($(this).parents("form")).find("input:not(#video), select, textarea")).trigger("change")
        $($($(this).parents("form")).find("input, select, textarea")).trigger("keydown")
        $($($(this).parents("form")).find("input, select, textarea")).trigger("keyup")
        $($($(this).parents("form")).find("input, select, textarea")).trigger("blur")
        for (var a = 0; a < 15; a++) {
            testData(xl = $($(this).parents("form").find(".input"))[a])
            testData(xl = $($(this).parents(".formPublic").find("input"))[a])
        }
    })

    $(document).on("keyup change blur", ".completeDataStudent .input[requiredX]:not(#phone2)", function () {
        testData(xl = this)
    })

    $(document).on("change keyup", ".completeDataStudent  .input:not(#phone2)", function () {
        for (var i = 0; i < $(".completeDataStudent .input:not(#phone2)").length; i++) {
            if ($($(".completeDataStudent .input:not(#phone2)")[i]).attr("true") != "yes") {
                x++;
            }
            if (i == $(".completeDataStudent .input:not(#phone2)").length - 1) {

                if (x != 0 && phoneStep2 == 0) {
                    $($(".completeDataStudent").find(".submit")).attr("type", "button");
                } else if (x == 0 && phoneStep2 == 1) {
                    $($(".completeDataStudent").find(".submit")).attr("type", "submit");
                }
                testData(xl = this)
                x = 0;
            }
        }
    })

    var phoneStep2 = 0;
    var valid205 = 0;

    $(document).on("keyup", "#phone2", function () {

        if ($(this).val().split("")[0] == 0 && $(this).val().split("")[1] == 5) {
            $(this).addClass("correct").removeClass("error");
            $($($(this).parent(".divInput")).next("small.error")).hide();
            $($($(this).parent(".divInput")).next("small.error")).text();
            $(this).attr("true", "yes");
            valid205 = 1;
        } else {
            $(this).addClass("error").removeClass("correct");
            $($($(this).parent(".divInput")).next("small.error")).show();
            $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("error05"));
            $(this).attr("true", "no");
            phoneStep2 = 0;
            valid205 = 0;
        }

        if (($(this).val().length == 10) && ($(this).val().split("")[0] == 0) && ($(this).val().split("")[1] == 5)) {
            $(this).addClass("correct").removeClass("error");
            $($($(this).parent(".divInput")).next("small.error")).hide();
            $($($(this).parent(".divInput")).next("small.error")).text();
            $(this).attr("true", "yes");
            phoneStep2 = 1;
        } else {
            if (valid205 == 1) {
                $(this).addClass("error").removeClass("correct");
                $($($(this).parent(".divInput")).next("small.error")).show();
                $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMin"));
                $(this).attr("true", "no");
            } else {
                $(this).addClass("error").removeClass("correct");
                $($($(this).parent(".divInput")).next("small.error")).show();
                $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("error05"));
                $(this).attr("true", "no");
            }
            phoneStep2 = 0;
        }

        for (var i = 0; i < $(".completeDataStudent .input").length; i++) {
            if ($($(".completeDataStudent .input")[i]).attr("true") != "yes") {
                x++;
            }
            if (i == $(".completeDataStudent .input").length - 1) {

                if (x != 0 && phoneStep2 == 0) {
                    $($(".completeDataStudent").find(".submit").remove()).attr("type", "button");

                } else if (x == 0 && phoneStep2 == 1) {
                    $($(".completeDataStudent").find(".submit")).attr("type", "submit");
                    // var element = document.getElementById("step1");
                    // element.classList.remove("submit");
                }
                testData(xl = this)
                x = 0;
            }
        }

        for (var i = 0; i < $(".formPublic input").length; i++) {
            if ($($(".formPublic input")[i]).attr("true") != "yes") {
                x++;
            }
            if (i == $(".formPublic input[requiredX]").length - 1) {

                if (x != 0 && phoneStep2 == 0) {
                    $($(".formPublic").find(".submit")).attr("type", "button");
                } else if (x == 0 && phoneStep2 == 1) {
                    $($(".formPublic").find(".submit")).attr("type", "submit");
                }
                testData(xl = this)
                x = 0;
            }
        }

    })

    var x = 0;

    for (var i = 0; i < $(".formPublic input").length; i++) {

        if ($($(".formPublic input")[i]).attr("requiredX") == "yes") {
            $($(".formPublic input")[i]).attr("true", "no");
        } else {
            $($(".formPublic input")[i]).attr("true", "yes");
        }
    };

    $(document).on("keyup change blur", ".formPublic input[requiredX]:not(#phone2)", function () {
        testData(xl = this)
    })

    $(document).on("change keyup", ".formPublic  input", function () {
        for (var i = 0; i < $(".formPublic input").length; i++) {
            if ($($(".formPublic input")[i]).attr("true") != "yes") {
                x++;
            }
            if (i == $(".formPublic input[requiredX]").length - 1) {

                if (x != 0 && phoneStep2 == 0) {
                    $($(".formPublic").find(".submit")).attr("type", "submit");
                } else if (x == 0 && phoneStep2 == 1) {
                    $($(".formPublic").find(".submit")).attr("type", "submit");
                }
                testData(xl = this)
                x = 0;
            }
        }
    })

    for (var i = 0; i < $(".formPublic input[requiredX]").length; i++) {
        $($($(".formPublic input[requiredX]")[i]).parent(".divInput")).after('<small class="error"></small>');
    }

    // ************



    // ************

    // for (var i = 0; i < $(".formPublic .input").length; i++) {

    //     if ($($(".formPublic .input")[i]).attr("requiredX") == "yes") {
    //         $($(".formPublic .input")[i]).attr("true", "no");
    //     } else {
    //         $($(".formPublic .input")[i]).attr("true", "yes");
    //     }
    // };

    // for (var i = 0; i < $(".formPublic .input[requiredX]").length; i++) {
    //     $($($(".formPublic .input[requiredX]")[i]).parent(".divInput")).after('<small class="error"></small>');
    // }

    // $(document).on("change keyup", ".formPublic  .input", function () {
    //     for (var i = 0; i < $(".formPublic .input").length; i++) {
    //         if ($($(".formPublic .input")[i]).attr("true") != "yes") {
    //             x++;
    //         }
    //         if (i == $(".formPublic .input").length - 1) {

    //             if (x != 0) {
    //                 $($(".formPublic").find(".submit")).attr("type", "button");
    //             } else if (x == 0) {
    //                 $($(".formPublic").find(".submit")).attr("type", "submit");
    //             }
    //             testData(xl = this)
    //             console.log(x)
    //             x = 0;
    //         }
    //     }
    // })

    $(document).on("change", ".imgFile", function () {
        readURL(this)
    })

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('img.imgFile').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    /********************/
    // script for teacher
    /********************/

    // script for teacher => header

    $(document).on("click", ".headerTeacher .user", function () {
        $(".headerTeacher .menuNotification").removeClass("active");
        $(".headerTeacher .menuUser").toggleClass("active");
    })

    $(document).mousedown(function (e) {
        var container = $(".contentTeacher .secInfo .select2");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
          //  container.siblings(".select2").attr("disabled", "disabled");
        }
    });

    $(document).on("click", ".headerTeacher .notification", function () {
        $(".headerTeacher .menuUser").removeClass("active");
        $(".headerTeacher .menuNotification").toggleClass("active");
    })

    $(document).mousedown(function (e) {
        var container = $(".headerTeacher .menuUser");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".headerTeacher .menuUser").removeClass("none");
        }
    });

    $(document).mousedown(function (e) {
        var container = $(".headerTeacher .menuNotification");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".headerTeacher .menuNotification").removeClass("none");
        }
    });

    // script for teacher => alert

    $(document).on("click", ".textAlertData", function () {
        if ($(window).outerWidth(true) <= 991.9) {
            $(".over").addClass("active");
        }
    })

    $(document).mousedown(function (e) {
        var container = $(".alertUser");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            if ($(window).outerWidth(true) <= 991.9) {
                setTimeout(function () {
                    $(".alertUser").removeClass("active");
                }, 200)
            } else {
                $(".alertUser").removeClass("active");
            }
        }
    });

    $(document).on("click", ".over.active,.over.show", function () {
        setTimeout(function () {
            $(".alertUser").removeClass("active");
            $(".over").removeClass("active show");
        }, 200)
    })

    // script for teacher => content

    $(document).on("click", ".contentTeacher .keyList", function () {
        $(".sideRightTeacher .listSideRight").slideToggle(400)
    })

    $(document).on("click", ".headerTeacher .menuMobile .icon", function () {
        $(".sideRightTeacher").toggleClass("active");
    })

    // $(document).on("click", ".contentTeacher .textInfo label", function () {
    //     $(this).siblings("input").removeAttr("disabled");
    //     $(this).siblings("textarea").removeAttr("disabled");
    // })
    //
    // $(document).mousedown(function (e) {
    //     var container = $(".contentTeacher .textInfo label, .contentTeacher .textInfo input, .contentTeacher .textInfo textarea");
    //     if (!container.is(e.target) && container.has(e.target).length === 0) {
    //         container.siblings("input").attr("disabled", "disabled");
    //         container.siblings("textarea").attr("disabled", "disabled");
    //     }
    // });

    $(document).on("click", ".contentTeacher .btnShowHide.less", function () {
        $($(this).next("p.less")).removeClass("less").addClass("more");
        $(this).removeClass("less").addClass("more");
    })

    $(document).on("click", ".contentTeacher .btnShowHide.more", function () {
        $($(this).next("p.more")).removeClass("more").addClass("less");
        $(this).removeClass("more").addClass("less");
    })

    var myVideo = document.getElementById("video1");

    function playPause() {
        if (myVideo.paused) {
            myVideo.play();
        } else {
            myVideo.pause();
        }
    }

    $(document).on("click", ".contentTeacher #video1", function () {
        playPause();
    })

    $(document).on("click", ".contentTeacher .iconPlay", function () {
        $(this).hide();
        $(".contentTeacher .coverVideo").hide();
        playPause();
    })

    $(document).on("click", ".contentTeacher .btnEdit", function () {
        $($(this).parents(".title")).addClass("barEdit");
        $($(this).parents(".box")).addClass("boxEdit");
        $($($(this).parents(".title")).siblings(".show")).removeClass("active");
        $($($(this).parents(".title")).siblings(".edit")).addClass("active");
    })

    $(document).on("click", ".contentTeacher .btns>*", function () {
        $($(this).parents(".title")).removeClass("barEdit");
        $($(this).parents(".box")).removeClass("boxEdit");
        $($($(this).parents(".title")).siblings(".show")).addClass("active");
        $($($(this).parents(".title")).siblings(".edit")).removeClass("active");
    })


    var i = 2;
    var eduHtml = $(".dataTeacher .edit .eduCol").html();

    $(document).on("click", ".dataTeacher .addEduCol>a", function () {
        $(".dataTeacher .eduCol").append(eduHtml.replaceAll('edu1-01', ('edu' + i + '-01')).replaceAll('edu1-02', ('edu' + i + '-02')).replaceAll('edu1-03', ('edu' + i + '-03')))
        i++;
    })


    // new delete
    $(document).on("click", ".completeDataTeacher .Delete", function () {
        $(this).parents(".rowTitleTeacher").remove()
        console.log($(this).parents(".rowTitleTeacher"))
    })

    $(document).on("click", ".addSubjectBtn", function () {
        $(".popUpAddSubject").addClass("show")
    })
    $(document).on("click", ".popUpAddSubject .closeX", function () {
        $(".popUpAddSubject").removeClass("show")
    })
    $(document).on("click", ".completeDataTeacher .noActive input", function () {
        if ($(this).is(':checked')) {
            $(".completeDataTeacher td." + $(this).attr("id") + " input").attr("disabled", "disabled");
        } else {
            $(".completeDataTeacher td." + $(this).attr("id") + " input").removeAttr("disabled");
        }
    })

    $(document).on("click", ".completeDataTeacher .addHours td span", function () {
        $('<tr class="rowFrom">' + $(".rowFrom").html() + '</tr>').insertBefore(".completeDataTeacher tr.addHours");
        $('<tr class="rowTo">' + $(".rowTo").html() + '</tr>').insertBefore(".completeDataTeacher tr.addHours");
        // rowFrom();
        // rowTo();
    })

    // new img

    $(document).on("change", ".imgFile", function () {
        readURL(this)
    })

    // change img
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('img.imgFile').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on("change", ".contentTeacher #city", function () {

        for (var i = 0; i < 100; i++) {
            if ($(this).val() == $($(".contentTeacher .nameCountry")[i]).text()) {
                $($(".contentTeacher .nameCountry")[i]).remove()
            }
        }

        $(".edit .country .listCountry").append('<span class="nameCountry">' + $(this).val() + '<i class="fal fa-times-circle"></i>' + '</span>');

        for (var i = 0; i < 100; i++) {
            if ($(this).val() == $($(".contentTeacher .nameCountry")[i]).text()) {
                $($(".contentTeacher .nameCountry")[i]).remove()
            }
        }

        // $(this).attr("data-value", $($($(this).next("datalist")).find("option[value='" + $(this).val() + "']")).attr("data-value"))

        $(this).val("")

    })

    $(document).on("click", ".nameCountry2 i", function () {
        $($(this).parents(".nameCountry2")).remove();
    })

    $(document).on("change", "#city2", function () {

        for (var i = 0; i < 10; i++) {
            if ($(this).val() == $($(".nameCountry2")[i]).text()) {
                $($(".nameCountry2")[i]).remove()
            }
        }

        $(".country2 .listCountry2").append('<span class="nameCountry2">' + $(this).val() + '<i class="fal fa-times-circle"></i>' + '</span>');

        $(this).val("");
        testStep3();

    })

    $(document).on("click", ".nameCountry2 i", function () {
        $($(this).parents(".nameCountry2")).remove();
        testStep3();
    })

    $(document).on("change", ".contentTeacher #education", function () {

        for (var i = 0; i < 10; i++) {
            if ($(this).val() == $($(".contentTeacher .nameEducation")[i]).text()) {
                $($(".contentTeacher .nameEducation")[i]).remove()
            }
        }

        $(".contentTeacher .edit .education .listEducation").append('<span class="nameEducation">' + $(this).val() + '<i class="fal fa-times-circle"></i>' + '</span>');

        $(this).val("")

    })

    $(document).on("click", ".go_for_higher", function () {
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
    })

    $(document).mousedown(function (e) {
        var container = $(".overUserChat");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".listUsers li .icon").removeClass("active");
            $(".overUserChat").removeClass("active");
        }
    });

    $(document).on("click", ".listUsers li .icon", function () {
        $(this).toggleClass("active");
        $($(this).find(".overUserChat")).toggleClass("active");
    })

    $(document).on("click", ".listUsers li", function (e) {
        $(".listUsers li").removeClass("active");
        $(this).addClass("active");
        showChat();
        if ($(window).outerWidth(true) <= 800) {
            if ($($(e.target).parents(".icon")).length !== 1) {
                $(".colList").removeClass("activeMob");
                $(".btnBack").css("display", "inline-flex");
            } else {
                $($(this).find(".icon")).toggleClass("active");
                $($($(this).parents(".icon")).find(".overUserChat")).toggleClass("active");
            }
        }
    })

    $(document).on("click", ".boxChat .btnBack", function () {
        if ($(window).outerWidth(true) <= 800) {
            $(".colList").addClass("activeMob")
            $(".btnBack").css("display", "none")
        }
    })

    function showChat() {
        for (var i = 0; i < $(".listUsers li").length; i++) {
            if ($($(".listUsers li")[i]).hasClass("active")) {
                $(".chatPerson").removeClass("active");
                $(".chatPerson." + $($(".listUsers li")[i]).attr("data-chat")).addClass("active")
            }
        }
    }

    showChat()

    function boxLoginTeacher() {
        if ($(window).outerWidth(true) >= 992) {
            $(".boxLoginTeacher .col2").height($(".boxLoginTeacher .col1>div").outerHeight(true))
        } else {
            $(".boxLoginTeacher .col2").css("height", "auto");
        }
    }

    boxLoginTeacher();

    $(window).resize(function () {
        boxLoginTeacher();
    })

    $(document).on("click", ".numsMobile .num1", function () {
        $(".numsMobile").attr("data-active", "1")
    })
    $(document).on("click", ".numsMobile .num2", function () {
        $(".numsMobile").attr("data-active", "2")
    })
    $(document).on("click", ".numsMobile .num3", function () {
        $(".numsMobile").attr("data-active", "3")
    })
    $(document).on("click", ".numsMobile .num4", function () {
        $(".numsMobile").attr("data-active", "4")
    })

    var x = 0;

    for (var i = 0; i < $(".formToJoinTeacher .input").length; i++) {

        if ($($(".formToJoinTeacher .input")[i]).attr("requiredX") == "yes") {
            $($(".formToJoinTeacher .input")[i]).attr("true", "no");
        } else {
            $($(".formToJoinTeacher .input")[i]).attr("true", "yes");
        }
    };

    /**********************/
    /* login Teacher */
    /**********************/

    for (var i = 0; i < $(".boxLoginTeacher .input[requiredX]").length; i++) {
        $($($(".boxLoginTeacher .input[requiredX]")[i]).parent(".divInput")).after('<small class="error"></small>');
    }

    var phoneStep3 = 0;
    var valid305 = 0;

    $(document).on("keyup", "#teacher_mobile", function () {

        if ($(this).val().split("")[0] == 0 && $(this).val().split("")[1] == 5) {
            $(this).addClass("correct").removeClass("error");
            $($($(this).parent(".divInput")).next("small.error")).hide();
            $($($(this).parent(".divInput")).next("small.error")).text();
            $(this).attr("true", "yes");
            valid305 = 1;
        } else {
            $(this).addClass("error").removeClass("correct");
            $($($(this).parent(".divInput")).next("small.error")).show();
            $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("error05"));
            $(this).attr("true", "no");
            phoneStep3 = 0;
            valid305 = 0;
        }

        if (($(this).val().length == 10) && ($(this).val().split("")[0] == 0) && ($(this).val().split("")[1] == 5)) {
            $(this).addClass("correct").removeClass("error");
            $($($(this).parent(".divInput")).next("small.error")).hide();
            $($($(this).parent(".divInput")).next("small.error")).text();
            $(this).attr("true", "yes");
            phoneStep3 = 1;
        } else {
            if (valid305 == 1) {
                $(this).addClass("error").removeClass("correct");
                $($($(this).parent(".divInput")).next("small.error")).show();
                $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMin"));
                $(this).attr("true", "no");
            } else {
                $(this).addClass("error").removeClass("correct");
                $($($(this).parent(".divInput")).next("small.error")).show();
                $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("error05"));
                $(this).attr("true", "no");
            }
            phoneStep3 = 0;
        }

        for (var i = 0; i < $(".boxLoginTeacher form .input").length; i++) {
            if ($($(".boxLoginTeacher form .input")[i]).attr("true") != "yes") {
                x++;
            }
            if (i == $(".boxLoginTeacher form .input").length - 1) {

                if (x != 0 && phoneStep3 == 0) {
                    $($(".boxLoginTeacher form").find(".submit")).attr("type", "button");
                } else if (x == 0 && phoneStep3 == 1) {
                    $($(".boxLoginTeacher form").find(".submit")).attr("type", "submit");
                }
                x = 0;
            }
        }


    })


    $(document).on("keyup change blur", ".boxLoginTeacher form .input[requiredX]:not(#teacher_mobile)", function () {

        if ($(this).attr("requiredX") == "yes") {
            if ($(this).val().length == 0) {
                $(this).addClass("error").removeClass("correct");
                $($($(this).parent(".divInput")).next("small.error")).show();
                $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorEmpty"));
                $(this).attr("true", "no");
            } else if ($(this).val().length < $(this).attr("min")) {
                if ($(this).attr("min")) {
                    $(this).addClass("error").removeClass("correct");
                    $($($(this).parent(".divInput")).next("small.error")).show();
                    $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMin"));
                    $(this).attr("true", "no");
                }
            } else if ($(this).val().length > $(this).attr("max")) {
                if ($(this).attr("max")) {
                    $(this).addClass("error").removeClass("correct");
                    $($($(this).parent(".divInput")).next("small.error")).show();
                    $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMax"));
                    $(this).attr("true", "no");
                }
            } else {
                $(this).addClass("correct").removeClass("error");
                $($($(this).parent(".divInput")).next("small.error")).hide();
                $($($(this).parent(".divInput")).next("small.error")).text();
                $(this).attr("true", "yes");
            }

            if ($(this).attr("type") == "email") {
                if (IsEmail($(this).val()) == false) {
                    $(this).addClass("error").removeClass("correct");
                    $($($(this).parent(".divInput")).next("small.error")).show();
                    $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorType"));
                    $(this).attr("true", "no");
                    return false;
                } else {
                    $(this).addClass("correct").removeClass("error");
                    $($($(this).parent(".divInput")).next("small.error")).hide();
                    $($($(this).parent(".divInput")).next("small.error")).text();
                    $(this).attr("true", "yes");
                    return true;
                }
            }

            if ($(this).attr("type") == "checkbox") {
                if ($(this).is(':checked')) {
                    $(this).attr("true", "yes");
                } else {
                    $(this).attr("true", "no");
                }
            }
        } else {
            $(this).attr("true", "yes")
        }

        for (var i = 0; i < $(".boxLoginTeacher form .input:not(#teacher_mobile)").length; i++) {
            if ($($(".boxLoginTeacher form .input")[i]).attr("true") != "yes") {
                x++;
            }
            if (i == $(".boxLoginTeacher form .input").length - 1) {

                if (x != 0) {
                    $($(".boxLoginTeacher form").find(".submit")).attr("type", "button");
                } else {
                    $($(".boxLoginTeacher form").find(".submit")).attr("type", "submit");
                }
                x = 0;
            }
        }

    })
    var x = 0;

    for (var i = 0; i < $(".boxLoginTeacher form .input").length; i++) {

        if ($($(".boxLoginTeacher form .input")[i]).attr("requiredX") == "yes") {
            $($(".boxLoginTeacher form .input")[i]).attr("true", "no");
        } else {
            $($(".boxLoginTeacher form .input")[i]).attr("true", "yes");
        }
    };

    /**********************/
    /* complete Data Teacher Step 1 to 5 */
    /**********************/


    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(email)) {
            return false;
        } else {
            return true;
        }
    }

    setTimeout(function () {
        for (var i = 0; i < $(".completeDataTeacher .input").length; i++) {
            $($($(".completeDataTeacher .input")[i]).parents(".divInput")).after(
                '<small class="error"></small>');
        }
    }, 250)

    $(document).on("keyup change blur", ".completeDataTeacher .input[requiredX]", function () {

        if ($(this).attr("requiredX") == "yes") {
            if (this.nodeName.toLowerCase() == "input" && $(this).attr("type") != "radio" && $(this).attr(
                "type") != "checkbox") {
                if ($(this).val().length == 0) {
                    $(this).addClass("error").removeClass("correct");
                    $($($(this).parent(".divInput")).next("small.error")).show();
                    $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorEmpty"));
                    $(this).attr("true", "no");
                    console.log("1")
                } else if ($(this).val().length < $(this).attr("min")) {
                    if ($(this).attr("min")) {
                        $(this).addClass("error").removeClass("correct");
                        $($($(this).parent(".divInput")).next("small.error")).show();
                        $($($(this).parent(".divInput")).next("small.error")).text($(this).attr(
                            "errorMin"));
                        $(this).attr("true", "no");
                    }
                    console.log("2")
                } else if ($(this).attr("name") == "discount_percentage") {
                    if ($(this).val() < $(this).attr("min")) {
                        if ($(this).attr("min")) {
                            $(this).addClass("error").removeClass("correct");
                            $($($(this).parent(".divInput")).next("small.error")).show();
                            $($($(this).parent(".divInput")).next("small.error")).text($(this).attr(
                                "errorMin"));
                            $(this).attr("true", "no");
                        }
                        console.log("02")
                    } else {
                        $(this).addClass("correct").removeClass("error");
                        $($($(this).parent(".divInput")).next("small.error")).hide();
                        $($($(this).parent(".divInput")).next("small.error")).text();
                        $(this).attr("true", "yes");
                    }
                } else if ($(this).val().length > $(this).attr("max")) {
                    if ($(this).attr("max")) {
                        $(this).addClass("error").removeClass("correct");
                        $($($(this).parent(".divInput")).next("small.error")).show();
                        $($($(this).parent(".divInput")).next("small.error")).text($(this).attr(
                            "errorMax"));
                        $(this).attr("true", "no");
                    }
                    console.log("3")
                } else {
                    $(this).addClass("correct").removeClass("error");
                    $($($(this).parent(".divInput")).next("small.error")).hide();
                    $($($(this).parent(".divInput")).next("small.error")).text();
                    $(this).attr("true", "yes");
                    console.log("4")
                }
            } else if (this.nodeName.toLowerCase() == "select") {
                if ($(this).val() == 0) {
                    $(this).addClass("error").removeClass("correct");
                    $($($(this).parent(".divInput")).next("small.error")).show();
                    $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorEmpty"));
                    $(this).attr("true", "no");
                    console.log("5")
                } else if ($(this).val() != 0) {
                    $(this).addClass("correct").removeClass("error");
                    $($($(this).parent(".divInput")).next("small.error")).hide();
                    $($($(this).parent(".divInput")).next("small.error")).text();
                    $(this).attr("true", "yes");
                    console.log("6")
                }
            }

            if ($(this).attr("type") == "email") {
                if (IsEmail($(this).val()) == false) {
                    $(this).addClass("error").removeClass("correct");
                    $($($(this).parent(".divInput")).next("small.error")).show();
                    $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorType"));
                    $(this).attr("true", "no");
                    console.log("7")
                    return false;
                } else {
                    $(this).addClass("correct").removeClass("error");
                    $($($(this).parent(".divInput")).next("small.error")).hide();
                    $($($(this).parent(".divInput")).next("small.error")).text();
                    $(this).attr("true", "yes");
                    console.log("8")
                    return true;
                }
            }

            if ($(this).attr("type") == "checkbox") {
                if ($(this).is(':checked')) {
                    $(this).attr("true", "yes");
                    $("[name='" + $(this).attr("name") + "']").addClass("correct").removeClass("error");
                    $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .hide();
                    $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .text();
                    console.log("9")
                } else {
                    $(this).attr("true", "no");
                    $("[name='" + $(this).attr("name") + "']").addClass("error").removeClass("correct");
                    $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .show();
                    $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .text($(this).attr("errorEmpty"));
                    console.log("10")
                }
            }

            if ($(this).attr("type") == "radio") {
                if ($(this).is(':checked')) {
                    $("[name='" + $(this).attr("name") + "']").attr("true", "yes");
                    $("[name='" + $(this).attr("name") + "']").addClass("correct").removeClass("error");
                    $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .hide();
                    $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .text();
                    console.log("11")
                } else {
                    $("[name='" + $(this).attr("name") + "']").attr("true", "no");
                    $("[name='" + $(this).attr("name") + "']").addClass("error").removeClass("correct");
                    $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .show();
                    $($($("[name='" + $(this).attr("name") + "']").parent(".divInput")).next("small.error"))
                        .text($(this).attr("errorEmpty"));
                    console.log("12")
                }
            }

        } else {
            $(this).attr("true", "yes")
        }

        for (var i = 0; i < $(".completeDataTeacher form.formToJoinTeacher input").length; i++) {
            if ($($(".completeDataTeacher form.formToJoinTeacher input")[i]).attr("true") != "yes") {
                x++;
            }
            if (i == $(".completeDataTeacher form.formToJoinTeacher input").length - 1) {

                if (x != 0) {
                    $($(".completeDataTeacher form.formToJoinTeacher").find(".submit")).attr("type",
                        "button");
                } else {
                    $($(".completeDataTeacher form.formToJoinTeacher").find(".submit")).attr("type",
                        "submit");

                }
                x = 0;
            }
        }

    })

    var x = 0;

    $(document).on("change keyup", ".step1 .input", function () {
        for (var i = 0; i < $(".completeDataTeacher .dataTeacher .step1 .input:not(#phone)").length; i++) {
            if ($($(".completeDataTeacher .dataTeacher .step1 .input:not(#phone)")[i]).attr("true") != "yes") {
                x++;
            }
            if (i == $(".completeDataTeacher .dataTeacher .step1 .input:not(#phone)").length - 1) {

                if (x != 0) {
                    $($(".completeDataTeacher .dataTeacher .step1").find(".submit")).attr("type",
                        "button");

                } else if (x == 0 && phoneStep1 == 1) {
                    $($(".completeDataTeacher .dataTeacher .step1").find(".submit")).attr("type",
                        "submit");

                }
                x = 0;
            }
        }
    })

    var phoneStep1 = 0;
    var valid05 = 0;

    $(document).on("keyup", "#phone", function () {

        if ($(this).val().split("")[0] == 0 && $(this).val().split("")[1] == 5) {
            $(this).addClass("correct").removeClass("error");
            $($($(this).parent(".divInput")).next("small.error")).hide();
            $($($(this).parent(".divInput")).next("small.error")).text();
            $(this).attr("true", "yes");
            valid05 = 1;
        } else {
            $(this).addClass("error").removeClass("correct");
            $($($(this).parent(".divInput")).next("small.error")).show();
            $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("error05"));
            $(this).attr("true", "no");
            phoneStep1 = 0;
            valid05 = 0;
        }

        if ($(this).val().length == 10) {
            $(this).addClass("correct").removeClass("error");
            $($($(this).parent(".divInput")).next("small.error")).hide();
            $($($(this).parent(".divInput")).next("small.error")).text();
            $(this).attr("true", "yes");
        } else {
            if (valid05 == 1) {
                $(this).addClass("error").removeClass("correct");
                $($($(this).parent(".divInput")).next("small.error")).show();
                $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMin"));
            }
        }
    })

    for (var i = 0; i < $(".completeDataTeacher .dataTeacher .input").length; i++) {

        if ($($(".completeDataTeacher .dataTeacher .input")[i]).attr("requiredX") == "yes") {
            $($(".completeDataTeacher .dataTeacher .input")[i]).attr("true", "no");
        } else {
            $($(".completeDataTeacher .dataTeacher .input")[i]).attr("true", "yes");
        }
    };

    //////////////////Step 2///////////////////

    var x2 = 0;

    $(document).on("change keyup", ".step2 .input", function () {
        for (var i = 0; i < $(".completeDataTeacher .dataTeacher .step2 .input").length; i++) {
            if ($($(".completeDataTeacher .dataTeacher .step2 .input")[i]).attr("true") != "yes") {
                x2++;
            }
            if (i == $(".completeDataTeacher .dataTeacher .step2 .input").length - 1) {

                if (x2 != 0) {
                    $($(".completeDataTeacher .dataTeacher .step2").find(".submit")).attr("type", "button");
                } else {
                    $($(".completeDataTeacher .dataTeacher .step2").find(".submit")).attr("type", 'submit');
                }
                x2 = 0;
            }
        }
    })

    /////////////////////////////step3///////////////////////////

    var y = 0;

    // $(document).find(".completeDataTeacher .step3 .input[requiredx='yes']").trigger("change")

    $(document).on("change keyup", ".completeDataTeacher .step3 .input[requiredx='yes']", function () {

        for (var i = 0; i < $(".completeDataTeacher .dataTeacher .step3 .input[type='text']").length; i++) {
            if ($($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).val() == '') {
                $($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).attr("true",
                    "no");
                y++;
            } else {
                $($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).attr("true",
                    "yes");
            }
            if (i == $(".completeDataTeacher .dataTeacher .step3 .input[type='text']").length - 1) {
                if (y != 0) {
                    $($(".completeDataTeacher .dataTeacher .step3").find(".submit")).attr("type",
                        "button");
                } else if (y == 0) {
                    $($(".completeDataTeacher .dataTeacher .step3").find(".submit")).attr("type",
                        "submit");
                }
                y = 0;
            }
        }

    })

    $(document).on("change keyup", ".completeDataTeacher .step3 .input[requiredx='yes']", function () {
        if ($(this).val() == '') {
            $($(this).parents(".inputPrice")).removeClass("bcGreen").addClass("bcRed");
            $($($(this).parents(".divInput")).next("small")).show()
            $($(this).parents(".divInput ")).next("small").text($(this).attr("errorempty"))
        } else {
            $($(this).parents(".inputPrice")).removeClass("bcRed").addClass("bcGreen");
            $($($(this).parents(".divInput")).next("small")).hide()
        }
    })

    setTimeout(function () {

        for (var i = 0; i < $(".completeDataTeacher .step3 input[type='checkbox']").length; i++) {
            if ($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).is(':checked')) {
                $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
                    "input[type='text']")).attr("true", "no");
                $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
                    "input[type='text']")).attr("requiredX", "yes");
            } else {
                $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
                    "input[type='text']")).attr("true", "yes");
                $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
                    "input[type='text']")).attr("requiredX", "no");
            }
        };

    }, 100);

    $(document).on("change", ".completeDataTeacher .step3 input[type='checkbox']", function () {

        for (var i = 0; i < $(".completeDataTeacher .step3 input[type='checkbox']").length; i++) {
            if ($($(".step3 input[type='checkbox']")[i]).is(':checked')) {
                if ($($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
                    "input[type='text']")).val() == "") {

                    $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
                        "input[type='text']")).attr("true", "no");

                    $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
                        "input[type='text']")).attr("requiredX", "yes");
                }
            } else {
                $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
                    "input[type='text']")).attr("true", "yes");
                $($($($(".completeDataTeacher .step3 input[type='checkbox']")[i]).parent()).find(
                    "input[type='text']")).attr("requiredX", "no");
            }
        };

        for (var i = 0; i < $(".completeDataTeacher .dataTeacher .step3 .input[type='text']").length; i++) {
            if ($($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).val() == '') {
                $($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).attr("true",
                    "no");
                y++;
            } else {
                $($(".completeDataTeacher .dataTeacher .step3 .input[requiredx='yes']")[i]).attr("true",
                    "yes");
            }
            if (i == $(".completeDataTeacher .dataTeacher .step3 .input[type='text']").length - 1) {
                if (y != 0) {
                    $($(".completeDataTeacher .dataTeacher .step3").find(".submit")).attr("type",
                        "button");
                } else if (y == 0) {
                    $($(".completeDataTeacher .dataTeacher .step3").find(".submit")).attr("type",
                        "submit");
                }
                y = 0;
            }
        }
    });

    //////////////////////////////Step4///////////////////////

    $(document).on("keyup blur", ".completeDataTeacher .textMarketStep4", function () {
        $(".completeDataTeacher .step4 .limitText b").text($(this).val().length)
        if ($(this).val().length == 0) {
            check2Step4 = 0;
            $(".step4 .error2").show()
            $(this).addClass("bcRed").removeClass("bcGreen")
        } else {
            check2Step4 = 1;
            $(".step4 .error2").hide()
            $(this).addClass("bcGreen").removeClass("bcRed")
        }
    })

    var z = 0;

    $(document).on("change", ".completeDataTeacher .step4 .exp .input", function () {

        if ($($(".completeDataTeacher .step4 .exp .input")).is(':checked')) {
            check1Step4 = 1;
            $(".step4 .error1").hide()
        } else {
            check1Step4 = 0;
            $(".step4 .error1").show()
        }

    });

    /*step4 teacher cv_text required/***************************/
    // $(document).on("keyup blur", ".completeDataTeacher .aboutMe textarea", function () {
    //     if ($(this).val().length == 0) {
    //         check4Step4 = 0;
    //         $(".step4 .error4").show()
    //         $(this).addClass("bcRed").removeClass("bcGreen")
    //     } else {
    //         check4Step4 = 1;
    //         $(".step4 .error4").hide()
    //         $(this).addClass("bcGreen").removeClass("bcRed")
    //     }
    // })

    $(document).on("change", ".completeDataTeacher .video input", function (e) {

        var $source = $('.completeDataTeacher #video_here video');
        $source[0].src = URL.createObjectURL(this.files[0]);

        $(".completeDataTeacher #video_here i").hide();
        $('.completeDataTeacher #video_here video').show();

        if ($($(this)[0].files).length == 0) {
            check3Step4 = 1; // video validation
            $(".step4 .error3").hide()
        } else {
            check3Step4 = 1;
            $(".step4 .error3").hide()
        }
    })

    var check1Step4 = 0;
    var check2Step4 = 0;
    var check3Step4 = 1;
    var check4Step4 = 0;

    $(document).on("mousemove", ".completeDataTeacher .step4 ", function () {
        if (check1Step4 == 1 && check2Step4 == 1 && check3Step4 == 1  /* && check4Step4 == 1 */ ) {
            $($(".completeDataTeacher .dataTeacher .step4").find(".submit")).attr("type", "submit")
        } else {
            $($(".completeDataTeacher .dataTeacher .step4").find(".submit")).attr("type", "button")
        }
    })

    ///////////////////////////////

    $(document).on("click", ".completeDataTeacher .dataTeacher .step1 .ok .submit[type='submit']", function () {
        $(".completeDataTeacher .rowStep").attr("step", "2");
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
    })

    $(document).on("click", ".completeDataTeacher .dataTeacher .step2 .ok .submit[type='submit']", function () {
        $(".completeDataTeacher .rowStep").attr("step", "3");
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
    })

    $(document).on("click", ".completeDataTeacher .dataTeacher .step3 .ok .submit[type='submit']", function () {
        $(".completeDataTeacher .rowStep").attr("step", "4");
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
    })

    $(document).on("click", ".completeDataTeacher .dataTeacher .step4 .ok.submit[type='submit']", function () {
        $(".completeDataTeacher .rowStep").attr("step", "5");
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
    })

    var x = 0;

    $(document).on("change keyup", ".step1 .input", function () {
        for (var i = 0; i < $(".completeDataTeacher .dataTeacher .step1 .input").length; i++) {
            if ($($(".completeDataTeacher .dataTeacher .step1 .input")[i]).attr("true") != "yes") {
                x++;
            }
            if (i == $(".completeDataTeacher .dataTeacher .step1 .input").length - 1) {

                if (x != 0) {
                    $($(".completeDataTeacher .dataTeacher .step1").find(".submit")).attr("type", "button");
                } else {
                    $($(".completeDataTeacher .dataTeacher .step1").find(".submit")).attr("type", "submit");
                    document.getElementById("step1").classList.remove('submit');

                }
                x = 0;
            }
        }
    })

    $(document).on("click", ".completeDataTeacher .dataTeacher .step1 .ok .submit[type='submit']", function () {
        $(".completeDataTeacher .rowStep").attr("step", "2");
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
    })

    // $(document).on("click", ".submit", function () {
    //     // $($($(this).parents("form")).find("input:not(#video), select, textarea")).trigger("change")
    //     // $($($(this).parents("form")).find("input, select, textarea")).trigger("keydown")
    //     // $($($(this).parents("form")).find("input, select, textarea")).trigger("keyup")
    //     // $($($(this).parents("form")).find("input, select, textarea")).trigger("blur")
    //     testData(xl = $($(this).parents("form")).find("input:not(#video), select, textarea"))
    //     console.log(xl)
    // })

    setTimeout(function () {
        for (var i = 0; i < $($(".step1 .input")).length; i++) {
            if (($(".step1 .input")[i]).nodeName.toLowerCase() == "input" && $($(".step1 .input")[i]).attr(
                "type") != "radio" && $($(".step1 .input")[i]).attr("type") != "checkbox") {
                if ($($(".step1 .input")[i]).val() != "" && $($(".step1 .input")[i]).val() != 0) {
                    $($(".step1 .input")[i]).trigger("keyup")
                }
            }
        }

        for (var i = 0; i < $($(".step1 .input")).length; i++) {
            if (($(".step1 .input")[i]).nodeName.toLowerCase() == "input" && $($(".step1 .input")[i]).attr(
                "type") == "checkbox") {
                $($(".step1 .input")[i]).trigger("keyup")
            }
        }
    }, 500)

    // var inputSub = $(".inputSub .divInput").html();
    //
    // $(document).on("click", ".addAnotherInputSub", function () {
    //     $(".inputSub").append('<div class="divInput"> ' + inputSub + '</div>')
    // })

    $(document).on("click", ".btnDeleteHour", function () {
        $($($(this).parents("tr.rowTo")[0]).prev()).remove();
        $($(this).parents("tr.rowTo")[0]).remove();
    })

    function preventNumberInput(e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107) {
            e.preventDefault();
        }
    }

    $(document).ready(function () {
        $(document).on("keypress", "td input", function (e) {
            preventNumberInput(e);
        });
    })


    //***popup deleteAccount**//
    $(document).on("click", ".deleteAccount", function () {
        $(".popUpDelete").addClass("show");
    })

    //***popup editWorkHours**//
    $(document).on("click", ".editWorkHours", function () {
        $(".popUpsd").addClass("show");
    })

    $(document).on("click", ".popUpFromTeacher.editWorkHours .closeX", function () {
        $(".popUpsd").removeClass("show");
    })
    /********************************************/
    /* script for visitor */
    /********************************************/

    // click User Menu
    $(document).on("click", ".headerVisitor .addToUs", function () {
        $(".headerVisitor .menuAddToUs").toggleClass("active");
    })


    //***popup**//
    $(document).on("click", ".headerVisitor .joinAsStudent,.studentReg ", function () {
        $(".popUpJoinAsTeacher.popUpFromVisitor1").addClass("show");
    })

    $(document).on("click", ".popUpJoinAsTeacher .closeX", function () {
        $(".popUpJoinAsTeacher.popUpFromVisitor1").removeClass("show");
    })

    $(document).mousedown(function (e) {
        var container = $(".popUpJoinAsTeacher.popUpFromVisitor1>div");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".popUpJoinAsTeacher.popUpFromVisitor1").removeClass("show");
        }
    });

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(email)) {
            return false;
        } else {
            return true;
        }
    }

    // for (var i = 0; i < $(".popUpJoinAsTeacher.formPublic input[requiredX]").length; i++) {
    //     $($($("input[requiredX]")[i]).parent(".divInput")).after('<small class="error"></small>');
    // }

    // $(document).on("keyup change blur", ".popUpJoinAsTeacher.formPublic input[requiredX]", function () {

    //     if ($(this).attr("requiredX") == "yes") {
    //         if ($(this).val().length == 0) {
    //             $(this).addClass("error").removeClass("correct");
    //             $($($(this).parent(".divInput")).next("small.error")).show();
    //             $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorEmpty"));
    //             $(this).attr("true", "no");
    //         } else if ($(this).val().length < $(this).attr("min")) {
    //             if ($(this).attr("min")) {
    //                 $(this).addClass("error").removeClass("correct");
    //                 $($($(this).parent(".divInput")).next("small.error")).show();
    //                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMin"));
    //                 $(this).attr("true", "no");
    //             }
    //         } else if ($(this).val().length > $(this).attr("max")) {
    //             if ($(this).attr("max")) {
    //                 $(this).addClass("error").removeClass("correct");
    //                 $($($(this).parent(".divInput")).next("small.error")).show();
    //                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorMax"));
    //                 $(this).attr("true", "no");
    //             }
    //         } else {
    //             $(this).addClass("correct").removeClass("error");
    //             $($($(this).parent(".divInput")).next("small.error")).hide();
    //             $($($(this).parent(".divInput")).next("small.error")).text();
    //             $(this).attr("true", "yes");
    //         }

    //         if ($(this).attr("type") == "email") {
    //             if (IsEmail($(this).val()) == false) {
    //                 $(this).addClass("error").removeClass("correct");
    //                 $($($(this).parent(".divInput")).next("small.error")).show();
    //                 $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorType"));
    //                 $(this).attr("true", "no");
    //                 return false;
    //             } else {
    //                 $(this).addClass("correct").removeClass("error");
    //                 $($($(this).parent(".divInput")).next("small.error")).hide();
    //                 $($($(this).parent(".divInput")).next("small.error")).text();
    //                 $(this).attr("true", "yes");
    //                 return true;
    //             }
    //         }

    //         if ($(this).attr("type") == "checkbox") {
    //             if ($(this).is(':checked')) {
    //                 $(this).attr("true", "yes");
    //             } else {
    //                 $(this).attr("true", "no");
    //             }
    //         }
    //     } else {
    //         $(this).attr("true", "yes")
    //     }

    //     for (var i = 0; i < $(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input").length; i++) {
    //         if ($($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input")[i]).attr("true") != "yes") {
    //             x++;
    //         }
    //         if (i == $(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input").length - 1) {

    //             if (x != 0) {
    //                 $($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin").find(".submit")).attr("disabled", "disabled");
    //             } else {
    //                 $($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin").find(".submit")).removeAttr("disabled");
    //             }
    //             x = 0;
    //         }
    //     }

    // })
    // var x = 0;

    // for (var i = 0; i < $(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input").length; i++) {

    //     if ($($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input")[i]).attr("requiredX") == "yes") {
    //         $($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input")[i]).attr("true", "no");
    //     } else {
    //         $($(".popUpJoinAsTeacher.popUpFromVisitor1 form.formToJoin input")[i]).attr("true", "yes");
    //     }
    // };

})

/* Home */
/* Home */
/* Home */
/* Home */

$(document).ready(function () {

    $('.slick').slick({
        infinite: true,
        speed: 10000,
        fade: true,
        rtl: true,
        pauseOnHover: false,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 10000,
        prevArrow: false,
        nextArrow: false,
    });

    function ratingSkills() {
        if ($(window).outerWidth(true) >= 992) {
            $('.ratings_slick').not('.slick-initialized').slick({
                infinite: true,
                speed: 500,
                fade: false,
                rtl: true,
                pauseOnHover: false,
                cssEase: 'linear',
                autoplay: false,
                autoplaySpeed: 2000,
                prevArrow: false,
                nextArrow: false,
                slidesToShow: 3,
                slidesToScroll: 3,
                centerMode: false,
                responsive: [{
                    breakpoint: 400,
                    settings: {
                        fade: false,
                        autoplay: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: false,
                        /* set centerMode to false to show complete slide instead of 3 */
                        pauseOnHover: true,
                    }
                },
                    {
                        breakpoint: 768,
                        settings: {
                            fade: false,
                            autoplay: true,
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            centerMode: false,
                            /* set centerMode to false to show complete slide instead of 3 */
                            pauseOnHover: true,
                        }
                    },
                    {
                        breakpoint: 1200,
                        settings: {
                            fade: false,
                            autoplay: true,
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            centerMode: false,
                            /* set centerMode to false to show complete slide instead of 3 */
                            pauseOnHover: true,
                        }
                    },
                ]
            });
        } else if ($(window).outerWidth(true) >= 768) {
            $('.ratings_slick').not('.slick-initialized').slick({
                infinite: true,
                speed: 500,
                fade: false,
                rtl: true,
                pauseOnHover: false,
                cssEase: 'linear',
                autoplay: false,
                autoplaySpeed: 2000,
                prevArrow: false,
                nextArrow: false,
                slidesToShow: 2,
                slidesToScroll: 2,
                centerMode: false,
                responsive: [{
                    breakpoint: 400,
                    settings: {
                        fade: false,
                        autoplay: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: false,
                        /* set centerMode to false to show complete slide instead of 3 */
                        pauseOnHover: true,
                    }
                },
                    {
                        breakpoint: 768,
                        settings: {
                            fade: false,
                            autoplay: true,
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            centerMode: false,
                            /* set centerMode to false to show complete slide instead of 3 */
                            pauseOnHover: true,
                        }
                    }
                ]
            });
        } else {
            $('.ratings_slick').not('.slick-initialized').slick({
                infinite: true,
                speed: 500,
                fade: false,
                rtl: true,
                pauseOnHover: false,
                cssEase: 'linear',
                autoplay: false,
                autoplaySpeed: 2000,
                prevArrow: false,
                nextArrow: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false,
                responsive: [{
                    breakpoint: 400,
                    settings: {
                        fade: false,
                        autoplay: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        centerMode: false,
                        /* set centerMode to false to show complete slide instead of 3 */
                        pauseOnHover: true,
                    }
                }]
            });
        }
    }

    ratingSkills()

    $(window).resize(function () {
        ratingSkills()
    })

    $('.timer').each(function () {
        var $this = $(this);
        jQuery({
            Counter: 0
        }).animate({
            Counter: $this.text()
        }, {
            duration: 5000,
            easing: 'swing',
            step: function () {
                $this.text(Math.ceil(this.Counter));
            }
        });

    });

    $('.new_teacher_slick').slick({
        infinite: true,
        speed: 500,
        fade: false,
        rtl: true,
        pauseOnHover: false,
        cssEase: 'linear',
        autoplay: false,
        autoplaySpeed: 2000,
        prevArrow: false,
        nextArrow: false,
        slidesToShow: 5,
        slidesToScroll: 5,
        centerMode: false,
        responsive: [{
            breakpoint: 400,
            settings: {
                fade: false,
                autoplay: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false,
                /* set centerMode to false to show complete slide instead of 3 */
                pauseOnHover: true,

            }
        },
            {
                breakpoint: 768,
                settings: {
                    fade: false,
                    autoplay: true,
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    centerMode: false,
                    /* set centerMode to false to show complete slide instead of 3 */
                    pauseOnHover: true,
                }
            },
            {
                breakpoint: 900,
                settings: {
                    fade: false,
                    autoplay: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    centerMode: false,
                    /* set centerMode to false to show complete slide instead of 3 */
                    pauseOnHover: true,
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    fade: false,
                    autoplay: true,
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    centerMode: false,
                    /* set centerMode to false to show complete slide instead of 3 */
                    pauseOnHover: true,
                }
            },
        ]

    });

    $('#Basic_topics_slider').slick({
        infinite: true,
        speed: 500,
        fade: false,
        rtl: true,
        pauseOnHover: false,
        cssEase: 'linear',
        autoplay: false,
        autoplaySpeed: 2000,
        prevArrow: false,
        nextArrow: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        centerMode: false,
        responsive: [{
            breakpoint: 400,
            settings: {
                fade: false,
                autoplay: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false,
                /* set centerMode to false to show complete slide instead of 3 */
                pauseOnHover: true,

            }
        },
            {
                breakpoint: 768,
                settings: {
                    fade: false,
                    autoplay: true,
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    centerMode: false,
                    /* set centerMode to false to show complete slide instead of 3 */
                    pauseOnHover: true,
                }
            },
            {
                breakpoint: 900,
                settings: {
                    fade: false,
                    autoplay: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    centerMode: false,
                    /* set centerMode to false to show complete slide instead of 3 */
                    pauseOnHover: true,
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    fade: false,
                    autoplay: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    centerMode: false,
                    /* set centerMode to false to show complete slide instead of 3 */
                    pauseOnHover: true,
                }
            },
        ]

    });

    $('#blog_slider').slick({
        infinite: true,
        speed: 500,
        fade: false,
        rtl: true,
        pauseOnHover: false,
        cssEase: 'linear',
        autoplay: false,
        autoplaySpeed: 2000,
        prevArrow: false,
        nextArrow: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        centerMode: false,
        responsive: [{
            breakpoint: 400,
            settings: {
                fade: false,
                autoplay: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false,
                /* set centerMode to false to show complete slide instead of 3 */
                pauseOnHover: true,

            }
        },
            {
                breakpoint: 768,
                settings: {
                    fade: false,
                    autoplay: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false,
                    /* set centerMode to false to show complete slide instead of 3 */
                    pauseOnHover: true,
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    fade: false,
                    autoplay: true,
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    centerMode: false,
                    /* set centerMode to false to show complete slide instead of 3 */
                    pauseOnHover: true,
                }
            },
        ]

    });
    $('#bild_teacher_profile').slick({
        infinite: false,
        speed: 500,
        fade: false,
        rtl: true,
        pauseOnHover: false,
        cssEase: 'linear',
        autoplay: false,
        autoplaySpeed: 2000,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: false,

    });

    var myVideo3 = document.getElementById("video3");

    function playPause() {
        if (myVideo3.paused) {
            myVideo3.play();
        } else {
            myVideo3.pause();
        }
    }

    $(document).on("click", ".about_as #video3", function () {
        playPause();
    })

    $(document).on("click", ".about_as .iconPlay", function () {
        $(this).hide();
        $(".about_as .coverVideo").hide();
        playPause();
    })

    $(document).on("click", ".have_question .head", function () {
        $(".have_question").toggleClass("open")
    })

    $(document).on("click", ".linkLogin", function () {
        $(".loginHome").addClass("show");
    })

    $(document).on("click", ".loginHome .closeX", function () {
        $(".loginHome").removeClass("show");
    })

    $(document).mousedown(function (e) {
        var container = $(".loginHome>div");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".loginHome").removeClass("show");
        }
    });

});

/******************************************************************/
// rate star

$(document).on("mouseover", ".rateStar:not('.click') i", function () {
    if ($(this).attr("num") == 1) {
        $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
    } else if ($(this).attr("num") == 2) {
        $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
    } else if ($(this).attr("num") == 3) {
        $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[2]).addClass("active")
    } else if ($(this).attr("num") == 4) {
        $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[2]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[3]).addClass("active")
    } else if ($(this).attr("num") == 5) {
        $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[2]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[3]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[4]).addClass("active")
    }
})

// $(document).on("click", ".rateStar:not('.click') i", function () {
//     $($(this).parents(".rateStar")).addClass("click");
//     $($($(this).parents(".rateStar")).find("input")).val($(this).attr("num"))
// })
$(document).on("click", ".rateStar:not('.click') i", function () {
    $($(this).parents(".rateStar")).addClass("click");
    $($($(this).parents(".rateStar")).find("input")).val($(this).attr("num"))
    $($($(this).parents(".rateStar")).find("input")).trigger("keyup");
    if ($(this).attr("num") == 1) {
        $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
    } else if ($(this).attr("num") == 2) {
        $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
    } else if ($(this).attr("num") == 3) {
        $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[2]).addClass("active")
    } else if ($(this).attr("num") == 4) {
        $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[2]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[3]).addClass("active")
    } else if ($(this).attr("num") == 5) {
        $($($(this).parents(".rateStar")).find($("i"))[0]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[1]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[2]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[3]).addClass("active")
        $($($(this).parents(".rateStar")).find($("i"))[4]).addClass("active")
    }
})

$(document).on("click", ".rateStar.click i", function () {
    $(this).removeClass("active")
    $($(this).siblings("i")).removeClass("active")
    $($(this).parents(".rateStar")).removeClass("click")
})

$(document).on("mouseout", ".rateStar:not('.click') i", function () {
    $(this).removeClass("active")
    $($(this).siblings("i")).removeClass("active")
})

$(document).on("click", ".dropHour ul li", function () {
    $($($(this).parent("ul")).find("li")).removeClass("select")
    $(this).addClass("select");
    $($($(this).parents(".dropHour")).next("input")).val($($($(this).parents(".dropHour")).find(".ulHour li.select")).text().replace(/ /g, '') + ":" + $($($(this).parents(".dropHour")).find(".ulMin li.select")).text().replace(/ /g, ''))
})

$(document).on("focus", "td input", function () {
    $($($(this).parents("td")).find(".dropHour")).addClass("show")
})

$(document).mousedown(function (e) {
    var container = $(".dropHour");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(".dropHour").removeClass("show");
    }
});

$(document).on("click", ".forgetPass", function () {
    $(".loginHome").removeClass("login").addClass("remmber")
})

$(document).on("click", ".btnLog", function () {
    $(".loginHome").removeClass("remmber").addClass("login")
})

$(document).on("click", ".btnJoin", function () {
    $(".popJoinX").addClass("show")
})

$(document).on("click", ".popJoinX .closeX", function () {
    $(".popJoinX").removeClass("show")
})

$(document).mousedown(function (e) {
    var container = $(".popJoinX>div");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(".popJoinX").removeClass("show");
    }
});

$(document).on("click", ".addRateTeacher a", function () {
    $(".popUpRateTeacher").addClass("show")
})

$(document).on("click", ".popUpRateTeacher .closeX", function () {
    $(".popUpRateTeacher").removeClass("show")
})

$(document).mousedown(function (e) {
    var container = $(".popUpRateTeacher>div");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(".popUpRateTeacher").removeClass("show");
    }
});

$(document).on("click", ".deleteAccount", function () {
    $(".popUpDelete").addClass("show")
})

$(document).on("click", ".popUpDelete .closeX", function () {
    $(".popUpDelete").removeClass("show")
})

$(document).mousedown(function (e) {
    var container = $(".popUpDelete>div");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(".popUpDelete").removeClass("show");
    }
});

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
        $(".fixedInfoTeacher").addClass("down");
    } else {
        $(".fixedInfoTeacher").removeClass("down");
    }
});

$(document).on("click", ".fixedInfoTeacher .rate", function () {
    $(".popUpAddRate").addClass("show")
})


$(document).on("click", ".addRate", function () {
    $(".popUpAddRate").addClass("show")
})

$(document).on("click", ".popUpAddRate .closeX", function () {
    $(".popUpAddRate").removeClass("show")
})

$(document).on("click", ".popUp .closeX", function () {
    $($(this).parents(".popUp")).removeClass("show")
})

$(document).mousedown(function (e) {
    var container = $(".popUpAddRate>div");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(".popUpAddRate").removeClass("show");
    }
});


/*************************/
var phone_span_contantX;
var phone_Number;
$('.show_phone_Number').click(function () {
    if ($(this).hasClass("numberShowing")) {
        phone_Number = $($(this).find(".phone_Number"));
        $($($(this).find(".phone_Number")).siblings(".phone_span_contant")).fadeToggle(10)
        $($(this).find(".phone_Number")).fadeToggle(10)
        $(this).removeClass("numberShowing")
    } else {
        phone_span_contantX = $($(this).find(".phone_span_contant"));
        $($(this).find(".phone_span_contant")).fadeToggle(10)
        $($(this).find(".phone_span_contant")).parents(".show_phone_Number").addClass("noVisible")
        setTimeout(function () {
            phone_span_contantX.parents(".show_phone_Number").removeClass("noVisible")


            phone_span_contantX.parents(".show_phone_Number").addClass("numberShowing")
            $(phone_span_contantX.siblings(".phone_Number")).fadeToggle(10)
        }, 100)
    }
});



// 2-1-2020

$(document).on("keyup", "input", function () {
    if ($(this).siblings(".listAutoSearch").length == 1) {
        if ($(this).val().length > 0) {
            $($(this).siblings(".listAutoSearch")).removeClass("none")
        } else {
            $($(this).siblings(".listAutoSearch")).addClass("none")
        }
    }
})

$(document).on("click", ".popUpsd .addHours td span", function () {
    $('<tr class="rowFrom">' + $(".rowFrom").html() + '</tr>').insertBefore(".popUpsd tr.addHours");
    $('<tr class="rowTo">' + $(".rowTo").html() + '</tr>').insertBefore(".popUpsd tr.addHours");
})

$(document).on("click", ".ulHour", function () {
    $(".dropHour").removeClass("show");
})


// menu bar

$(document).on("click", ".menuMobile .icon", function () {
    $(".menuBarsMobile").toggleClass("active");
})

$(document).mousedown(function (e) {
    var container = $(".menuBarsMobile>div:nth-of-type(1)");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(".menuBarsMobile").removeClass("active");
    }
});


$(document).on("click", ".popUpsd  .noActive input", function () {
    if ($(this).is(':checked')) {
        $(".popUpsd  td." + $(this).attr("id") + " input").attr("disabled", "disabled");
    } else {
        $(".popUpsd  td." + $(this).attr("id") + " input").removeAttr("disabled");
    }
})


$(document).on("click", ".cancelTop", function () {
    $($(this).parents(".btnsForm")).addClass("d-none");
    $($($($(this).parents("form"))).find("input")).attr("disabled", "disabled")
    $($($($(this).parents("form"))).find("select")).attr("disabled", "disabled")
    $($($($(this).parents("form"))).find("textarea")).attr("disabled", "disabled")
});


$(document).on("click", "input[type='password']", function () {
    $(".confirmPassword").show();
    $(".confirmPassword input").removeAttr("disabled")
})

$(document).on("click", ".cancelTop", function() {
    $(".confirmPassword").hide();

})




$(document).on("click", ".headerTeacher .user", function () {
    $(".headerTeacher .menuNotification").removeClass("active");
    $(".headerTeacher .menuUser").toggleClass("active");
})

$(document).on("click", ".headerTeacher .notification", function () {
    $(".headerTeacher .menuUser").removeClass("active");
    $(".headerTeacher .menuNotification").toggleClass("active");
})

$(document).mousedown(function (e) {
    var container = $(".headerTeacher .menuUser");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(".headerTeacher .menuUser").removeClass("none");
    }
});

$(document).mousedown(function (e) {
    var container = $(".headerTeacher .menuNotification");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(".headerTeacher .menuNotification").removeClass("none");
    }
});

// script for teacher => alert

$(document).on("click", ".textAlertData", function () {
    if ($(window).outerWidth(true) <= 991.9) {
        $(".over").addClass("active");
    }
})

$(document).mousedown(function (e) {
    var container = $(".alertUser");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($(window).outerWidth(true) <= 991.9) {
            setTimeout(function () {
                $(".alertUser").removeClass("active");
            }, 200)
        } else {
            $(".alertUser").removeClass("active");
        }
    }
});

$(document).on("click", ".over.active,.over.show", function () {
    setTimeout(function () {
        $(".alertUser").removeClass("active");
        $(".over").removeClass("active show");
    }, 200)
})

$(document).mousedown(function (e) {
    var container = $(".overUserChat");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $(".listUsers li .icon").removeClass("active");
        $(".overUserChat").removeClass("active");
    }
});

$(document).on("click", ".listUsers li .icon", function () {
    $(this).toggleClass("active");
    $($(this).find(".overUserChat")).toggleClass("active");
})

// $(document).on("click", ".listUsers li", function (e) {
//     $(".listUsers li").removeClass("active");
//     $(this).addClass("active");
//     showChat();
//     if ($(window).outerWidth(true) <= 800) {
//         if ($($(e.target).parents(".icon")).length !== 1) {
//             $(".colList").removeClass("activeMob");
//             $(".btnBack").css("display", "inline-flex");
//         } else {
//             $($(this).find(".icon")).toggleClass("active");
//             $($($(this).parents(".icon")).find(".overUserChat")).toggleClass("active");
//         }
//     }
// })

$(document).on("click", ".boxChat .btnBack", function () {
    if ($(window).outerWidth(true) <= 800) {
        $(".colList").addClass("activeMob")
        $(".btnBack").css("display", "none")
    }
})

// function showChat() {
//     for (var i = 0; i < $(".listUsers li").length; i++) {
//         if ($($(".listUsers li")[i]).hasClass("active")) {
//             $(".chatPerson").removeClass("active");
//             $(".chatPerson." + $($(".listUsers li")[i]).attr("data-chat")).addClass("active")
//         }
//     }
// }

// showChat()

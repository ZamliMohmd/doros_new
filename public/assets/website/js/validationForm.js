$( document ).ready( function () {

    $( "#teacher_join " ).validate( {
        rules: {
            name: {
                required: true,
                minlength: 4
            },
            mobile: "required",
         //   email: "required",
           // password: "required",
        //    country:"required",
       //     country_code:"required",
        //    title:"required",
         //   start_date:"required",
         //   start_time:"required",
          //  desc:"required",
          //  duration:"required",
            body:{
             required: true,
             minlength: 20
            },
            password: {
                required: true,
                minlength: 6
            },
            password_confirmation: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            agree1: "required"
        },
        messages: {
            name: "رجاء ادخل اسمك كاملا",
            mobile: "رجاء ادخل رقم جوالك ",
            educat_level: "رجاء اختر التخصص",
            specialized:"هذا الحقل مطلوب",
            country:"هذا الحقل مطلوب",
            country_code:"هذا الحقل مطلوب",
            title:"هذا الحقل مطلوب",
            start_date:"هذا الحقل مطلوب",
            start_time:"هذا الحقل مطلوب",
            desc:"هذا الحقل مطلوب",
            duration:"هذا الحقل مطلوب",
            body:{
             required:"رجاء اكتب الرساله",
             minlength: "عدد الحروف غير كافيه",
            },
            password: {
                required: "رجاء ادخل كلمه السر",
                minlength: "يجب ان تكون كلمه السر اكثر من 5 حروف"
            },
            // password_confirmation: {
            //     required: "رجاء ادخل تاكيد كلمه السر",
            //     equalTo: "يجب ان تكون نفس كلمه السر السابقه",
            //     minlength: "يجب ان تكون كلمه السر اكثر من 5 حروف",
            //
            // },
            email: "رجاء ادخل عنوان البريد الالكتروني",
            accept_use_terms: "رجاء اقبل شروط وسياسات الموقع "
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "help-block" );
            // Add `has-feedback` class to the parent div.form-group
            // in order to add icons to inputs
            element.parents( ".col-sm-6,.col-sm-4,.col-sm-2" ).addClass( "has-feedback" );
            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if ( !element.next( "span" )[ 0 ] ) {
                $( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
            }
        },
        success: function ( label, element ) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if ( !$( element ).next( "span" )[ 0 ] ) {
                $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parents( ".col-sm-6,.col-sm-4,.col-sm-2" ).addClass( "has-error" ).removeClass( "has-success" );
            $( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
        },
        unhighlight: function ( element, errorClass, validClass ) {
            $( element ).parents( ".col-sm-6,.col-sm-4,.col-sm-2" ).addClass( "has-success" ).removeClass( "has-error" );
            $( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
        }
    } );
} );

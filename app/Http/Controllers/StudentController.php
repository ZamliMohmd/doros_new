<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Lesson;
use App\Models\Maintopic;
use App\Models\Rate;
use App\Models\Student;
use App\Models\Studyinglevel;
use App\Models\Subtopic;
use App\Models\TeacherStudent;
use App\Models\User;
use App\Notifications\ActivateNewUserNotification;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response

    public function main(){
        dd(Auth::user());
        $user = Auth::user();
        $student = Student::where('user_id' , $user->id)->first();
        return view('website.student.index' , compact('user' , 'student'));

    }
     *  */



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function register(Request $request){
        //  dd($request->all());

        $validator =    $this->validate($request, [
            'name' => 'required|min:2|max:25',
            'email' => 'required|email|unique:users',
            'mobile' => 'required|unique:users',
            'password' => 'required|min:6|max:15',
            //  'accept_use_terms' => 'required'

        ]);
//dd($request->_token);
        $fullname = $request->name;
        if (str_contains($fullname , " ")  == true){
            $first_name = strtok( $fullname, ' ' );
            $last_name = substr($fullname, strpos($fullname, " ") + 1);
            $firstname= $first_name;
            $lastname = $last_name;
        }else{
            $firstname = $request->name;
            $lastname = null;
        }

        $user =  User::create([
            'name' => $firstname,
            'last_name' => $lastname,
            'email' => strtolower($request->input('email')),
            'mobile' => $request->input('mobile'),
            'password' => bcrypt($request->input('password')),
            'token' => time().$request->_token,
            'is_verified' => 0,
            'usertype_id' => '4',
            //  'created_at' => date_timestamp_set(),
        ]);

        $student = new Student();
        $student->user_id = $user->id;
        $student->save();

        // \Illuminate\Support\Facades\Notification::send($user, new ActivateNewUserNotification($user));
        session()->flash('message', 'Your account is created');
        Auth::login($user);
        return redirect('/')->with('success', 'تم تسجيلك بنجاح');


    }


    public function messages(){
        $messages = \App\Models\Message::where('user_id' ,Auth::id())
            ->orwhere('receiver_id' , Auth::id())
            ->with(['senderUser'=> function($query) {
                $query->select('id', 'name' , 'last_name' , 'email' , 'mobile');
            }]) ->with(['receiverUser' => function($query) {
                $query->select('id', 'name' , 'last_name'  , 'email' , 'mobile');
            }])
            //  ->with('replies')
            ->orderBy('created_at' , 'desc')
            ->get();
        // dd($messages);
        return view('website.student.messages_old' , compact('messages'));
    }







}

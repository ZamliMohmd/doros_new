<?php

namespace App\Http\Controllers;

use App\Events\MessageEvents;
use App\Helper\Zoom;
use App\Models\Blog;
use App\Models\City;
use App\Models\Degree;
use App\Models\Experience;
use App\Models\Mainspecialtie;
use App\Models\Maintopic;
use App\Models\Message;
use App\Models\Rate;
use App\Models\Subtopic;
use App\Models\Teacher;
use App\Models\Teacherexperience;
use App\Models\TeacherHourswork;
use App\Models\TeacherPrices;
use App\Models\Terms;
use App\Models\University;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomesController extends Controller
{
    public function index(){
       // dd('111');
        $teachers = \App\Models\Teacher::count();
        $subtopics = \App\Models\Subtopic::count();
        $universities = \App\Models\University::count();
        $cities = \App\Models\City::count();

        $lastTeachers = Teacher::orderBy('id', 'desc')->where('is_approved' , 1)->take(5)->get();
      //  dd($lastTeachers);

        return view('welcome' , compact('teachers' , 'subtopics' , 'universities' , 'cities',
        'lastTeachers'));
    }

    public function search(Request $request){

       $data = $request->all();
       // dd($data);

        $builder = Teacher::leftJoin('users', 'teachers.user_id', '=', 'users.id')
                ->leftJoin('teacher_prices', 'teachers.id', '=', 'teacher_prices.teacher_id')
         //   ->leftJoin('specializations', 'teachers.specialization_id', 'specializations.id')
         //   ->leftJoin('educationlevels', 'teachers.educationlevel_id', 'educationlevels.id')
            ->where('teachers.is_approved' , 1)
            ->select('users.id as user_id','users.name as user_name', 'users.mobile as user_mobile',
                'teachers.*');
        //, 'countries.name as country' , 'specializations.name as spec_name')
        //;
//->get();
//dd($data);

     //   dd( $builder->with('price'));
        if (array_key_exists('minimum_price', $data) && array_key_exists('maximum_price', $data)) {
            $builder
                ->whereBetween('teacher_prices.price_online',[ $data['minimum_price'] , $data['maximum_price']]);
        }

        if (array_key_exists('gender', $data) && $data['gender'] != "") {
            $builder->where('teachers.gender_id', '=', $data['gender']);
        }

        if (array_key_exists('experience', $data) && $data['experience'] != "") {

           $experience = $data['experience'];
            $builder->whereHas('experience', function($q) use ($experience) {
                $q->where('id', $experience);
            });

        }

        if (array_key_exists('subtopic', $data) && $data['subtopic'] != "") {

            $subtopic = $data['subtopic'];
            $builder->whereHas('cities', function($q) use ($subtopic) {
                $q->where('id', $subtopic);
            });

        }

        if (array_key_exists('cities', $data) && $data['cities'] != "") {

            $cities = $data['cities'];
            $builder->whereHas('subtopic', function($q) use ($cities) {
                $q->where('id', $cities);
            });

        }

            if (array_key_exists('place', $data) && $data['place'] != "") {

                $place = $data['place'];
                $builder->whereHas('place', function($q) use ($place) {
                    $q->where('id', $place);
                });

            }

        if (array_key_exists('universities', $data) && $data['universities'] != "") {
           // $builder->leftJoin('teacher_degrees', 'teachers.id', '=', 'teacher_degrees.teacher_id')
            $university = $data['universities'];
            $builder->whereHas('degree', function($q) use ($university) {
                $q->where('university_id', $university);
            });
        }

        if (array_key_exists('specialtion', $data) && $data['specialtion'] != "") {
            // $builder->leftJoin('teacher_degrees', 'teachers.id', '=', 'teacher_degrees.teacher_id')
            $specialtion = $data['specialtion'];
            $builder->whereHas('degree', function($q) use ($specialtion) {
                $q->where('specialty_id', $specialtion);
            });
        }
         //   ->whereHas('tags', function($query) use ($keyword) {
            //    $query->where('name', 'LIKE', "%$keyword%");
           // })

//
//        if (array_key_exists('educationLevel', $data) && $data['educationLevel'] != "") {
//            $builder->where('teachers.educationlevel_id',  '=', $data['educationLevel']);
//        }
//
//        if (array_key_exists('specialization', $data) && $data['specialization'] != "") {
//            $builder->where('teachers.specialization_id', '=', $data['specialization']);
//        }



//dd($builder);
        // $teacherAll = $builder->orderBy('teachers.id', 'desc')->paginate(40);

        // dd($teacherAll);

        $teachers = $builder->orderBy('id','desc')->paginate(24);


      // dd($teachers);
        $universities = University::all();
        $mainspecialties = Mainspecialtie::all();
        $degrees = Degree::all();
        $cities = City::all();
        $maintopics = Maintopic::all();
        $subtopics = Subtopic::all();
        $mainspecialties = Mainspecialtie::all();
        $experiences = Experience::take(6)->get();


        return view('website.search' , compact('universities' , 'mainspecialties' , 'degrees' , 'cities',
        'teachers' , 'maintopics' , 'subtopics' , 'mainspecialties' , 'experiences'));
    }

    public function sendMessage(Request $request)
    {
       // dd($request->all());
        $user = Auth::user();

        $message = Message::create([
            'message' => $request->input('message'),
            'user_id' => $user->id,
            'receiver_id' => $request->receiver_id,
            'is_seen' => 0
        ]);

        broadcast(new MessageEvents($user, $message))->toOthers();

        return redirect()->back();
    }



}

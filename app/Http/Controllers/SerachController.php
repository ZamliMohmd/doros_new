<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Subtopic;
use App\Models\User;
use Illuminate\Http\Request;

class SerachController extends Controller
{
    public function index(){
        return view('auto');
    }

    public function autocompleteSubtopic(Request $request)
    {
      //  dd($request->all());
         $data = Subtopic::where( "name",'like', "%{$request->get('query')}%" )->get();

        return response()->json($data);



    }

    function fetchSubtopic(Request $request)
    {
        if($request->get('query'))
        {
            $query = $request->get('query');
            $data =
                Subtopic::leftJoin('maintopics', 'subtopics.maintopic_id', '=', 'maintopics.id')
                ->where('subtopics.name_ar', 'LIKE', "%{$query}%")
                    ->orWhere('subtopics.name_he', 'LIKE', "%{$query}%")
                    ->orWhere('maintopics.name_ar', 'LIKE', "%{$query}%")
                    ->orWhere('maintopics.name_he', 'LIKE', "%{$query}%")
                    ->select('maintopics.name_ar as maintopic' , 'subtopics.name_ar as subtopic')
                    ->take(9)->get();
            $output = '<ul class="dropdown-menu" id="subtopics-ul" style="display:block; position:relative;min-width: 17.8rem;color: black">';
            foreach($data as $row)
            {
                $output .= '
         <li>'.$row->maintopic." - ".$row->subtopic .'</li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }

    function fetchCities(Request $request)
    {
        if($request->get('query')) {
            $query = $request->get('query');
            $data =
                City::where('name_ar', 'LIKE', "%{$query}%")
                    ->orWhere('name_he', 'LIKE', "%{$query}%")
                    ->take(9)->get();
            $output = '<ul class="dropdown-menu" id="cities-ul" style="display:block; position:relative;min-width: 17.8rem;color: black">';

            foreach($data as $row)
            {
                $output .= '
       <li style="">'.$row->name_ar.'</li>
       ';
            }
            if ($data == null){
                $output .= '<li><a href="#">لا يوجد نتائج</a></li>';
            }
            $output .= '</ul>';

            echo $output;
        }
    }

}




<?php

namespace App\Http\Controllers;

//use App\Events\MessageEvent;
use App\Events\MessageEvents;
use App\Models\Blog;
use App\Models\City;
use App\Models\Degree;
use App\Models\Experience;
use App\Models\Lesson;
use App\Models\Mainspecialtie;
use App\Models\Maintopic;
use App\Models\Message;
use App\Models\Rate;
use App\Models\Subscribeplan;
use App\Models\Studyinglevel;
use App\Models\Subtopic;
use App\Models\Teacher;
use App\Models\TeacherDegree;
use App\Models\Teacherexperience;
use App\Models\TeacherHourswork;
use App\Models\TeacherPrices;
use App\Models\TeacherStudent;
use App\Models\TeacherWorkhours;
use App\Models\TopicsByTeacher;
use App\Models\TranzillaPayment;
use App\Models\University;
use App\Models\User;
//use App\Notifications\ActivateNewUserNotification;
use App\Notifications\ActivateNewUserNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\File;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;
use function GuzzleHttp\Promise\all;
use function MongoDB\BSON\fromJSON;


class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function registerPage(){

        return view('website.teacher.register');
    }

    public function register(Request $request){
       // dd($request->all());

            $request->validate([
                'name' => 'required|min:2|max:25',
                'email' => 'required|email|unique:users',
                'mobile' => 'required|unique:users',
                'password' => 'required|min:6|max:25',
               // 'password' => 'required|min:6|max:25|confirmed|regex:/^(?=.*[a-z])(?=.*\d).+$/',

                 'accept_use_terms' => 'required'
            ]);
//        dd($this->validate($request, [
//            'name' => 'required|min:2|max:25',
//            'email' => 'required|email|unique:users',
//            'mobile' => 'required|unique:users',
//            'password' => 'required|min:6|max:15',
//             'accept_use_terms' => 'required'
//        ]));
            $fullname = $request->name;
            if (str_contains($fullname , " ")  == true){
                $first_name = strtok( $fullname, ' ' );
                $last_name = substr($fullname, strpos($fullname, " ") + 1);
                $firstname= $first_name;
                $lastname = $last_name;
            }else{
                $firstname = $request->name;
                $lastname = null;
            }

                    $user = new User();
                  $user->name = $firstname;
                 $user->last_name = $lastname;
                 $user->email = strtolower($request->input('email'));
                 $user->mobile = $request->input('mobile');
                 $user->password = bcrypt($request->input('password'));
                 $user->token = time().$request->_token;
                 $user->is_verified = 1;
                 $user->usertype_id = '3';
                 $user->save();

                 $teacher = new Teacher();
                 $teacher->user_id = $user->id;
                 $teacher->complete_profile_percentage = 0;
                 $teacher->save();
//dd($user->id);
          //  \Illuminate\Support\Facades\Notification::send($user, new ActivateNewUserNotification($user));
            session()->flash('message', 'Your account is created');
            Auth::login($user);

//            if ($user)
//                return response()->json([
//                    'status' => true,
//                    'msg' => 'تم تسجيلك بنجاح, تم ارسال رابط التفعيل على بريدك الالكتروني',
//                    'redirect' => route('teacher_completeprofile')
//
//                ]);
//
//            else
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'فشل الحفظ برجاء المحاوله مجددا',
//                ]);

           // return redirect('/login')->with('status', 'تم ارسال رابط التفعيل الى بريدك الالكتروني.قم بتفعيل حسابك لتتمكن من استخدام الموقع');
          return redirect()->route('teacher_completeprofile')->with('success', 'تم تسجيلك بنجاح');
    }


    public function completeProfile()
    {
        try {
            if(Auth::user()){
            //  if (Auth::user()->isVerified() == 1) {
                  if ( Auth::user()->usertypeID() == 3) {
                 $user = Auth::user();
                  //    $userID = Auth::user()->id;
                // dd(Auth::user()->teacherId());
                 if (Auth::user()->teacherId() != null) {
                    $teacher = Teacher::where('user_id' , $user->id)->with('user')->first();
                   // dd($teacher);
                 }
                $teacher = Teacher::where('user_id' , $user->id)->first();
                $studinglevels = Studyinglevel::all();
                $maintopics = Maintopic::all();
                $subtopics = Subtopic::all();
                $cities = City::all();
                $universities = University::all();
                $Mainspecialties = Mainspecialtie::all();
                $experiences = Experience::all();
                $degrees = Degree::all();
                $degreeTeacher = $teacher->degree()->first();
                $topics = $teacher->subtopic()->get();
                $teacherCities = $teacher->cities()->get();
                 $prices = TeacherPrices::where('teacher_id' , $teacher->id)->first();
                 $experience = $teacher->experience()->get();

                      // dd($degreeTeacher);
               // dd($degreeTeacher);
                if ($user->isCompleteProfile() == 0) {
                    return view('website.teacher.complete_profile', compact('user' , 'studinglevels' ,
                        'maintopics' , 'subtopics' , 'cities' , 'universities' , 'Mainspecialties' , 'degrees' , 'experiences' , 'teacher' ,
                    'degreeTeacher' , 'topics' , 'teacherCities' , 'prices' , 'experience'));
                } else {
                    return redirect('/teacher/index');
                }
            } else {
                return redirect('/');
            }
//             } else{
//                  return redirect('/login');
//               }
            }
              else{
                return redirect('/login');
            }


        } catch (\Exception $exception){

dd($exception);
        }

    }



    public function chat(){
        return view('chat');
    }

//    public function completeProfileStep2(){
//        return view('website.teacher.step2');
//    }

    public function messages(){
        $messages = \App\Models\Message::where('receiver_id' , Auth::id())
            ->orwhere('user_id' , Auth::id())
            ->with(['senderUser'=> function($query) {
                $query->select('id', 'name' , 'last_name' , 'email' , 'mobile');
            }]) ->with(['receiverUser' => function($query) {
                $query->select('id', 'name' , 'last_name'  , 'email' , 'mobile');
            }])
           // ->with('replies')
            ->orderBy('created_at' , 'desc')
            ->get();
        return view('website.teacher.messages_old' , compact('messages'));
    }

    public function getAllMessages($id1 , $id){
        $messages = Message::where('receiver_id' , $id)
            ->orWhere('user_id' ,  $id)
            ->orderBy('created_at' , 'desc')
            ->get();

        return response()->json($messages , 200);
    }

    public function fetchMessages()
    {
        return Message::with('user')->get();
    }

    public function storeCompleteProfiles(Request $request){
        // dd($request->all());

        $teacher = Teacher::findOrFail(Auth::user()->teacherId());
        $teacher->user_id = Auth::id();
        $teacher->gender_id = $request->gender_id;
        // $teacher->is_accept_appear_mobile_no = $request->is_accept_appear_mobile_no;
        isset($request->is_accept_appear_mobile_no)? $teacher->is_accept_appear_mobile_no = $request->is_accept_appear_mobile_no : $teacher->is_accept_appear_mobile_no = 0;
        isset($request->is_accept_whatsapp_msg)? $teacher->is_accept_whatsapp_msg = $request->is_accept_whatsapp_msg : $teacher->is_accept_whatsapp_msg = 0;

        $teacher->city_id  = $request->city_id ;
        $teacher->birth_year = $request->birth_year;
        $teacher->complete_profile_percentage = 30;

        if ($file = $request->file('image')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name = $request->user_id . '_' . time() . '_.' . $ext;
            $file->move('website/teachers/images/', $name);
            File::delete($teacher->image);
            $teacher->image = 'website/teachers/images/' . $name;
            $teacher->complete_profile_percentage = 80;
        }

        $teacher->save();

        $degree = new TeacherDegree();
        $degree->teacher_id  = $teacher->id;
        $degree->user_id = Auth::user()->id;
        if($request->degree_id != 6) {
            $degree->degree_id = $request->degree_id;
            $degree->university_id = $request->university_id;
            $degree->specialty_id = $request->specialization_id;
        }elseif($request->specialization_id == 6){
            $degree->degree_id = $request->degree_id;
        }
        $degree->from_year = $request->from_year;
        $degree->to_year = $request->to_year;
        $degree->save();
        // $teacher->subtopic()->attach($request->topics);


//dd($student);
        $user = User::find(Auth::id());
        //dd($user);
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->mobile = $request->mobile;
        //  $user->email = $request->email;
        $user->is_complete_profile = 0;
        //   $user->password =  Hash::make($request->password);
        $user->save();
        /*
                if ($user)
                    return response()->json([
                        'status' => true,
                        'msg' => 'تم تسجيل البيانات الشخصية بنجاح, الرجاء الانتقال للخطوة التالية',
                        //'redirect' => route('teacher_step2')

                    ]);

                else
                    return response()->json([
                        'status' => false,
                        'msg' => 'فشل الحفظ برجاء المحاوله مجددا',
                    ]);
        */
        session()->flash('message', 'تم اكمال بياناتك بنجاح');
        return redirect()-> back();
    }

    public function addLearningSubjects(Request $request){
        // dd($request->all());

        $teacher = Teacher::where('user_id' , Auth::id())->first();

        if (isset($request->topics)){
            $teacher->subtopic()->detach();
            $teacher->subtopic()->attach($request->topics);
            $teacher->save();
        }


        //dd($teacher);

        //  dd($teacher);
        /*
        if ($teacher)
            return response()->json([
                'status' => true,
                'msg' => 'تم تسجيل مواد التعليم بنجاح, الرجاء الانتقال للخطوة التالية',
                //'redirect' => route('teacher_step2')

            ]);

        else
            return response()->json([
                'status' => false,
                'msg' => 'فشل الحفظ برجاء المحاوله مجددا',
            ]);
        */
        session()->flash('message', 'تم اكمال بياناتك بنجاح');
        return redirect()-> back();

    }


    public function storePriceAndCities(Request $request){
        // dd( $request->all());
        $teacher = Teacher::where('user_id' , Auth::id())->first();
        $teacherPrice = TeacherPrices::where('teacher_id' , $teacher->id)->first();
        if (isset($teacherPrice) && $teacherPrice != null){
            $teacherPrice->price_online = $request->price_online;
            $teacherPrice->price_at_teacher = $request->price_at_teacher;
            $teacherPrice->price_at_student = $request->price_at_student;
            $teacherPrice->price_at_others = $request->price_at_others;
            $teacherPrice->user_id = $request->user_id;
            $teacherPrice->is_discount = $request->is_discount;
            $teacherPrice->discount_percentage = $request->discount_percentage;
            $teacherPrice->teacher_id = $teacher->id;
            $teacherPrice->save();

            $teacher->cities()->detach();
            $teacher->cities()->attach($request->teacher_city);
            $teacher->save();

        }else{
            $teacherPrices = new TeacherPrices();
            $teacherPrices->price_online = $request->price_online;
            $teacherPrices->price_at_teacher = $request->price_at_teacher;
            $teacherPrices->price_at_student = $request->price_at_student;
            $teacherPrices->price_at_others = $request->price_at_others;
            $teacherPrices->user_id = $request->user_id;
            $teacherPrices->is_discount = $request->is_discount;
            $teacherPrices->discount_percentage = $request->discount_percentage;
            $teacherPrices->teacher_id = $teacher->id;
            $teacherPrices->save();


            $teacher->cities()->attach($request->teacher_city);
            $teacher->save();
        }


        // dd($teacher);

        /*
                if ($teacher)
                    return response()->json([
                        'status' => true,
                        'msg' => 'تم تسجيل البلدات والاسعار بنجاح, الرجاء الانتقال للخطوة التالية',
                        //'redirect' => route('teacher_step2')

                    ]);

                else
                    return response()->json([
                        'status' => false,
                        'msg' => 'فشل الحفظ برجاء المحاوله مجددا',
                    ]);
        */

        session()->flash('message', 'تم اكمال بياناتك بنجاح');
        return redirect()-> back();

    }

    public function storeExperience(Request $request){

        //dd($request->all());
        $this->validate($request, [
            'video' => 'max:102400 | mimes:flv,mp4,wmv,avi,3gp,mkv',
        ]);
        $user = Auth::user();
        $userT = User::find(Auth::id());
        $teacher_id =$userT->teacher->id;

//        $teacherExperiences = new Teacherexperience();
//        $teacherExperiences->school_student_learning = $request->school_student_learning;
//        $teacherExperiences->lebsekhiometry = $request->lebsekhiometry;
//        $teacherExperiences->lebjrot_preparation = $request->lebjrot_preparation;
//        $teacherExperiences->exam_preparation = $request->exam_preparation;
//        $teacherExperiences->university_lecturer = $request->university_lecturer;
//        $teacherExperiences->teaching_assistant = $request->teaching_assistant;
//        $teacherExperiences->no_exprence = $request->no_exprence;
//        $teacherExperiences->user_id = $user->id;
//        $teacherExperiences->teacher_id = $teacher_id;
//        $teacherExperiences->save();

        $teacher = Teacher::find($teacher_id);

        if ( $teacher->experience()->get() != null){
            $teacher->experience()->detach();
        }
        $teacher->experience()->attach($request->experiences);
        $teacher->marketing_text = strip_tags($request->marketing_text);
        $teacher->cv_text = strip_tags($request->cv_text);

        if ($file = $request->file('video')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name = $userT->name.$userT->last_name . '_' . time() . '_' . $ext;
            // dd($ext);
            $file->move('website/teachers/video/', $name);
            $teacher->video = 'website/teachers/video/' . $name;
            $teacher->complete_profile_percentage = 90;
        }
        $teacher->save();

        return redirect()-> back();

    }

    public function storeWorkHours(Request $request){
        // dd($request->all());
        // dd(Auth::user()->name);
        //dd($request->inputFrom-1-1);
        $userId = Auth::id();
        $teacher = Teacher::where('user_id' , $userId)->first();
        //  dd($request->inputFrom-1-1);
        // dd(array_merge($request->sun_from,$request->sun_to));
        // dd([$request->sun_from,$request->sun_to]);
        $data=$request->all();
        // dd($data);
        if ($request->sun_from != null && $request->sun_to != null){
            foreach (array_combine($request->sun_from , $request->sun_to) as $from => $to ){
                if ($from != null && $to != null) {
//dd(array_combine($request->sun_from , $request->sun_to));
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_slug = 'sun';
                    $hourWork->day_id = 1;
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }

        if ($request->mon_from != null && $request->mon_to != null){
            foreach (array_combine($request->mon_from , $request->mon_to) as $from => $to ) {
                if ($from != null && $to != null) {
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_id = 2;
                    $hourWork->day_slug = 'mon';
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }

        if ($request->tues_from != null && $request->tues_to != null){
            foreach (array_combine($request->tues_from , $request->tues_to) as $from => $to ) {
                if ($from != null && $to != null) {
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_slug = 'tues';
                    $hourWork->day_id = 3;
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }

        if ($request->wend_from != null && $request->wend_to  != null){
            foreach (array_combine($request->wend_from , $request->wend_to) as $from => $to ) {
                if ($from != null && $to != null) {
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_slug = 'wed';
                    $hourWork->day_id = 4;
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }

        if ($request->thr_from != null && $request->thr_to  != null){
            foreach (array_combine($request->thr_from , $request->thr_to) as $from => $to ) {
                if ($from != null && $to != null) {
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_slug = 'thr';
                    $hourWork->day_id = 5;
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }


        if ($request->fri_from != null && $request->fri_to  != null){
            foreach (array_combine($request->fri_from , $request->fri_to) as $from => $to ) {
                if ($from != null && $to != null) {
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_slug = 'fri';
                    $hourWork->day_id = 6;
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }

        if ($request->sat_from != null && $request->sat_to  != null){
            foreach (array_combine($request->sat_from , $request->sat_to) as $from => $to ) {
                if ($from != null && $to != null){
                    $hourWork = new TeacherHourswork();
                    $hourWork->from = $from;
                    $hourWork->to = $to;
                    $hourWork->day_slug = 'sat';
                    $hourWork->day_id = 7;
                    $hourWork->teacher_id = $teacher->id;
                    $hourWork->user_id = $userId;
                    $hourWork->save();
                    $teacher->complete_profile_percentage = 100;
                    $teacher->save();
                }
            }
        }
//dd($hourWork);

        return redirect('/search');
        //   dd($request->all());
        // $data=$request->all();
        //  dd(  strtok($data, "-"));

        //  $whatIWant = substr($request->all(), strpos($data, "-") + 1);
//dd($whatIWant);



    }



}

<?php

namespace App\Http\Controllers;

use App\Events\MessageEvents;
use App\Models\Replymessage;
use App\Models\TeacherStudent;
use App\Models\User;
use App\Notifications\MessageNotification;
use App\Notifications\ReplyMessageNotification;
use http\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());
        $message = \App\Models\Message::where('user_id' , Auth::id())
            ->where('receiver_id' , $request->receiver_id)->first();

        if ($message == null){
        $message = new \App\Models\Message();
        $message->message = $request->message;
        $message->is_seen = 0;

        if ($file = $request->file('msg_file')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name = Auth::id() . '_' . time() . '_.' . $ext;

            $file->move('website/students/messages/files', $name);
            $message->image = 'website/students/messages/files' . $name;
        }

        $message->token = time() . csrf_token();
        $message->user_id = Auth::id();
        $message->receiver_id = $request->receiver_id;
        $message->save();
            $user = User::find($message->receiver_id);
            $student_teacher = TeacherStudent::where('student_id' , Auth::user()->studentId())->where('teacher_id' , $user->teacherId())->first();
          // dd($student_teacher);
            if (! isset($student_teacher)){
                $student_teacher = new TeacherStudent();
                $student_teacher->student_id =  Auth::user()->studentId();
                $student_teacher->teacher_id =  $user->teacherId();
                $student_teacher->user_id =  Auth::id();
                $student_teacher->save();
            }
        $user = User::find($message->receiver_id);
            $data = [
                'sender_id' => Auth::id(),
                'message_id' => $request->message,
                'receiver_id' => $request->receiver_id,
            ];
            Notification::send($user , new MessageNotification($message));
            event(new MessageEvents($data));
            return redirect()->back();
        }elseif ($message != null){
            $reply = new \App\Models\Replymessage();
            $reply->reply = $request->message;
            $reply->is_seen = 0;

            if ($file = $request->file('msg_file')) {
                $ext = strtolower($file->getClientOriginalExtension());
                $name = $request->sender_id . '_' . time() . '_' . $ext;

                $file->move('website/students/messages/files', $name);
                $reply->image = 'website/students/messages/files' . $name;
            }

            $reply->token = time().$request->_token;
            $reply->message_id = $message->id;
            $reply->sender_id = Auth::id();
            if($message->user_id == Auth::id()){
                $reply->receiver_id = $message->receiver_id;
            }elseif ($message->receiver_id == Auth::id()){
                $reply->receiver_id = $message->user_id;
            }
            $reply->save();
            //  dd($reply);
            $user = User::find($reply->receiver_id);

            $student_teacher = TeacherStudent::where('student_id' , Auth::user()->studentId())->where('teacher_id' , $user->teacherId())->first();
            if (! isset($student_teacher)){
                $student_teacher = new TeacherStudent();
                $student_teacher->student_id =  $user->studentId();
                $student_teacher->teacher_id =  Auth::user()->teacherId();
                $student_teacher->user_id =  Auth::id();
                $student_teacher->save();
            }
            $data = [
                'sender_id' => Auth::id(),
                'message_id' => $request->message,
                'receiver_id' => $request->receiver_id,
            ];
            Notification::send($user , new ReplyMessageNotification($reply));
            event(new MessageEvents($data));
            return redirect()->back();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function getAllMessages(){

        $messages = \App\Models\Message::where('user_id' ,Auth::id())
        ->orWhere('receiver_id' , Auth::id())
            ->with(['senderUser'=> function($query) {
            $query->select('id', 'name' , 'last_name' , 'email' , 'mobile');
        }]) ->with(['receiverUser' => function($query) {
            $query->select('id', 'name' , 'last_name'  , 'email' , 'mobile');
        }])
            ->with('replies')
            ->get();
      //  dd($messages);
        return response()->json($messages , 200);
    }


    public function storeMessage(Request $request)
    {
       // dd($request->all());
        $message = new \App\Models\Message();
        $message->message = $request->message;
        $message->is_seen = 0;

        if ($file = $request->file('msg_file')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name = Auth::id() . '_' . time() . '_' . $ext;

            $file->move('website/students/messages/files', $name);
            $message->image = 'website/students/messages/files' . $name;
        }

        $message->token = time().$request->_token;
        $message->user_id = Auth::id();
        $message->receiver_id = $request->user_id;
        $message->save();
        $user = User::find($message->receiver_id);
        Notification::send($user , new MessageNotification($message));
        // event(new MessageEvents($messageData));
        return response()->json($message , 200);

    }

    public function storeReply(Request $request)
    {
      //  dd( Auth::id());
      // dd($request->all());
        $message = \App\Models\Message::find($request->message_id);
       // dd($message);
        $reply = new \App\Models\Replymessage();
        $reply->reply = $request->reply;
        $reply->is_seen = 0;

        if ($file = $request->file('msg_file')) {
            $ext = strtolower($file->getClientOriginalExtension());
            $name = $request->sender_id . '_' . time() . '_' . $ext;

            $file->move('website/students/messages/files', $name);
            $reply->image = 'website/students/messages/files' . $name;
        }

        $reply->token = time().$request->_token;
        $reply->message_id = $request->message_id;
        $reply->sender_id = Auth::id();
         if($message->user_id == Auth::id()){
             $reply->receiver_id = $message->receiver_id;
    }elseif ($message->receiver_id == Auth::id()){
             $reply->receiver_id = $message->user_id;
         }
        $reply->save();
       //  dd($reply);
        $user = User::find($reply->receiver_id);
        Notification::send($user , new ReplyMessageNotification($reply));
        // event(new MessageEvents($messageData));
     //   return response()->json($reply , 200);
         return redirect()->back();

    }

    public function getAllReplies( $id , $userID){
        $messages = \App\Models\Replymessage::where('message_id' , $id)
            ->where('sender_id' ,  $userID)
            ->orWhere('receiver_id' ,  $userID)
            ->with(['senderUser'=> function($query) {
                $query->select('id', 'name' , 'last_name' , 'email' , 'mobile');
            }]) ->with(['receiverUser' => function($query) {
                $query->select('id', 'name' , 'last_name'  , 'email' , 'mobile');
            }])
            ->orderBy('created_at' , 'desc')
            ->get();

        return response()->json($messages , 200);
    }

    public function getNotification(){

      $notifications =   DB::table('notifications')
          ->join('users' , 'notifications.notifiable_id' , '=' , 'users.id')
          ->where('notifications.notifiable_id' , Auth::id())
          ->select('notifications.*' , 'users.id as user_id' , 'users.name as user_first_name'
              , 'users.last_name as user_last_name')
          ->orderBy('notifications.created_at' , 'desc')
          ->get();

      return response()->json($notifications , 200);
}
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAllMessageSentToUserByUserId($id){
       $messages =  \App\Models\Message::where('receiver_id' , $id)->get();
       return response()->json($messages);
    }

    public function getCountUnReadRepliesByUserId($id,$userID){
        $count =  Replymessage::where('message_id' , $id)
            ->where('sender_id' ,  $userID)
            ->orWhere('receiver_id' ,  $userID)->count();
        return response()->json($count);
    }
}

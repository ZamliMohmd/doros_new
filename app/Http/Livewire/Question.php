<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Question extends Component
{
    protected $listeners;
    public   $email, $question , $selected_id;
    public $updateMode = false;

    protected $rules = [
        'email' => 'email|required|max:50',
        'question' => 'required|max:1000'
    ];

    public function render()
    {
        return view('livewire.question');
    }

    public function updated($email , $question){
        $this->validateOnly($email);
        $this->validateOnly($question);
    }

    public function addQuestion(){

        $this->validate();
        \App\Models\Question::create([
            'question' => $this->question,
            'email' => $this->email,
        ]);

        session()->flash('success' , 'تم اضافة السؤال بنجاح');
        $this->question = "";
        $this->email = "";
    }
}

<?php

use App\Models\Teacherexperience;
use App\Models\TeacherHourswork;
use Illuminate\Support\Facades\Auth;

function customRequestCaptcha(){
    return new \ReCaptcha\RequestMethod\Post();
}

function testCap(){
    return  'light'
    ;
}

function getAllFinishLesson(){
    $lessons =  \App\Models\Lesson::where('status_id' , 2)
        ->where('user_id' , Auth::id())
        ->where('is_rated' , null)
        ->orderBy('created_at' , 'desc')->get();
    return $lessons;

}

function teacherCompleteProfilePercintage(){
    if ($image = null && $video = null && $workhours = null ){
        return '70';
    }
}
function getNotificationsByUserId(){
    return \App\Models\Notification::where('notifiable_id' , Auth::id())
        ->where('read_at' , null)
        ->orderBy('created_at' , 'desc')
        ->get();
}
function getTeacherIdByRateId($rateId){
    return \App\Models\Rate::where('teacher_id', $rateId)->first();
}
function getTeacherRateCount($teacher_id){
    return \App\Models\Rate::where('teacher_id' , $teacher_id)->count();
}

function getMainTopicIdBySubTopicId($id){
   return \App\Models\Subtopic::where('id' , $id)->first()->maintopic_id;
}

function getMainTopicNameBySubTopicId($id){
    return \App\Models\Maintopic::where('id' , $id)->first();
}
function getTeacherRate($teacher_id){
    $teacherRateCount =  \App\Models\Rate::where('teacher_id' , $teacher_id)->count();
    if ($teacherRateCount != 0){
    $teacherRates =  \App\Models\Rate::where('teacher_id' , $teacher_id)->select('rate')->get();
    $sum = 0;
    foreach ($teacherRates as $rate){
        $sum += $rate->rate;
    }
     $ratTeacher = round($sum / $teacherRateCount);
    return $ratTeacher;}
    else return 0;
}
function getDegreeByID($id){
    return \App\Models\Degree::where('id' , $id)->first();
}
function getStudentByID($id){
    return \App\Models\Degree::where('id' , $id)->first();
}
function getSubspecialtiesByID($id){
    return \App\Models\Subspecialties::where('id' , $id)->first();
}

function getUniversityByID($id){
    return \App\Models\University::where('id' , $id)->first();
}

function ttttt($id){
    return  Teacherexperience::where('teacher_id' , $id)->first();
}

function getMaintopicNameById($id){
    return \App\Models\Maintopic::where('id' , $id)->first()->name_he;
}
function years($select_year){

    $currently_selected = date('Y');

    $earliest_year = 1950;
    // Set your latest year you want in the range, in this case we use PHP to just set it to the current year.
    $latest_year = date('Y');

   // print '<select>';
    // Loops over each int[year] from current year, back to the $earliest_year [1950]
   // dd($select_year);
    if ($select_year != null){
    foreach ( range( $latest_year, $earliest_year ) as $i ) {
        // Prints the option with the next year in range.
        print '<option value="'.$i.'"'.($i == $select_year ? 'selected' : '').'>'.$i.'</option>';
    }
    }else{
        foreach ( range( $latest_year, $earliest_year ) as $i ) {
            // Prints the option with the next year in range.
            print '<option value="'.$i.'"' . $i . '>' . $i . '</option>';
        }
    }
  //  print '</select>';

    //return $years = ['1970']
}
//dd(years());
if (!function_exists('setting')) {
    function is_active(string $routeName)
    {

        return null !== request()->segment(2) && request()->segment(2) == $routeName ? 'active menu-open' : '';
    }
}

function getMainSpecialties($id){

   return \App\Models\Mainspecialtie::where('id' , $id)->first()->name_ar;
}

function getSerachTopic($id){

    return \App\Models\Maintopic::where('id' , $id)->first()->name_ar;
}

function getsubTopicByMainTopicId($id){

    return \App\Models\Subtopic::where('maintopic_id' , $id)->get();
}

function getsubSpecialtiesByMainSpecialtiesId($id){

    return \App\Models\Subspecialties::where('mainspecialtie_id' , $id)->get();
}

 function getPriceSubscripeByToken($id){
    return \App\Models\Subscribeplan::where('id' , $id)->first()->token;
}

function getNoOfInstallments(){
    return \App\Models\Subscribeplan::all();
}

function getUserByID($id){
    return \App\Models\User::where('id' , $id)->first();
}

function getTeacherProfileStatusByUserId(){
    $user = \Illuminate\Support\Facades\Auth::user();
    $teacher = \App\Models\Teacher::where('user_id' , $user->id)->first();
    if ($user && $teacher->city_id == null && $teacher->birth_year == null){
        return 1;
    }

    $TeacherDegree =  \App\Models\TeacherDegree::where('user_id' , $user->id)->first();
    $TeacherPrices =  \App\Models\TeacherPrices::where('user_id' , $user->id)->first();
   // $teacherCities =  $teacher->cities()->count();
    $teacherExperience = \App\Models\Teacherexperience::where('teacher_id' , $teacher->id)->first();
    //dd($teacherExperience);
        //  dd($TeacherPrices);
        //dd($user);

    if ($teacherExperience != null){
            return 5;
    }
    elseif ($TeacherPrices != null /*&& $teacher->cities()->count() > 0*/){
        return 4;
    }
    elseif ($teacher->subtopic()->count() > 0){
        return 3;
    }elseif ($teacher  != null && $TeacherDegree != null){
        return 2;
    }elseif ($user && $teacher == null){
            return 1;
        }
   // }
    //return 1;
//    $userId = Illuminate\Support\Facades\Auth::user()->id;
//    $teacher = \App\Models\Teacher::where('user_id' , $userId)->first();
//
  //  if ($teacher->subtopic()->count > 0)
   //

//    if($teacher->cities() != null){
//    if ( $teacher->cities()->count() > 0){
//        return 4;
//    }}
//if ($teacher->subtopic()->count() >0){
//        return 3;
//    }
//elseif (isset($teacher)){
//       return 2;
//   }


 //   $teacher->cities()->get();

   // \App\Models\TeacherPrices::where('user_id' , $userId)->first();

   // dd(\App\Models\TeacherPrices::where('user_id' , $userId)->first());


    function seoUrl($string) {
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }

//    function years(){
//
//        for ($year = 2000 ; $year<=2000; $year--){
//           echo $year;
//        }
//    }

    function universities(){
        return \App\Models\University::all();
    }
}


function getAllRepliesForOneMessageByMessageId($id){
  return  \App\Models\Replymessage::where('message_id' , $id)
      ->Where(function($query) {
          $query ->where('sender_id' ,Auth::id())
              ->orWhere('receiver_id' , Auth::id());
      })
  //    ->where('sender_id' ,Auth::id())
    //  ->orWhere('receiver_id' , Auth::id())
      ->with(['senderUser'=> function($query) {
          $query->select('id', 'name' , 'last_name' , 'email' , 'mobile');
      }]) ->with(['receiverUser' => function($query) {
          $query->select('id', 'name' , 'last_name'  , 'email' , 'mobile');
      }])->get();

}

  function unSeenRepliesMessagesCount($id){
   return \App\Models\Replymessage::where('receiver_id' , Auth::id())
       ->where('message_id' , $id)
       ->count();
  }

function getDegreeNameByTeacherId($teacherId){

   $degree_id =  \App\Models\TeacherDegree::where('teacher_id' , $teacherId)->first()->degree_id;
  return \App\Models\Degree::find($degree_id);

}

function getSubspecialtiesNameByTeacherId($teacherId){

      if(isset(\App\Models\TeacherDegree::where('teacher_id' , $teacherId)->first()->specialty_id)){
          $specialty_id = \App\Models\TeacherDegree::where('teacher_id' , $teacherId)->first()->specialty_id;
          return \App\Models\Subspecialties::find($specialty_id);
    }else
        return null;

}


function getUniversityNameByTeacherId($teacherId){

    $university_id =  \App\Models\TeacherDegree::where('teacher_id' , $teacherId)->first()->university_id;
    return \App\Models\University::find($university_id);

}

function getTimeWorkByUserId(){
    $data = TeacherHourswork::where('user_id' , 4) ->select('day' , 'from' , 'to')->get();
    return response()->json($data);
}

function getUserIdByTeacherId($teacherId){
    return \App\Models\Teacher::where('id' , $teacherId)->first()->user_id;
}

function getUserIdByStudentId($studentId){
    return \App\Models\Student::where('id' , $studentId)->first()->user_id;
}


function getZoomUrlByLessonId($lessonId){
//dd($lessonId);
    $app = \App\Models\Zoomlesson::where('lesson_id' , $lessonId)->first();
//dd($app);
    if ($app!= null){
        return $app->join_url;
    }



}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Replymessage extends Model
{
    use HasFactory;

    public function messages()
    {
        return $this->belongsTo(Message::class , 'message_id' , 'id');
   }


    public function senderUser()
    {
        return $this->belongsTo(User::class , 'sender_id' , 'id');
    }

    public function receiverUser()
    {
        return $this->belongsTo(User::class , 'receiver_id' , 'id');

    }
}

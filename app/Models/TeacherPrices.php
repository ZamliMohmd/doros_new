<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherPrices extends Model
{
    use HasFactory;

    public function price(){
        return $this->belongsTo(Teacher::class  , 'teacher_id' , 'id');
    }

}

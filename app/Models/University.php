<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    use HasFactory;

    public function teacher_university(){
        return $this->hasMany(TeacherDegree::class , 'university_id' , 'id' );
    }
}

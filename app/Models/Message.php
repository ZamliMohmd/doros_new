<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [
        'message', 'user_id' , 'is_seen' , 'token' , 'receiver_id ' , '' , '' , '' , ''
    ];
    public function replies()
    {
        return $this->hasMany(Replymessage::class , 'message_id' , 'id');
    }

    public function senderUser()
    {
        return $this->belongsTo(User::class , 'user_id' , 'id');
    }

    public function receiverUser()
    {
        return $this->belongsTo(User::class , 'receiver_id' , 'id');

    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherDegree extends Model
{
    use HasFactory;

    public function teacher()
    {
        return $this->belongsTo(Teacher::class , 'teacher_id' , 'id');
    }

    public function degree()
    {
        return $this->belongsTo(Degree::class , 'degree_id' , 'id');
    }

    public function university()
    {
        return $this->belongsTo(University::class , 'university_id' , 'id');
    }

    public function subspecialties()
    {
        return $this->belongsTo(Subspecialties::class , 'specialty_id' , 'id');
    }


}

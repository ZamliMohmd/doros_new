<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class , 'user_id' , 'id' );
    }

    public function subtopic()
    {
        return $this->belongsToMany(Subtopic::class , 'teacher_topics', 'teacher_id' , 'subtopic_id');
    }

    public function experience()
    {
        return $this->belongsToMany(Experience::class , 'teacher_experiences', 'teacher_id' , 'experience_id');
    }

    //public function city(){
       // return $this->belongsTo(City::class , 'city_id' , 'id' );
  //  }

    public function university(){
        return $this->belongsTo(University::class);
    }
    public function city(){
        return $this->belongsTo(City::class);
    }

    public function cities(){
        return $this->belongsToMany(City::class , 'teacher_cities' , 'teacher_id' , 'cities_id');
    }
    public function place(){
        return $this->belongsToMany(Place::class , 'teacher_places' , 'teacher_id' , 'place_id');
    }

    public function workhours(){
        return $this->belongsToMany(Place::class , 'teacher_hoursworks' , 'teacher_id' , 'day_id');
    }

    public function price(){
        return $this->hasOne(TeacherPrices::class  , 'teacher_id' , 'id');
    }

    public function degree(){
        return $this->hasMany(TeacherDegree::class , 'teacher_id' , 'id' );
    }


    public function tranzillaPayment(){
        return $this->hasMany(TranzillaPayment::class , 'teacher_id' , 'id' );

    }

    public function subscribeplan(){
        return $this->hasMany(Subscribeplan::class , 'subscribeplan_id' , 'id' );

    }
}

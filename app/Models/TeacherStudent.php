<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherStudent extends Model
{
    use HasFactory;

    public function student(){
        return $this->belongsTo(Student::class , 'student_id' , 'id' );
    }

    public function teacher(){
        return $this->belongsTo(Teacher::class , 'teacher_id' , 'id' );
    }
}

<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\StudentController;
/*
|--------------------------------------------------------------------------
| Student Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('student')->group(function () {
    Route::post('/register' , [StudentController::class, 'register'])->name('student_register');
    Route::get('/test' , [StudentController::class, 'test']);

Route::middleware(['auth'])->group(function () {
Route::get('/messages' , [StudentController::class, 'messages'])->name('student_messages');




});


});



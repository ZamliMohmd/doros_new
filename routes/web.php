<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/lang/{local}', [\App\Http\Controllers\LocalizationController::class, 'index']);

Route::get('/', [App\Http\Controllers\HomesController::class, 'index'])->name('index');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('status', 'verification-link-sent');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');
// ->middleware('verified');




Route::get('/user/verify/{token}', [\App\Http\Controllers\UserController::class , 'verifyNewUser'])->name('activeNewUser');
Route::get('/login', [\App\Http\Controllers\UserController::class , 'loginPage'])->name('login');
Route::post('/loginuser', [\App\Http\Controllers\UserController::class , 'login'])->name('loginuser');

//Route::get('/user/verify/notification', 'ActivateNewUserNotification@sendOfferNotification');


/******************            Search                   ********************/
Route::get('/search' , [\App\Http\Controllers\SerachController::class, 'index']);


Route::get('/chat', [\App\Http\Controllers\TeacherController::class, 'chat']);
Route::get('messages', [\App\Http\Controllers\TeacherController::class, 'fetchMessages']);
Route::post('messages', [\App\Http\Controllers\MessageController::class, 'store']);



Route::get('/search', [\App\Http\Controllers\HomesController::class, 'search']);
Route::post('/new_message', [\App\Http\Controllers\MessageController::class, 'store'])->name('new_message');
Route::get('/getAllMessages' , [\App\Http\Controllers\MessageController::class, 'getAllMessages']);
Route::post('/message/reply' , [\App\Http\Controllers\MessageController::class, 'storeReply']);



Route::get('/allusers' , function (){

    return \App\Models\User::all();
});

//});



Route::get('autocompletes', [\App\Http\Controllers\SerachController::class, 'index']);
Route::get('autocomplete', [\App\Http\Controllers\SerachController::class, 'autocomplete'])->name('autocomplete');


Route::post('/autocomplete/fetchSubtopic', [\App\Http\Controllers\SerachController::class, 'fetchSubtopic'])->name('autocomplete.fetchSubtopic');
Route::post('/autocomplete/fetchCities', [\App\Http\Controllers\SerachController::class, 'fetchCities'])->name('autocomplete.fetchCities');


Route::get('editor', [\App\Http\Controllers\TestController::class, 'editor']);




/****************************************************lessons*********************************************************/


Route::get('/notification', function () {
    return view('layouts.app');
});

?>

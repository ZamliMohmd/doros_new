<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\TeacherController;
/*
|--------------------------------------------------------------------------
| Teacher Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('teacher')->group(function () {
    Route::get('/register', [TeacherController::class, 'registerPage'])->name('teacher_register_page');
    Route::post('/registers', [TeacherController::class, 'register'])->name('teacher_register');

    //Route::middleware(['auth'])->group(function () {

    Route::get('/messages', [TeacherController::class, 'messages'])->name('teacher_messages');

    Route::post('/topic/add' , [TeacherController::class, 'addNewTopic'])->name('teacher.addNewTopic');

    Route::get('/completeprofile' , [TeacherController::class, 'completeProfile'])->name('teacher_completeprofile')->middleware('auth');
    Route::post('/completeprofiles' , [TeacherController::class, 'storeCompleteProfiles'])->name('teacher_storeCompleteProfile');
    Route::post('/completeprofile/addLearningSubjects' , [TeacherController::class, 'addLearningSubjects'])->name('addLearningSubjects');
    Route::post('/completeprofile/priceAndCities' , [TeacherController::class, 'storePriceAndCities'])->name('storePriceAndCities');
    Route::post('/completeprofile/experience' , [TeacherController::class, 'storeExperience'])->name('storeExperience');
    Route::post('/completeprofile/workhours' , [TeacherController::class, 'storeWorkHours'])->name('storeWorkHours');
    Route::post('/completeprofile/teacher_index' , [TeacherController::class, 'teacher_index'])->name('teacher_index');

});


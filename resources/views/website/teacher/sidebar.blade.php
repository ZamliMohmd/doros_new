<div class="col-lg-2 sideRightTeacher" id="sideRightTeacher">
    <ul class="ul_teacher">
        <li>
            <a href="javascript:void(0)" class="keyList">
                            <span class="icon">
                                <i class="droosvip-2-small-caps-1"></i>
                            </span>
                @lang('teacher.account_as_teacher')
                <div class="iconDown">
{{--                    <i class="fas fa-chevron-down"></i>--}}
                </div>
            </a>
            <ul class="listSideRight">
                <li>
                    <a href="{{url('/teacher/index')}}" class="@if (url()->current() == url('/teacher/index'))
                        active
    @endif
    ">
                                    <span class="icon">
                                        <i class="fad fa-user-tie"></i>
                                    </span>
                        @lang('teacher.my_account')
                    </a>
                </li>
                <li>
                    <a href="{{url('/teacher/messages')}}" class="@if (url()->current() == url('/teacher/messages'))
                        active
    @endif">
                                    <span class="icon">
                                        <i class="fal fa-comment-lines"></i>
                                    </span>
                        @lang('teacher.messages')
                    </a>
                </li>
                <li>
                    <a href="" class="@if (url()->current() == url('/teacher/calender'))active @endif">
                                    <span class="icon">
                                        <i class="fal fa-calendar"></i>
                                    </span>
                        @lang('teacher.calender')
                    </a>
                </li>
                <li>
                    <a href="" class="">
                                    <span class="icon">
                                        <i class="fal fa-sticky-note"></i>
                                    </span>
                        @lang('teacher.my_blogs')
                    </a>
                </li>
                <li>
                    <a href="" class=" ">
                                    <span class="icon">
                                        <i class="fad fa-wallet"></i>
                                    </span>
                        @lang('teacher.my_payments')
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>





@extends('layouts.app')

@section('header')
    <style>
        .boxLoginTeacher .col2 .step2 .boxUser {
            padding: 40px 45px 7px;
            width: 175px;
            margin-left: 45px;
        }

        .boxLoginTeacher .col2 .step2 .boxUser .boxImg::before {
            display: none;
        }

        .boxLoginTeacher .col2 .step2 .boxUser img {
            width: 80px;
        }

        .boxLoginTeacher .col2 .step2 .boxUser .boxImg .vip {
            position: absolute;
            right: -35px;
            top: -29px;
        }

        .boxLoginTeacher .col2 .step4 .boxUser .boxImg img {
            width: 80px;
        }

        .boxLoginTeacher .col2 .step4 .boxUser .boxImg .vip {
            position: absolute;
            right: -37px;
            top: -30px;
        }

        .boxLoginTeacher .col2 .step4 .right>div {
            padding: 25px 45px 5px 20px;
        }

        .boxLoginTeacher .col2 .step4 .boxUser p {
            margin-bottom: 3px;
            font-size: 13px;
        }

        .boxLoginTeacher .col2 .step4 .rate {
            font-family: 'arabicBook';
            margin: 10px 0 0;
            font-size: 11px;
        }

        .boxLoginTeacher b {
            font-size: 18px;
        }

        .boxLoginTeacher .col2 .boxStep3 img {
            width: 85px;
        }

        .boxLoginTeacher .col2 .boxStep3 p {
            font-size: 13px;
        }

        .boxLoginTeacher .col2 .body>div.step1 img {
            width: 200px;
        }

        .boxLoginTeacher .col2 .body>div .left {
            padding: 0px 10px;
        }

        .boxLoginTeacher .col2 .body>div .right {
            padding: 0 10px;
        }

        .boxLoginTeacher .col2 .body>div .right span {
            font-size: 13px;
        }

        .boxLoginTeacher .col2 .body>div .center::before {
            height: 155px;
        }
    </style>
    @endsection
@section('content')
    <div class="container boxLoginTeacher">
        <div class="row">
            <div class="col-lg-4 col1">
                <div>
                    <div class="header">
                        <i class="fal fa-user-circle"></i>
                        @lang('teacher.join_teacher')
                    </div>
                    <div class="body">
                        <div class="waysToJoin">
                            <div>
                               {{ \Illuminate\Support\Facades\Session::put('usertype','teacher')}}

                                <a class="join1" href="{{url('auth/google')}}">
                                    @lang('teacher.join_by')
                                    <span class="img">
                                        <img src="{{asset('assets/website/img/google.png')}}" alt="">
                                    </span>
                                </a>
                            </div>
                            <div>
                                <a class="join2" href="{{url('auth/facebook')}}">
                                    @lang('teacher.join_by')
                                    <span class="img">
                                        <img src="{{asset('assets/website/img/facebook.png')}}" alt="">
                                    </span>
                                </a>
                            </div>
                        </div>
                        <form action="{{ url('/teacher/registers')}}" class=" formToJoin formToJoinTeacher"  method="post"
                              id="teacher_join" numCorrect="0">
                         {{csrf_field()}}
                            <div>
                                <label for="name">
                                    @lang('teacher.full_name')
                                </label>
                                <div class="divInput">
                                    <input class="input" requiredX="yes" min="2" max="35" required
                                           errorMin="@lang('teacher.full_name_error')" errorMax="@lang('teacher.full_name_error')"
                                           errorEmpty="@lang('teacher.full_name_error')" placeholder="@lang('teacher.full_name_placeholder')" autocomplete="off" id="teacher_name"
                                           name="name" type="text" value="{{old('name')}}">
                                    <i class="fal fa-user-circle"></i>
                                </div>
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                                <div class="alert-message" id="nameError"></div>

                            </div>
                            <div>
                                <label for="email">
                                    @lang('teacher.email')
                                </label>
                                <div class="divInput">
                                    <input class="input" requiredX="yes" min="3" max="35" required
                                           errorMin="@lang('teacher.email_error')" errorMax="@lang('teacher.email_error')"
                                           errorEmpty="@lang('teacher.email_error')" errorType="@lang('teacher.email_error')"
                                           autocomplete="off" id="teacher_email" type="email" placeholder="@lang('teacher.email_placeholder')" name="email" value="{{old('email')}}">
                                    <i class="fal fa-envelope"></i>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                                <div class="alert-message" id="emailError"></div>

                            </div>
                            <div>
                                <label for="phone">
                                    @lang('teacher.mobile')
                                </label>
                                <div class="divInput">
                                    <input class="input" requiredX="yes" min="8" max="9999999999999" required
                                           errorMax="@lang('teacher.mobile_error')" error05="@lang('teacher.mobile_error')"
                                           errorEmpty="@lang('teacher.mobile_error')" errorMin="@lang('teacher.mobile_error')"
                                           autocomplete="off" name="mobile" id="teacher_mobile" type="number" placeholder="@lang('teacher.mobile_placeholder')" value="{{old('mobile')}}">
                                    <i class="fal fa-phone"></i>
                                </div>
                                @if ($errors->has('mobile'))
                                    <span class="text-danger">{{ $errors->first('mobile') }}</span>
                                @endif
                                <div class="alert-message" id="mobileError"></div>

                            </div>
                            <div>
                                <label for="password">
                                    @lang('teacher.password')
                                </label>
                                <div class="divInput">
                                    <input class="input" requiredX="yes" min="8" max="12" required
                                           errorMin="@lang('teacher.password_error')" errorMax="@lang('teacher.password_error')"
                                           errorEmpty="@lang('teacher.password_error')" autocomplete="off" name="password"
                                           id="teacher_password" type="password" placeholder="@lang('teacher.password_placeholder')" value="{{old('password')}}">
                                    <i class="fal fa-lock-alt"></i>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                                <div class="alert-message" id="passwordError"></div>

                            </div>
                            <div class="s agree">
                                <input class="input" requiredX="yes" id="agree" name="accept_use_terms"
                                       id="teacher_accept_use_terms" value="{{old('accept_use_terms' ,  1 )}}" hidden type="checkbox">
                                <label class="colorText" for="agree">
                                    @lang('teacher.accept') <a href="{{url('/terms')}}">&nbsp; @lang('teacher.terms')</a>
                                </label>
                            </div>
                            @if ($errors->has('accept_use_terms'))
                                <span class="text-danger">{{ $errors->first('accept_use_terms') }}</span>
                            @endif
                            <div class="alert-message" id="accept_use_termsError"></div>

                            <div class="ok">
                                <button class="submit teacher_submit" type="button" id="teacher_register_submit">
                                    @lang('teacher.join')
                                </button>
                            </div>
                            <div class="login">
                                <p>
                                    @lang('teacher.have_account')
                                    <a href="">
                                        @lang('main.login')
                                    </a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
{{--            <div class="col-lg-8 col2">--}}
{{--                <div>--}}
{{--                    <div class="header">--}}
{{--                        <b>--}}
{{--                            @lang('teacher.join_our')--}}
{{--                        </b>--}}
{{--                    </div>--}}
{{--                    <div class="body">--}}
{{--                        <div class="numsMobile" data-active="1">--}}
{{--                            <span class="num1">1</span>--}}
{{--                            <span class="num2">2</span>--}}
{{--                            <span class="num3">3</span>--}}
{{--                            <span class="num4">4</span>--}}
{{--                        </div>--}}
{{--                        <div class="step1">--}}
{{--                            <div class="right">--}}
{{--                                <b>--}}
{{--                                    @lang('teacher.create')--}}
{{--                                    <span>--}}
{{--                                       @lang('teacher.file')--}}
{{--                                    </span>--}}
{{--                                     @lang('teacher.share')--}}
{{--                                </b>--}}
{{--                            </div>--}}
{{--                            <div class="center">--}}
{{--                                <span>--}}
{{--                                    1--}}
{{--                                </span>--}}
{{--                            </div>--}}
{{--                            <div class="left">--}}
{{--                                <img src="{{asset('assets/website/img/imgLoginStep1.png')}}" alt="">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="step2">--}}
{{--                            <div class="right text-right">--}}
{{--                                <div class="boxUser">--}}
{{--                                    <div class="boxImg">--}}
{{--                                        <img src="{{asset('assets/website/img/userStep2.png')}}" alt="">--}}
{{--                                        <img class="vip" src="{{asset('assets/website/img/vip.png')}}" alt="">--}}
{{--                                    </div>--}}
{{--                                    <div class="boxName">--}}
{{--                                        <p>--}}
{{--                                           @lang('teacher.name')--}}
{{--                                        </p>--}}
{{--                                        <p>--}}
{{--                                            @lang('teacher.math')--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                    <span>--}}
{{--                                        <i class="fal fa-fire-alt"></i>--}}
{{--                                      @lang('teacher.new')--}}
{{--                                    </span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="center">--}}
{{--                                <span>--}}
{{--                                    2--}}
{{--                                </span>--}}
{{--                            </div>--}}
{{--                            <div class="left">--}}
{{--                                <b>--}}
{{--                                    @lang('teacher.show')--}}
{{--                                    <span>--}}
{{--                                       @lang('teacher.file')--}}
{{--                                    </span>--}}
{{--                                    @lang('teacher.to_student')--}}
{{--                                </b>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="step3">--}}
{{--                            <div class="right">--}}
{{--                                <b>--}}
{{--                                    @lang('teacher.start')--}}
{{--                                    <span>--}}
{{--                                       @lang('teacher.lessons')--}}
{{--                                    </span>--}}
{{--                                    @lang('teacher.by_your_way')--}}
{{--                                </b>--}}
{{--                            </div>--}}
{{--                            <div class="center">--}}
{{--                                <span>--}}
{{--                                    3--}}
{{--                                </span>--}}
{{--                            </div>--}}
{{--                            <div class="left text-left">--}}
{{--                                <div class="boxStep3">--}}
{{--                                    <div>--}}
{{--                                        <img src="{{asset('assets/website/img/imgStep3.png')}}" alt="">--}}
{{--                                        <p>--}}
{{--                                            online--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                    <div>--}}
{{--                                        <img src="{{asset('assets/website/img/imgStep3.png')}}" alt="">--}}
{{--                                        <p>--}}
{{--                                            Offline--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="step4">--}}
{{--                            <div class="right">--}}
{{--                                <div>--}}
{{--                                    <div class="boxUser">--}}
{{--                                        <div class="boxImg">--}}
{{--                                            <img src="{{asset('assets/website/img/userStep2.png')}}" alt="">--}}
{{--                                            <img class="vip" src="{{asset('assets/website/img/vip.png')}}" alt="">--}}
{{--                                        </div>--}}
{{--                                        <div class="boxName">--}}
{{--                                            <p>--}}
{{--                                                @lang('teacher.name')--}}
{{--                                            </p>--}}
{{--                                            <p>--}}
{{--                                                @lang('teacher.math')--}}
{{--                                            </p>--}}
{{--                                            <p class="stars" data-star="3">--}}
{{--                                                <i class="fas fa-star"></i>--}}
{{--                                                <i class="fas fa-star"></i>--}}
{{--                                                <i class="fas fa-star"></i>--}}
{{--                                                <i class="fas fa-star"></i>--}}
{{--                                                <i class="fas fa-star"></i>--}}
{{--                                            </p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div>--}}
{{--                                        <p class="rate">--}}
{{--                                            <span>--}}
{{--                                                <i class="fas fa-quote-right"></i>--}}
{{--                                            </span>--}}
{{--                                            هنالك العديد من الأنواع المتوفرة لنصوص لوريم--}}
{{--                                            <span>--}}
{{--                                                <i class="fas fa-quote-left"></i>--}}
{{--                                            </span>--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                    <div class="text-right">--}}
{{--                                        <p class="last">--}}
{{--                                            <img src="{{asset('assets/website/img/user.png')}}" alt="">--}}
{{--                                            محمد أحمد--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="center">--}}
{{--                                <span>--}}
{{--                                    4--}}
{{--                                </span>--}}
{{--                            </div>--}}
{{--                            <div class="left">--}}
{{--                                <b>--}}
{{--                                   @lang('teacher.get')--}}
{{--                                    <span>--}}
{{--                                          @lang('teacher.rate')--}}
{{--                                    </span>--}}
{{--                                    @lang('teacher.from_student')--}}
{{--                                </b>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="col-lg-8 col2">
                <div>
                    <div class="header">
                        <b>
                            إنضم للمنصة التعليمية الاولى والوحيدة في الوسط العربي
                        </b>
                    </div>
                    <div class="body">
                        <div class="numsMobile" data-active="1">
                            <span class="num1">1</span>
                            <span class="num2">2</span>
                            <span class="num3">3</span>
                            <span class="num4">4</span>
                        </div>
                        <div class="step1">
                            <div class="right">
                                <b>
                                    أنشىء
                                    <span>
                                        ملفك الخاص
                                    </span>
                                    وشاركه مع الجميع
                                </b>
                            </div>
                            <div class="center">
                                <span>
                                    1
                                </span>
                            </div>
                            <div class="left">
                                <img src="/assets/website/img/imgLoginStep1.png" alt="">
                            </div>
                        </div>
                        <div class="step2">
                            <div class="right text-right">
                                <div class="boxUser">
                                    <div class="boxImg">
                                        <img src="/assets/website/img/userStep2.png" alt="">
                                        <img class="vip" src="/assets/website/img/vip.png" alt="">
                                    </div>
                                    <!-- <div class="boxName">
                                        <p>
                                            أحمد العكلوك
                                        </p>
                                        <p>
                                            رياضيات، كفر عقب
                                        </p>
                                    </div> -->
                                    <span>
                                        <i class="fal fa-fire-alt"></i>
                                        جديد
                                    </span>
                                </div>
                            </div>
                            <div class="center">
                                <span>
                                    2
                                </span>
                            </div>
                            <div class="left">
                                <b>
                                    نعرض
                                    <span>
                                        ملفك الخاص
                                    </span>
                                    امام آلاف الطلاب
                                </b>
                            </div>
                        </div>
                        <div class="step3">
                            <div class="right">
                                <b>
                                    إبدأ
                                    <span>
                                        بإعطاء الدروس
                                    </span>
                                    بالطريقة المناسبة لك
                                </b>
                            </div>
                            <div class="center">
                                <span>
                                    3
                                </span>
                            </div>
                            <div class="left text-left">
                                <div class="boxStep3">
                                    <div>
                                        <img src="/assets/website/img/imgStep3.png" alt="">
                                        <p>
                                            online
                                        </p>
                                    </div>
                                    <div>
                                        <img src="/assets/website/img/ofline.jpeg" alt="">
                                        <p>
                                            offline
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step4">
                            <div class="right">
                                <div>
                                    <div class="boxUser">
                                        <div class="boxImg">
                                            <img src="/assets/website/img/userStep2.png" alt="">
                                            <img class="vip" src="img/vip.png" alt="">
                                        </div>
                                        <div class="boxName">
                                            <p class="stars" data-star="3">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </p>
                                        </div>
                                    </div>
                                    <div>
                                        <p class="rate">
                                            <span>
                                                <i class="fas fa-quote-right"></i>
                                            </span>
                                            هنالك العديد من الأنواع المتوفرة لنصوص لوريم
                                            <span>
                                                <i class="fas fa-quote-left"></i>
                                            </span>
                                        </p>
                                    </div>
                                    <!-- <div class="text-right">
                                        <p class="last">
                                            <img src="img/user.png" alt="">
                                            محمد أحمد
                                        </p>
                                    </div> -->
                                </div>
                            </div>
                            <div class="center">
                                <span>
                                    4
                                </span>
                            </div>
                            <div class="left">
                                <b>
                                    أحصل على
                                    <span>
                                        تقييمات
                                    </span>
                                    من الطلاب
                                </b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
{{--    <script type="text/javascript">--}}
{{--        $.ajaxSetup({--}}
{{--            headers: {--}}
{{--                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
{{--            }--}}
{{--        });--}}

{{--        $(".teacher_submit").click(function(e){--}}

{{--            e.preventDefault();--}}
{{--            $(this).html('جاري الحفظ...'); //this changes the html to Saving...--}}
{{--            $(this).attr('disabled', true); //this will disable the button onclick--}}

{{--            var name = $("input[name=name]").val();--}}
{{--            var email = $("input[name=email]").val();--}}
{{--            var password = $("input[name=password]").val();--}}
{{--            var mobile = $("input[name=mobile]").val();--}}
{{--            var _token = $("input[name=_token]").val();--}}
{{--            var url = '{{ url('/teacher/registers')}}';--}}

{{--            $.ajax({--}}
{{--                url:url,--}}
{{--                method:'POST',--}}
{{--                data:{--}}
{{--                    name:name,--}}
{{--                    email:email,--}}
{{--                    password:password,--}}
{{--                    mobile:mobile,--}}
{{--                    _token:_token,--}}
{{--                },--}}

{{--                success:function(response){--}}
{{--                    if(response.status == true){--}}
{{--                        alert(response.msg) //Message come from controller--}}
{{--                        $('#nameError,#emailError,#mobileError,#passwordError').remove();--}}
{{--                        $("#teacher_join").trigger('reset');--}}
{{--                        $(".teacher_submit").html('انضم'); //this changes the html to Saving...--}}
{{--                        setTimeout(3000);--}}
{{--                        window.location.replace(response.redirect);--}}
{{--                        //   $('#popUpJoinAsStudent').dialog('close');--}}
{{--                        //   $(document).ready(function () {--}}
{{--                        //       $(".popUpJoinAsStudent").hidden;--}}
{{--                        //    $(".popUpJoinAsStudent").addClass("show");--}}
{{--                        //   })--}}
{{--                    }else{--}}
{{--                        $(this).html('انضم'); //this changes the html to Saving...--}}

{{--                    }--}}
{{--                },--}}
{{--                /*--}}
{{--                error:function(error){--}}
{{--                   alert(log(error))--}}
{{--                    console.log(error)--}}
{{--                }*/--}}
{{--                error: function( response )--}}
{{--                {--}}
{{--                    if (response.responseJSON.errors.name){--}}
{{--                        $('#nameError').text(response.responseJSON.errors.name)--}}
{{--                        $('#teacher_name').addClass("error").removeClass("correct");--}}
{{--                    }--}}
{{--                    if (response.responseJSON.errors.email){--}}
{{--                        $('#emailError').text(response.responseJSON.errors.email);--}}
{{--                        $('#teacher_email').addClass("error").removeClass("correct");--}}
{{--                    }--}}
{{--                    if (response.responseJSON.errors.mobile){--}}
{{--                        $('#mobileError').text(response.responseJSON.errors.mobile);--}}
{{--                        $('#teacher_mobile').addClass("error").removeClass("correct");--}}
{{--                    }--}}
{{--                    if (response.responseJSON.errors.password){--}}
{{--                        $('#passwordError').text(response.responseJSON.errors.password);--}}
{{--                        $('#teacher_password').addClass("error").removeClass("correct");--}}
{{--                    }--}}
{{--                    if (response.responseJSON.errors.accept_use_terms){--}}
{{--                        $('#accept_use_termsError').text(response.responseJSON.errors.accept_use_terms);--}}
{{--                        $('#teacher_accept_use_termsError').addClass("error").removeClass("correct");--}}
{{--                    }--}}


{{--                    $(".teacher_submit").html('انضم');--}}
{{--                    $(".teacher_submit").attr('disabled', false);--}}
{{--                    --}}
{{--                }--}}
{{--            });--}}
{{--          --}}
{{--        });--}}

    </script>
@endsection

@extends('layouts.app')
@section('header')
    <link rel="stylesheet" href="{{--url('assets/website/css/completeDateTeacher.css')--}}">
    <link rel="stylesheet" href="{{--url('assets/website/css/amsify.select.css')--}}">

<style>

.selectHour{
    isplay: block;
    padding: 6px 35px 6px 8px;
    border: 1px solid #E8E8E8;
    border-radius: 8px;
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    -ms-border-radius: 8px;
    -o-border-radius: 8px;
    width: 100%;
    font-size: 15px;

    vertical-align: middle;
    border-top: 1px solid #E8E8E8;
    color: #696871;
    padding: 13px 5px;

}
.selectHour select {
    border-radius: 8px;
    width: 100px;
    text-align: center;
    height: 30px;

}
.selectHour select option{
    border-color: #F68213;
    text-align: center;
    max-height: 60px;
}
.selectHour select option:hover {
    background-color: yellow;
}

select.decorated option:hover {
    box-shadow: 0 0 10px 100px #1882A8 inset;
}

.selectHour select option:hover{
    color: #F68213;
}
    .dropHour {
        position: absolute;
        top: 38px;
        right: 0;
        width: 100%;
        z-index: 999;
        background-color: #fff;
        border: 1px solid #e8e8e8;
        /* display: flex; */
        height: 140px;
        overflow: hidden;
        display: none;
    }

    .dropHour.show {
        display: flex;
    }

    .dropHour ul {
        list-style: none;
        padding: 0;
        margin: 0;
        width: 50%;
        height: 100%;
        overflow: auto;
    }

    .dropHour ul:nth-of-type(1) {
        border-left: 1px solid #e8e8e8;
    }

    .dropHour ul li {
        padding: 7px;
        cursor: pointer;
    }

    .dropHour ul li:nth-of-type(odd) {
        background-color: #fdfdfd;
    }

    .dropHour ul li:nth-of-type(even) {
        background-color: #f7f7f7;
    }

    .dropHour ul li:hover,
    .dropHour ul li.select {
        background-color: var(--colorText1);
        color: #fff;
    }

    .loginHome.remmber .loginX {
        display: none;
    }

    .loginHome.login .remmberPassord {
        display: none;
    }

    /* style pop up login home */

     .select2-selection__arrow{
         margin-right: 20px;
     }
    input.timepicker {
        width: 100px !important;
        text-align: center !important;
        margin: auto;
    }
    .step2 .education>span:not(.icon) {
        color: #696871;
        border: 1px solid #E8E8E8;
        padding: 4px 12px;
        font-size: 14px;
        border-radius: 8px;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        -ms-border-radius: 8px;
        -o-border-radius: 8px;
        margin: 5px 0 5px 5px;
        display: inline-flex;
        align-items: center;
    }
    .step2 .education>span:not(.icon) {
        color: #FFF;
        background-color: var(--colorText1);
    }
    .step2 .education>span {
        color: var(--colorText2);
    }
    .step2 .education>span i {
        font-size: 18px;
        vertical-align: middle;
        display: none;
    }
    .step2 .education>span i {
        font-size: 19px;
        color: #FFF;
        display: inline-block;
        position: static !important;
        margin-left: 5px;
    }
    .step3 .education>span:not(.icon) {
        color: #696871;
        border: 1px solid #E8E8E8;
        padding: 4px 12px;
        font-size: 14px;
        border-radius: 8px;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        -ms-border-radius: 8px;
        -o-border-radius: 8px;
        margin: 5px 0 5px 5px;
        display: inline-flex;
        align-items: center;
    }
    .step3 .education>span:not(.icon) {
        color: #FFF;
        background-color: var(--colorText1);
    }
    .step3 .education>span {
        color: var(--colorText2);
    }
    .step3 .education>span i {
        font-size: 18px;
        vertical-align: middle;
        display: none;
    }
    .step3 .education>span i {
        font-size: 19px;
        color: #FFF;
        display: inline-block;
        position: static !important;
        margin-left: 5px;
    }
    #universities+span.select2 .select2-selection--single {
        /*height: 43px;*/
        /*margin-top: 0px;*/
    }
    #specializations+span.select2 .select2-selection--single {
        /*height: 43px;*/
        /*margin-top: 0px;*/
    }
    .completeDataTeacher .step5 .tableHour tr.rowTo td {
        position: relative;
    }
    .btnDeleteHour {
        position: absolute!important;
        left: 7px;
        bottom: -37px;
        background-color: #FF4747;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        color: #fff;
        border-radius: 8px;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        -ms-border-radius: 8px;
        -o-border-radius: 8px;
        padding: 2px 7px;
        margin: 0;
        font-size: 12px;
    }
    .completeDataTeacher .step5 .tableHour tr:nth-of-type(3) td .btnDeleteHour {
        display: none;
    }
    .select2-container--default .select2-results__group{
        font-weight: bold;
        color: orange;
        /* background: #e8e8e8;*/
        margin-right: 10px;
        font-size: 14px;
    }

    .completeDataTeacher .dataTeacher input[type="checkbox"]+label,
    .completeDataTeacher .dataTeacher input[type="radio"]+label {
        display: flex;
        align-items: center;
        font-size: 14px;
    }

    .completeDataTeacher .dataTeacher input[type="checkbox"]+label::before,
    .completeDataTeacher .dataTeacher input[type="radio"]+label::before {
        content: "\f111";
        font-family: 'Font Awesome 5 Pro';
        font-weight: 300;
        font-size: 18px;
        margin-left: 7px;
    }

    .completeDataTeacher .dataTeacher input[type="checkbox"]:checked+label::before,
    .completeDataTeacher .dataTeacher input[type="radio"]:checked+label::before {
        content: "\f058";
        font-family: 'Font Awesome 5 Pro';
        font-weight: 900;
        font-size: 18px;
        margin-left: 7px;
        color: var(--colorText1);
    }

    .completeDataTeacher form.dataTeacher input[type="checkbox"]+label,
    .completeDataTeacher form.dataTeacher input[type="radio"]+label {
        display: flex;
        align-items: center;
        font-size: 14px;
    }

    .completeDataTeacher form.dataTeacher input[type="checkbox"]+label::before,
    .completeDataTeacher form.dataTeacher input[type="radio"]+label::before {
        content: "\f111";
        font-family: 'Font Awesome 5 Pro';
        font-weight: 300;
        font-size: 18px;
        margin-left: 7px;
    }

    .completeDataTeacher form.dataTeacher input[type="checkbox"]:checked+label::before,
    .completeDataTeacher form.dataTeacher input[type="radio"]:checked+label::before {
        content: "\f058";
        font-family: 'Font Awesome 5 Pro';
        font-weight: 900;
        font-size: 18px;
        margin-left: 7px;
        color: var(--colorText1);
    }
    .fixError small{
        bottom: -12px !important;
    }
.Delete {
    font-size: 12px;
    background-color: #fff;
    color: black;
    position: absolute;
    left: -30px;
    top: unset;
    bottom: 3px;
    border-radius: 50%;
    border: 1px solid #E8E8E8;
    padding: 6px;
    display: inline-flex;


    /*align-items: center;*/
    /*justify-content: center;*/
    /*-webkit-border-radius: 50%;*/
    /*-moz-border-radius: 50%;*/
    /*-ms-border-radius: 50%;*/
    /*-o-border-radius: 50%;*/
    /*width: 30px;*/
    /*height: 30px;*/
}

.Delete:hover {
    color: #fff;
}

.Delete i {
    position: static !important;
    font-size: 18px !important;
    margin-left: 0px;
}

.formAddSubject .Delete {
    font-size: 12px;
    background-color: rgb(218, 62, 62);
    color: #fff;
    position: absolute;
    left: -30px;
    top: unset;
    bottom: 5px;
    border-radius: 50%;
    padding: 6px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    width: 26px;
    height: 26px;
}

.formAddSubject .divInput:first-of-type .Delete {
    display: none;
}

.tableHour input:disabled {
    background-color: rgba(239, 239, 239, 0.3);
    opacity: 0.5;
}
</style>

@endsection
@section('content')

    <div class="container completeData completeDataTeacher">

        <div class="row rowHeadPage">
            <div class="col-12 titlePage">
                <span class="icon">
                    <i class="fal fa-copy"></i>
                </span>
               @lang('teacher.build_profile')
            </div>
        </div>
        <div class="row rowStep" step="{{getTeacherProfileStatusByUserId()}}">
            <span class="step1">1</span>
            <span class="space">
                <b>
                  @lang('teacher.personal_info')
                </b>
            </span>
            <span class="step2">2</span>
            <span class="space">
                <b>
                      @lang('teacher.topics_learning')
                </b>
            </span>
            <span class="step3">3</span>
            <span class="space">
                <b>
                     @lang('teacher.places_prices')
                </b>
            </span>
            <span class="step4">4</span>
            <span class="space">
                <b>
                    @lang('teacher.marketing_experience')
                </b>
            </span>
            <span class="step5">5</span>
        </div>

        <div class="dataTeacher">

            <form action="{{route('teacher_storeCompleteProfile')}}"  id="teacherCompleteProfileStep1s" method="post"
                  class="dataTeacher" enctype="multipart/form-data">

                {{csrf_field()}}
                <input type="hidden"  name="user_id" value="{{\Illuminate\Support\Facades\Auth::user()->id}}">

                <div class="row step1">
                    <div class="col-12 mt-0">
                        <span class="titleStep">
                             @lang('teacher.personal_info')
                        </span>
                    </div>
                    <div class="col-lg-9 mt-0">
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div>
                                    <label for="firstName">
                                        @lang('teacher.personal_name')
                                    </label>
                                    <div class="divInput">
                                        <input requiredX="yes" class="input" min="2" max="25"
                                               errorMin="@lang('teacher.required')" errorMax="@lang('teacher.required')"
                                               errorEmpty="@lang('teacher.required')" autocomplete="off" id="firstName"
                                               type="text" name="name" value="{{$user->name}}">
                                        <i class="fal fa-user-circle"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="fixError">
                                    <label for="lastName">
                                        @lang('teacher.family_name')
                                    </label>
                                    <div class="divInput">
                                        <input requiredX="yes" class="input" min="2" max="25"
                                               errorMin="@lang('teacher.required')" errorMax="@lang('teacher.required')"
                                               errorEmpty="@lang('teacher.required')" autocomplete="off" id="lastName"
                                               type="text" name="last_name" value="{{$user->last_name}}">
                                        <i class="fal fa-user-circle"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="fixError">
                                    <label for="gender">
                                        @lang('teacher.gender')
                                    </label>
                                    <div class="divInput chx">
                                        <input requiredX="yes" @if($teacher->gender_id == 1) checked @endif errorEmpty="@lang('teacher.required')" type="radio"
                                               class="input" name="gender_id" hidden id="gender1" value="1">
                                        <label for="gender1">
                                            @lang('teacher.male')
                                        </label>
                                        <input requiredX="yes" @if($teacher->gender_id == 2) checked @endif   errorEmpty="@lang('teacher.required')" type="radio"
                                               class="input" name="gender_id" hidden id="gender2" value="2">
                                        <label for="gender2">
                                            @lang('teacher.female')
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="fixError">
                                    <label for="requiredCities">
                                        @lang('teacher.city')
                                    </label>
                                    <div class="divInput">
                                        <select requiredX="yes" dir="rtl" id="requiredCities" class="form-control select2 input" errorEmpty="@lang('teacher.required')"  searchable name="city_id"
                                        >
                                            <option ></option>
                                            @foreach($cities as $city)
                                                <option value="{{$city->id}}" {{ ( $city->id == $teacher->city_id) ? 'selected' : '' }}>
                                                    @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                        {{$city->name_ar}}
                                                    @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                        {{$city->name_he}}
                                                    @endif

                                                </option>
                                            @endforeach
                                        </select>
                                        <i class="fal fa-map-marker-alt"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="fixError">
                                    <label for="date">
                                        @lang('teacher.birth_year')
                                    </label>
                                    <div class="divInput">
                                        <select requiredX="yes"  autocomplete="off" id="date" style="border-radius: 5px 5px 5px 5px; width: 100%; max-width: 100%;" class="input select2" errorEmpty="@lang('teacher.required')" placeholder="سنة الميلاد"  name="birth_year">
                                            <option ></option>
                                            {{years($teacher->birth_year)}}
                                        </select>
                                        <i class="fal fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div>
                                    <label for="phone">
                                        @lang('teacher.mobile_no')
                                    </label>
                                    <div class="divInput">
                                        <input requiredX="yes" min="10" max="9999999999999"
                                               errorMax="@lang('teacher.required')" error05="@lang('teacher.required')" errorEmpty="@lang('teacher.required')" errorMin="@lang('teacher.required')"
                                               autocomplete="off" class="input" id="phone" name="mobile" type="number"
                                               value="{{$user->mobile}}">
                                        <i class="fal fa-phone"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div>
                                    <label for="email">
                                        @lang('teacher.whatsapp_no')
                                    </label>
                                    <div class="divInput">
                                        <input requiredX="yes" errorEmpty="@lang('teacher.required')" autocomplete="off"
                                               id="whatsapp" type="number" class="input" name="whatsapp" value="{{$user->mobile}}">
                                        <i class="fal fa-phone"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 align-self-center pb-0">
                                <div class="divInput">
                                    <input requiredX="no" @if($teacher->is_accept_appear_mobile_no == 1) checked @endif  type="checkbox" class="input"
                                           name="is_accept_appear_mobile_no" hidden id="agreeShow" value="1">
                                    <label class="colorText" for="agreeShow" style="font-size: 13px;">
                                        @lang('teacher.accept_appear_mobile_no')
                                    </label>
                                </div>
                                <div class="divInput">
                                    <input requiredX="no" @if($teacher->is_accept_whatsapp_msg == 1) checked @endif type="checkbox" class="input"
                                           name="is_accept_whatsapp_msg" hidden id="agreeWhatsapp" value="1">
                                    <label class="colorText" for="agreeWhatsapp" style="font-size: 13px;">
                                        @lang('teacher.accept_whatsapp_msg')
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col colImg mt-0" style="cursor: pointer">
                        <div>
                            <input class="imgFile" hidden type="file" name="image" id="img"  >
                            <label class="img" for="img">
                                <img class="imgFile" src="@if(isset($teacher->image))
                                                             {{ asset($teacher->image)}}
                                                             @else
                                                        {{ asset('assets/website/img/camera.svg')}}
                                                            @endif
                                    " alt="" style="cursor: pointer">
                            </label>
                            <a href="" class="btn btn-block">
                                @lang('teacher.upload_image')
                                <img class="px-3" src="{{asset('assets/website/img/facebook.svg')}}" alt="" style="display: inline-flex">
                            </a>
                        </div>
                    </div>
                    <div class="col-12 completeDataPart mt-0">
                        <div class="row edit">
                            <div class="col-12 eduCol mt-0">
                                <div class="row rowTitleTeacher m-0">
                                    <span class="col-lg-3 mt-3 col-md-6 col-12 px-2 divTS1">
                                        <label for="edu1-01">
                                             @lang('teacher.degree')
                                        </label>
                                        <br>
                                        <div class="divInput">
{{--                                                  <input list="browsers2" id="degree" class="input" requiredX="yes" errorEmpty="@lang('teacher.required')" name="browser2" id="browser2" value="">--}}
{{--                                                    <datalist id="browsers2">--}}
{{--                                                                @foreach($degrees as $degree)--}}
{{--                                                            <option value="{{$degree->name_ar}}" data-value="{{$degree->id}}"></option>--}}
{{--                                                        @endforeach--}}

{{--                                                    </datalist>--}}
{{--                                                    <input name="degree_id"  type="hidden" id="hi2">--}}

                                            <select requiredX="yes" errorEmpty="@lang('teacher.required')"
                                                    name="degree_id" id="degree" class="select2 input">
                                                <option  ></option>
                                                @foreach($degrees as $degree)
                                                    <option value="{{$degree->id}}" @if($degreeTeacher != null) {{ ( $degree->id == $degreeTeacher->degree_id) ? 'selected' : '' }} @endif style="font-weight: bold">
                                                    {{$degree->name_ar}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <span class="icon">
                                            <i style="bottom: 3px;" class="fal fa-book-alt"></i>
                                        </span>
                                    </span>
                                    <span class="col-lg-3 mt-3 col-md-6 col-12 px-2 divTS2">
                                        <label for="edu1-02">
                                             @lang('teacher.univercity_name')
                                        </label>
                                        <br>
                                        <div class="divInput">
{{--                                             <input list="universities" id="universitie" class="input" requiredX="yes" errorEmpty="@lang('teacher.required')" name="universities"  value="">--}}
{{--                                                    <datalist id="universities">--}}
{{--                                                         @foreach($universities as $university)--}}
{{--                                                            <option value="{{$university->name_ar}}" data-value="{{$university->id}}"></option>--}}
{{--                                                          @endforeach--}}

{{--                                                    </datalist>--}}
{{--                                                    <input name="university_id"  type="hidden" id="university_id">--}}

                                            <select requiredX="yes" dir="rtl" id="universities"
                                                    errorEmpty="@lang('teacher.required')" class="form-control
                                     select2 input" searchable name="university_id">
                                                <option style="    margin-right: 31px;font-weight: bold"
                                                        selected></option>

                                                @foreach($universities as $university)
                                                    <option value="{{$university->id}}" @if($degreeTeacher != null) {{ ( $university->id == $degreeTeacher->university_id) ? 'selected' : '' }} @endif style="font-weight: bold">
                                                    {{$university->name_ar}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <span class="icon">
                                            <i class="fal fa-user-graduate" style="bottom: 2px"></i>
                                        </span>
                                    </span>
                                    <span class="col-lg-3 mt-3 col-md-6 col-12 px-2 divTS3">
                                        <label for="edu1-03">
                                             @lang('teacher.specialization')
                                        </label>
                                        <br>
                                        <div class="divInput">
                                            <select errorEmpty="@lang('teacher.required')" requiredX="yes" dir="rtl"
                                                    id="specializations" class="form-control select2 input" searchable
                                                    name="specialization_id">
                                                <option ></option>
                                                @foreach($Mainspecialties as $specialty)
                                                    <optgroup label="{{$specialty->name_ar}}" style="color: maroon">
                                                    @foreach(getsubSpecialtiesByMainSpecialtiesId($specialty->id) as
                                                    $subSpecialty)
                                                            <option value="{{$subSpecialty->id}}" @if($degreeTeacher != null) {{( $subSpecialty->id == $degreeTeacher->specialty_id) ? 'selected' : ''}} @endif style="font-weight: bold">
                                                        {{$subSpecialty->name_ar}}</option>
                                                        @endforeach
                                                </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                        <span class="icon">
                                            <i class="fal fa-school" style="bottom: 7px"></i>
                                        </span>
                                    </span>
                                    <span class="col-lg-3 mt-3 col-md-6 col-12 px-2 divTS4">
                                        <label>
                                            @lang('teacher.from')
                                        </label>
                                         &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                          <label>
                                            @lang('teacher.to')
                                        </label>
                                        <br>
                                        <div class="date">

                                            <div class="position-relative" style="flex: 0 0 50%; max-width: 50%;">
                                                <div class="divInput">
                                              <select requiredX="yes" name="from_year" style="border-radius: 5px 5px 5px 5px; width: 100%; max-width: 100%;" class="input select2" errorEmpty="@lang('teacher.required')" placeholder=" @lang('teacher.from')" id='date-dropdown1' name="from">
                                                   <option ></option>
                                                     @if(isset($degreeTeacher->from_year))
                                                 {{years($degreeTeacher->from_year)}}
                                                  @else
                                                      {{years(null)}}
                                                  @endif
                                              </select>
                                            <i class="fal fa-calendar"></i>
                                                </div>
                                            </div>

                                            <div class="position-relative" style="flex: 0 0 50%; max-width: 50%;">
                                                <div class="divInput">
                                                     <select requiredX="yes" name="to_year" style="border-radius: 5px 5px 5px 5px; width: 100%; max-width: 100%;" class="input select2" errorEmpty="@lang('teacher.required')" placeholder=" @lang('teacher.to')" name="to">
                                                   <option ></option>
                                                         @if(isset($degreeTeacher->to_year))
                                                {{years($degreeTeacher->to_year)}}
                                                         @else
                                                             {{years(null)}}
                                                             @endif
                                              </select>

                                            <i class="fal fa-calendar"></i>
                                                </div>
                                            </div>

                                        </div>
                                    </span>
                                    <a class="Delete " href="javascript:void(0)">
                                        <i class="far fa-minus" style="padding-right: 2px;"></i>
                                    </a>

                                </div>
                            </div>
                            <div class="col-12 addEduCol noBT">
                                <a href="javascript:void(0)">
                                    <span class="icon">
                                        <i style="bottom: unset; right: unset;" class="fal fa-plus"></i>
                                    </span>
                                    @lang('teacher.add_new_degree')
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-0 text-right">
                        <div class="ok">
                            <button class="firstStep_submit submit next" active type="button" id="step1" style="font-weight: bold">
                                @lang('teacher.next_step')
                            </button>
                            <!-- <button class="nextStep1 next" type="button">الخطوة التالية</button> -->

                        </div>

                    </div>

                </div>

            </form>

            <form action="{{route('addLearningSubjects')}}" method="post" name="step2" id="teacherCompleteProfileStep2"
                  class="dataTeacher">
            {{csrf_field()}}
                <div class="row step2">
                    <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
                    <div class="col-12">
                        <span class="titleStep">
                            @lang('teacher.topics_learning')
                        </span>
                    </div>
                    <div class="col-12 education2">
                        <div>
                            <label for="requiredSubject">
                                @lang('teacher.choose_topics')
                            </label>
                            <div class="divInput mt-2">
                                <select requiredX="yes" errorEmpty=" @lang('teacher.required')" dir="rtl" id="subjects"
                                        class="form-control select2 input subjects" searchable name="topics[]" multiple="multiple">
                                    <option value=""></option>
                                    @foreach($maintopics as $key=> $maintopic)
                                    <optgroup label="{{$maintopic->name_ar}}">
                                        @foreach(getsubTopicByMainTopicId($maintopic->id) as $subtopic)
                                            <option value="{{$subtopic->id}}"
                                            @foreach($topics as $value)
                                                {{ ( $subtopic->id == $value->id) ? 'selected' : '' }}
                                                @endforeach
                                            >{{$maintopic->name_ar}}-
                                                {{$subtopic->name_ar}}</option>
                                        @endforeach
                                    </optgroup>
                                    @endforeach
                                </select>
                                <i style="top: 10px;" class="fal fa-map-marker-alt"></i>
                            </div>

                        </div>

                        <div>
                            <p>
                                @lang('teacher.not_find')
                            </p>
                            <a href="javascript:void(0)" class="addSubjectBtn">
                                @lang('teacher.add_topic')
                            </a>

                        </div>

                    </div>
                    <div class="col-12 mt-3 text-right">
                        <div class="ok">
                            <button type="button" class="back">
                                @lang('teacher.back_step')
                            </button>
                            <button class="secondStep_submit next submit" type="button">
                                @lang('teacher.next_step')
                            </button>

                        </div>
                    </div>

                </div>
            </form>

            <form method="post" action="{{route('storePriceAndCities')}}" id="teacherCompleteProfileStep3"
                  class="dataTeacher">
                {{csrf_field()}}
                <div class="row step3">
                    <div class="col-12">
                        <span class="titleStep">
                           @lang('teacher.places_prices')
                        </span>
                    </div>

                    <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
                    <div class="col-12 country2 mb-2">
                        <label for="city2">
                            @lang('teacher.choose_cities')
                        </label>
                        <div class="position-relative divInput">

                            <select requiredX="no" errorEmpty="@lang('teacher.required')" dir="rtl" id="cities2"
                                    class="form-control select2 input" searchable name="teacher_city[]" onKeyDown="preventBackspace()" multiple="multiple">

                                @foreach($cities as $city)
                                    <option value="{{$city->id}}"
                                    @foreach($teacherCities as $value)
                                        {{ ( $city->id == $value->id) ? 'selected' : '' }}
                                        @endforeach >
                                        @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                            {{$city->name_ar}}
                                        @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                        {{$city->name_ar}}
                                            @endif
                                    </option>
                                @endforeach
                            </select>

                            <label for="city2" class="icon iconDown">
                                <i class="fas fa-chevron-down"></i>
                            </label>
                            <label for="city2" class="icon iconPlace">
                                <i class="fad fa-map-marked-alt" style="bottom: unset; top: 12px;"></i>
                            </label>
                        </div>

                    </div>
                    <div class="col-12 m-0 p-0 px-3 mt-3">
                        <span style="display: inline-block; font-size: 15px; color: #696871;">
                             @lang('teacher.lesson_place_price')
                        </span>
                    </div>
                    <div class="col-12 lesson mt-2">
                        <div class="position-relative mb-3">
                            <span class="divInput d-inline-block">
                                <input checked {{--isset($prices->price_at_teacher) && $prices->price_at_teacher != null ? 'checked' : ''--}} type="checkbox" name="lesson" id="lesson1" hidden>
                                <label for="lesson1">
                                    <i class="notcheck fal fa-circle"></i>
                                    <i class="check fas fa-check-circle"></i>
                                    @lang('teacher.at_teacher')
                                </label>
                                <div class="inputPrice">
                                    <input type="text" errorEmpty="@lang('teacher.required')" name="price_at_teacher"
                                           class="input" placeholder="@lang('teacher.insert_hour_price')" id="" value="{{isset($prices->price_at_teacher) && $prices->price_at_teacher != null ? $prices->price_at_teacher : null}}">
                                    <i class="fal fa-shekel-sign"></i>
                                </div>
                            </span>
                        </div>
                        <div class="position-relative mb-3">
                            <span class="divInput d-inline-block">
                                <input checked type="checkbox" name="lesson" id="lesson2" hidden>
                                <label for="lesson2">
                                    <i class="notcheck fal fa-circle"></i>
                                    <i class="check fas fa-check-circle"></i>
                                     @lang('teacher.at_student')
                                </label>
                                <div class="inputPrice">
                                    <input type="text" errorEmpty="@lang('teacher.required')" name="price_at_student"
                                           class="input" placeholder="@lang('teacher.insert_hour_price')" id="" value="{{isset($prices->price_at_student) && $prices->price_at_student != null ? $prices->price_at_student : null}}">
                                    <i class="fal fa-shekel-sign"></i>
                                </div>
                            </span>
                        </div>
                        <div class="position-relative mb-3">
                            <span class="divInput d-inline-block">
                                <input {{isset($prices->price_online) && $prices->price_online != null ? 'checked' : ''}} type="checkbox" name="lesson" id="lesson3" hidden>
                                <label for="lesson3">
                                    <i class="notcheck fal fa-circle"></i>
                                    <i class="check fas fa-check-circle"></i>
                                      @lang('teacher.onlines')
                                </label>
                                <div class="inputPrice">
                                    <input errorEmpty="@lang('teacher.required')" type="text" name="price_online"
                                           class="input" placeholder="@lang('teacher.insert_hour_price')" id="" value="{{isset($prices->price_online) && $prices->price_online != null ? $prices->price_online : null}}">
                                    <i class="fal fa-shekel-sign"></i>
                                </div>
                            </span>
                        </div>
                        <div class="position-relative mb-3">
                            <span class="divInput d-inline-block">
                                <input {{isset($prices->price_at_others) && $prices->price_at_others != null ? 'checked' : ''}} type="checkbox" name="lesson" id="lesson4" hidden>
                                <label for="lesson4">
                                    <i class="notcheck fal fa-circle"></i>
                                    <i class="check fas fa-check-circle"></i>
                                    @lang('teacher.others_place')
                                </label>
                                <div class="inputPrice">
                                    <input   type="text" errorEmpty="@lang('teacher.required')" name="price_at_others"
                                           class="input" placeholder="@lang('teacher.insert_hour_price')" id="" value="{{isset($prices->price_at_others) && $prices->price_at_others != null ? $prices->price_at_others : null}}">
                                    <i class="fal fa-shekel-sign"></i>
                                </div>
                            </span>
                        </div>
                        <div class="mb-3 ifDiscount">
                            <p>
                                @lang('teacher.discount')
                            </p>
                            <span class="position-relative ">
                                <input {{isset($prices->is_discount) && $prices->is_discount != null ? 'checked' : ''}} type="checkbox" name="is_discount" class="input" id="discount" value="1"
                                       hidden>
                                <label for="discount">
                                    <i class="notcheck fal fa-circle"></i>
                                    <i class="check fas fa-check-circle"></i>
                                    @lang('teacher.yes')
                                </label>
                                <div class="inputPrice position-relative">
                                    <div class="divInput">
                                        <input errorEmpty="@lang('teacher.required')" type="text"
                                               name="discount_percentage" class="input"
                                               placeholder=" @lang('teacher.discount_percentage')"
                                               min="5" max="40" name="" id="" value="{{isset($prices->discount_percentage) && $prices->discount_percentage != null ? $prices->discount_percentage : null}}">
                                        <i class="fal fa-badge-percent"></i>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="col-12 mt-0 text-right">
                        <div class="ok">
                            <button type="button" class="back">
                                @lang('teacher.back_step')
                            </button>
                            <button class="thirdStep_submit submit" type="button">
                                @lang('teacher.next_step')
                            </button>

                        </div>
                    </div>

                </div>
            </form>

            <form action="{{route('storeExperience')}}" method="post" id="teacherCompleteProfileStep4"
                  enctype="multipart/form-data" class="dataTeacher">
                {{csrf_field()}}
                <div class="row step4">
                    <div class="col-12 mt-0 mb-1">
                        <span class="titleStep">
                            @lang('teacher.marketing_experience')
                        </span>
                    </div>
                    <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}">
                    <div class="col-12 mb-2">
                        <p class="subTitle">
                            @lang('teacher.experience')

                        </p>
                    </div>

                    <div class="col-12 mt-0 exp">
                        @foreach($experiences as $key => $exper)
                        <div>
                            <input id="exp{{$key}}" type="checkbox" class="input" hidden name="experiences[]"
                                   @foreach($experience as $exp)
                                   @if ( $exp->id == $exper->id)
                                   checked
                                   @endif
                                   @endforeach
                                   value="{{$exper->id}}">
                            <label for="exp{{$key}}">
                                <i class="notcheck fal fa-circle"></i>
                                <i class="check fas fa-check-circle"></i>
                                @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                {{$exper->name_ar}}
                                @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                    {{$exper->name_ar}}
                                    @endif
                            </label>
                        </div>
                        <small class="error error1" style="display: none;">
                            @lang('teacher.must_choose_one')
                        </small>


                    @endforeach
                    </div>
                    <div class="col-12">
                        <p class="subTitle float-left mb-2">
                            @lang('teacher.marketing')
                        </p>
                        <span class="limitText float-right">
                            <b>
                                0
                            </b>
                            /
                            120
                        </span>
                    </div>
                    <div class="col-12 mt-0">
                        <div class="input">
                            <input requiredX="yes" errorEmpty="@lang('teacher.required')" type="text" maxlength="120" name="marketing_text" value="{{$teacher->marketing_text}}" id="marketing_text" class="py-2 textMarketStep4"
                                   placeholder="@lang('teacher.marketing_120_leters')">
                            <i style="bottom: 18px; right: 25px;" class="fal fa-sticky-note"></i>
                        </div>
                        <small class="error error2" style="display: none;">
                            @lang('teacher.required')
                        </small>
                    </div>
                    <div class="col-lg-12 d-flex mt-3">
                        <div class="video position-relative">
                            <p class="subTitle">
                                <i style="position: static;" class="fas fa-film"></i>
                                @lang('teacher.video')
                            </p>
                            <input type="file" name="video" accept="" hidden id="video">
                            <label for="video">
                                <div id="video_here">
                                    <video style="display: none;" width="100%" height="240" controls src="{{url('assets/website/img/movie.mp4')}}">
{{--                                        <source src="--}}{{--asset($teacher->video)--}}{{--">--}}
                                    </video>
                                    <i class="fal fa-cloud-upload"></i>
                                </div>
                                <p>
                                    @lang('teacher.upload_video')
                                </p>
                                <small class="error error3" style="display: none; right: 25px; bottom: 5px;">
                                    @lang('teacher.required')
                                </small>
                            </label>
                        </div>
                        <div class="aboutMe position-relative">
                            <label for="about" class="subTitle d-block">
                                <i class="fal fa-sticky-note"></i>
                                @lang('teacher.cv')
                            </label>
                            <textarea requiredX="no" errorEmpty="@lang('teacher.required')" name="cv_text" value="{{$teacher->cv_text}}" id="about"></textarea>
                            <small class="error error4" style="display: none; right: 25px; bottom: 5px;">
                                @lang('teacher.required')
                            </small>
                        </div>
                    </div>
                    <div class="col-12 text-right mt-3">
                        <div class="ok">
                            <button type="button" class="back">
                                @lang('teacher.back_step')
                            </button>
                            <button class="fourthStep_submit submit" type="button">
                                @lang('teacher.next_step')
                            </button>

                        </div>

                    </div>

                </div>
            </form>

            <form action="{{route('storeWorkHours')}}" method="post" class="row step5">
                {{csrf_field()}}
                <div class="col-12 mt-0 mb-1">
                    <span class="titleStep" style="color:#F68213;font-weight: bold">
                      @lang('teacher.days_hours')
                        <br>
                    </span>
                    <span><br> @lang('teacher.choose_days_hours')</span>
                </div>
                <div class="col-12 mt-0">
                    <div class="table-responsive">
                        <table class="table tableHour">
                            <tr>
                                <td></td>
                                <td> @lang('teacher.sunday') </td>
                                <td> @lang('teacher.monday') </td>
                                <td> @lang('teacher.tuesday') </td>
                                <td> @lang('teacher.wednesday') </td>
                                <td> @lang('teacher.thursday') </td>
                                <td> @lang('teacher.friday') </td>
                                <td> @lang('teacher.saturday') </td>
                            </tr>
                            <tr class="rowFrom">
                                <td>@lang('teacher.from_hour')</td>
                                <td class="sun">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="sun_from[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
{{--<dvi class="selectHour" >--}}
{{--    <select  class="form-control  " style="align-content: center;max-height: 50px" >--}}
{{--        <option>07:00</option>--}}
{{--        <option>07:30</option>--}}
{{--        <option>08:00</option>--}}
{{--        <option>08:30</option>--}}
{{--        <option>09:00</option>--}}
{{--        <option>09:30</option>--}}
{{--        <option>10:00</option>--}}
{{--        <option>10:30</option>--}}
{{--        <option>11:00</option>--}}
{{--        <option>11:30</option>--}}
{{--        <option>12:00</option>--}}
{{--        <option>12:30</option>--}}
{{--        <option>13:00</option>--}}
{{--        <option>13:30</option>--}}
{{--        <option>14:00</option>--}}
{{--        <option>14:30</option>--}}
{{--        <option>15:00</option>--}}
{{--        <option>15:30</option>--}}
{{--    </select>--}}
{{--</dvi>--}}




                                <td class="mon">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="">07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="mon_from[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
                                <td class="tues">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="tues_from[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
                                <td class="wed">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" placeholder="00:00" type="text" autocomplete="off" name="wend_from[]" placeholder="00:00">
                                    </div>
                                </td>
                                <td class="thr">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="thr_from[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
                                <td class="fri">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="fri_from[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
                                <td class="sat">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="sat_from[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
                            </tr>
                            <tr class="rowTo">
                                <td>@lang('teacher.to_hour')</td>
                                <td class="sun">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li >07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li class="select">22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="sun_to[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
                                <td class="mon">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li class="select">22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="mon_to[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
                                <td class="tues">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li >07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li class="select">22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="tues_to[]"  autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
                                <td class="wed">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li>07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li class="select">22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="wend_to[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
                                <td class="thr">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li >07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li class="select">22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="thr_to[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
                                <td class="fri">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li class="select">07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li>22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="fri_to[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                </td>
                                <td class="sat">
                                    <div class="d-inline-flex text-center m-auto position-relative">
                                        <div class="dropHour">
                                            <ul class="ulMin">
                                                <li class="select">00</li>
                                                <li>30</li>
                                            </ul>
                                            <ul class="ulHour">
                                                <li >07</li>
                                                <li>08</li>
                                                <li>09</li>
                                                <li>10</li>
                                                <li>11</li>
                                                <li>12</li>
                                                <li>13</li>
                                                <li>14</li>
                                                <li>15</li>
                                                <li>16</li>
                                                <li>17</li>
                                                <li>18</li>
                                                <li>19</li>
                                                <li>20</li>
                                                <li>21</li>
                                                <li class="select">22</li>
                                            </ul>
                                        </div>
                                        <input style="border-radius:8px; width: 100px; text-align: center;" type="text" name="sat_to[]" autocomplete="off" placeholder="00:00">
                                    </div>
                                    <p class="btnDeleteHour">
                                        <i class="far fa-minus" style="padding-right: 2px;"></i>
                                    </p>
                                </td>
                            </tr>
                            <tr class="addHours">
                                <td colspan="8" data-day="sun">
                                    <span>
                                        <i class="fal fa-plus"></i>
                                        @lang('teacher.add_more_hours')
                                    </span>
                                </td>
                            </tr>
                            <tr class="noActive">
                                <td></td>
                                <td>
                                    <input type="checkbox" hidden name="" id="sun">
                                    <label for="sun">
                                        @lang('teacher.not_available')
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="mon">
                                    <label for="mon">
                                        @lang('teacher.not_available')
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="tues">
                                    <label for="tues">
                                        @lang('teacher.not_available')
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="wed">
                                    <label for="wed">
                                        @lang('teacher.not_available')
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="thr">
                                    <label for="thr">
                                        @lang('teacher.not_available')
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="fri">
                                    <label for="fri">
                                        @lang('teacher.not_available')
                                    </label>
                                </td>
                                <td>
                                    <input type="checkbox" hidden name="" id="sat">
                                    <label for="sat">
                                        @lang('teacher.not_available')
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-12 text-right">
                    <div class="ok">
                        <button type="button" class="back">
                            @lang('teacher.back_step')
                        </button>
                        <button class="submit" type="submit">
                            @lang('teacher.join_now')
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

{{--    <div class="popUp popUpFromTeacher popUpAddSubject">--}}
{{--        <div>--}}
{{--            <div class="headerPopUp">--}}
{{--                <i class="fal fa-user-circle"></i>--}}
{{--                @lang('teacher.add_topic_you_want')--}}
{{--                <span class="closeX">--}}
{{--                    <i class="fal fa-times"></i>--}}
{{--                </span>--}}
{{--            </div>--}}
{{--            <div class="bodyPopUp">--}}
{{--                <form class="formAddSubject" numCorrect="0" action="">--}}
{{--                    <div class="inputSub">--}}
{{--                        <label for="title">--}}
{{--                            @lang('teacher.write_topic_here')--}}
{{--                        </label>--}}
{{--                        <div class="divInput">--}}
{{--                            <input id="title" type="text" value="">--}}
{{--                            <i class="fal fa-user-circle"></i>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="addAnotherInputSub">--}}
{{--                        <i style="position: static!important;" class="fal fa-plus"></i>--}}
{{--                        @lang('teacher.add_another_topic')--}}
{{--                    </div>--}}
{{--                    <p class="note">--}}
{{--                        @lang('teacher.topic_will_add_after_accept')--}}
{{--                    </p>--}}
{{--                    <div class="ok">--}}
{{--                        <button type="button" class="closeX">--}}
{{--                            @lang('teacher.cancel')--}}
{{--                        </button>--}}
{{--                        <button class="submit" type="button">--}}
{{--                            @lang('teacher.add')--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </form>--}}

{{--                <form class="formAddSubject" numCorrect="0" action="">--}}
{{--                    <div class="inputSub">--}}
{{--                        <label for="title">--}}
{{--                            اكتب الموضوع هنا--}}
{{--                        </label>--}}
{{--                        <div class="divInput">--}}
{{--                            <input id="title" type="text" value="">--}}
{{--                            <i class="fal fa-user-circle"></i>--}}
{{--                            <a class="Delete" href="javascript:void(0)">--}}
{{--                                <i class="far fa-minus"></i>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="addAnotherInputSub">--}}
{{--                        <i style="position: static!important;" class="fal fa-plus"></i>--}}
{{--                        أضف موضوع أخر--}}
{{--                    </div>--}}
{{--                    <p class="note">--}}
{{--                        سيتم إضافة الموضوع بعد الموافقة عليه--}}
{{--                    </p>--}}
{{--                    <div class="ok">--}}
{{--                        <button type="button" class="closeX">--}}
{{--                            إلغاء--}}
{{--                        </button>--}}
{{--                        <button class="submit" type="button">--}}
{{--                            إضافة--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


@endsection


@section('footer')
    <script>
        $('.select2#requiredCities').select2({
            placeholder: "@lang('teacher.choose')",
            dir: 'rtl',
            maximumSelectionLength: '1'
        });

        $('.select2#requiredSubject').select2({
            placeholder: "@lang('teacher.choose')",
            dir: 'rtl',
            maximumSelectionLength: '20'
        });



        $('.select2#universities').select2({
            placeholder: "@lang('teacher.choose')",
            dir: 'rtl',
            maximumSelectionLength: '2'
        });

        $('.select2#specializations').select2({
            placeholder: "@lang('teacher.choose')",
            dir: 'rtl',
            maximumSelectionLength: '2'
        });

        $('.select2').select2({
            placeholder: "@lang('teacher.choose')",
            dir: 'rtl',

        });
    </script>

    <script>
   //   let dateDropdown1 = document.getElementById('date-dropdown1');
   //
   //   let currentYear1 = new Date().getFullYear();
   //   let earliestYear1 = 1970;
   //  while (currentYear1 >= earliestYear1) {
   //      let dateOption1 = document.createElement('option');
   //      dateOption1.text = currentYear1;
   //      dateOption1.value = currentYear1;
   //      dateDropdown1.add(dateOption1);
   //      currentYear1 -= 1;
   //  }
   //
   //  let dateDropdown2 = document.getElementById('date-dropdown2');
   //
   //  let currentYear2 = new Date().getFullYear();
   //  let earliestYear2 = 1970;
   //  while (currentYear2 >= earliestYear2) {
   //      let dateOption2 = document.createElement('option')
   //      dateOption2.text = currentYear2;
   //      dateOption2.value = currentYear2;
   //      dateDropdown2.add(dateOption2);
   //      currentYear2 -= 1;
   // }
   //
   //   let dateDropdown3 = document.getElementById('date-dropdown2');
   //
   //   let currentYear3 = new Date().getFullYear();
   //   let earliestYear3 = 1970;
   //   while (currentYear3 >= earliestYear23) {
   //       let dateOption3 = document.createElement('option')
   //       dateOption3.text = currentYear3;
   //       dateOption3.value = currentYear3;
   //       dateDropdown3.add(dateOption2);
   //       currentYear3 -= 1;
   //   }



    // $(document).on("click", ".completeDataTeacher .noActive input", function () {
    //     if ($(this).is(':checked')) {
    //         $(".completeDataTeacher td." + $(this).attr("id") + " input").attr("disabled", "disabled");
    //     } else {
    //         $(".completeDataTeacher td." + $(this).attr("id") + " input").removeAttr("disabled");
    //     }
    // })



</script>


{{--      <script type="text/javascript">--}}
{{--            $.ajaxSetup({--}}
{{--                headers: {--}}
{{--                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
{{--                }--}}
{{--            });--}}
{{--            $(".nextStep1").hide(); //this changes the html to Saving...--}}
{{--    --}}
{{--            $(".firstStep_submit").click(function(e){--}}
{{--    --}}
{{--                e.preventDefault();--}}
{{--                $(this).html('جاري الحفظ...'); //this changes the html to Saving...--}}
{{--                $(this).attr('disabled', true); //this will disable the button onclick--}}
{{--    --}}
{{--                var name = $("input[name=name]").val();--}}
{{--                var last_name = $("input[name=last_name]").val();--}}
{{--                var user_id = $("input[name=user_id]").val();--}}
{{--                var gender_id = $("input[name=gender_id]").val();--}}
{{--                var city_id = $("input[name=city_id]").val();--}}
{{--                var birth_year = $("input[name=birth_year]").val();--}}
{{--                var whatsapp = $("input[name=whatsapp]").val();--}}
{{--                var is_accept_appear_mobile_no = $("input[name=is_accept_appear_mobile_no]").val();--}}
{{--                var is_accept_whatsapp_msg = $("input[name=is_accept_whatsapp_msg]").val();--}}
{{--                var image = $("input[name=image]").val();--}}
{{--                var degree_id = $("input[name=degree_id]").val();--}}
{{--                var university_id = $("input[name=university_id]").val();--}}
{{--                var specialization_id = $("input[name=specialization_id]").val();--}}
{{--                var from_year = $("input[name=from_year]").val();--}}
{{--                var to_year = $("input[name=to_year]").val();--}}
{{--               // var email = $("input[name=email]").val();--}}
{{--              //  var password = $("input[name=password]").val();--}}
{{--                var mobile = $("input[name=mobile]").val();--}}
{{--                var _token = $("input[name=_token]").val();--}}
{{--                var url = '{{ route('teacher_storeCompleteProfile') }}';--}}
{{--    --}}
{{--                $.ajax({--}}
{{--                    url:url,--}}
{{--                    method:'POST',--}}
{{--                   enctype: 'multipart/form-data',--}}
{{--    --}}
{{--                    data:{--}}
{{--                        name:name,--}}
{{--                        last_name:last_name,--}}
{{--                        user_id:user_id,--}}
{{--                      //  email:email,--}}
{{--                      //  password:password,--}}
{{--                        gender_id:gender_id,--}}
{{--                        city_id:city_id,--}}
{{--                        birth_year:birth_year,--}}
{{--                        whatsapp:whatsapp,--}}
{{--                        is_accept_appear_mobile_no:is_accept_appear_mobile_no,--}}
{{--                        is_accept_whatsapp_msg:is_accept_whatsapp_msg,--}}
{{--                        image:image,--}}
{{--                        degree_id:degree_id,--}}
{{--                        university_id:university_id,--}}
{{--                        specialization_id:specialization_id,--}}
{{--                        from_year:from_year,--}}
{{--                        to_year:to_year,--}}
{{--                        mobile:mobile,--}}
{{--                        _token:_token,--}}
{{--                    },--}}
{{--                      processData: false,--}}
{{--                      contentType:false,--}}
{{--                       cache:false,--}}
{{--                    success:function(response){--}}
{{--                        if(response.status == true){--}}
{{--                         //   alert(response.msg)--}}
{{--                         //   $(".nextStep1").show()--}}
{{--                         //   if (confirm(response.msg)) {--}}
{{--                                $(".nextStep1").trigger("click")--}}
{{--                          //  }--}}
{{--                        //Message come from controller--}}
{{--                            $('#nameError,#emailError,#mobileError,#passwordError').remove();--}}
{{--                            $("#teacherCompleteProfileStep1").trigger('reset');--}}
{{--                            $(".firstStep_submit").html('انضم').hide(); //this changes the html to Saving...--}}
{{--                            $(".nextStep1").show(); //this changes the html to Saving...--}}
{{--                              $(".nextStep1").show(); //this changes the html to Saving...--}}
{{--                            setTimeout(3000);--}}
{{--                          //  window.location.replace(response.redirect);--}}
{{--    --}}
{{--                        }else{--}}
{{--                            $(this).html('انضم'); //this changes the html to Saving...--}}
{{--    --}}
{{--                        }--}}
{{--                    },--}}
{{--                    /*--}}
{{--                    error:function(error){--}}
{{--                       alert(log(error))--}}
{{--                        console.log(error)--}}
{{--                    }*/--}}
{{--                    error: function( response )--}}
{{--                    {--}}
{{--                        if (response.responseJSON.errors.name){--}}
{{--                            $('#nameError').text(response.responseJSON.errors.name)--}}
{{--                            $('#stu_name').addClass("error").removeClass("correct");--}}
{{--                        }--}}
{{--                        if (response.responseJSON.errors.email){--}}
{{--                            $('#emailError').text(response.responseJSON.errors.email);--}}
{{--                            $('#stu_email').addClass("error").removeClass("correct");--}}
{{--                        }--}}
{{--                        if (response.responseJSON.errors.mobile){--}}
{{--                            $('#mobileError').text(response.responseJSON.errors.mobile);--}}
{{--                            $('#stu_mobile').addClass("error").removeClass("correct");--}}
{{--                        }--}}
{{--                        if (response.responseJSON.errors.password){--}}
{{--                            $('#passwordError').text(response.responseJSON.errors.password);--}}
{{--                            $('#stu_password').addClass("error").removeClass("correct");--}}
{{--                        }--}}
{{--                        if (response.responseJSON.errors.accept_use_terms){--}}
{{--                            $('#accept_use_termsError').text(response.responseJSON.errors.accept_use_terms);--}}
{{--                            $('#stu_accept_use_termsError').addClass("error").removeClass("correct");--}}
{{--                        }--}}
{{--                        --}}
{{--                        $(".firstStep_submit").html('انضم');--}}
{{--                        $(".firstStep_submit").attr('disabled', false);--}}
{{--    --}}
{{--                        // $($($(this).parent(".divInput")).next("small.error")).show();--}}
{{--                        //  $($($(this).parent(".divInput")).next("small.error")).text($(this).attr("errorEmpty"));--}}
{{--                        //  $(this).attr("true", "no");--}}
{{--    --}}
{{--                        //  console.log(data);--}}
{{--                        //  alert(this.data.error);--}}
{{--                        //   printErrorMsg(data.error);--}}
{{--                    }--}}
{{--                });--}}
{{--                /*--}}
{{--                        function printErrorMsg (msg) {--}}
{{--                            $(".student_submit").find("ul").html('');--}}
{{--                            $(".student_submit").css('display','block');--}}
{{--                            $.each( msg, function( key, value ) {--}}
{{--                                $(".student_submit-msg").find("ul").append('<li>'+value+'</li>');--}}
{{--                            });--}}
{{--                        }--}}
{{--                 */--}}
{{--            });--}}
{{--    --}}
{{--            /////////////////////////////////ٍStep2/////////////////////////////////////--}}
{{--    --}}
{{--            $(".nextStep2").hide(); //this changes the html to Saving...--}}
{{--    --}}
{{--            $(".secondStep_submit").click(function(e){--}}
{{--    --}}
{{--                e.preventDefault();--}}
{{--                $(this).html('جاري الحفظ...'); //this changes the html to Saving...--}}
{{--                $(this).attr('disabled', true); //this will disable the button onclick--}}
{{--    --}}
{{--                var topics = $("input[name=topics]").val();--}}
{{--                var _token = $("input[name=_token]").val();--}}
{{--                var url = '{{ route('addLearningSubjects') }}';--}}
{{--    --}}
{{--                var data = $('step2').select2('topics');--}}
{{--                if(data) {--}}
{{--                    alert(data.text);--}}
{{--                }--}}
{{--    --}}
{{--                $.ajax({--}}
{{--                    url:url,--}}
{{--                    method:'POST',--}}
{{--                    data:{--}}
{{--                        topics:topics,--}}
{{--                        _token:_token,--}}
{{--                    },--}}
{{--    --}}
{{--                    success:function(response){--}}
{{--                        if(response.status == true){--}}
{{--                          //  alert(response.msg) //Message come from controller--}}
{{--    --}}
{{--                            $('#nameError').remove();--}}
{{--                            $("#teacherCompleteProfileStep2").trigger('reset');--}}
{{--                            $(".secondStep_submit").html('انضم').attr('disabled', false).hide();--}}
{{--                          //  $(".secondStep_submit").attr('disabled', false);--}}
{{--                         //   $(".secondStep_submit").html('انضم').hide(); //this changes the html to Saving...--}}
{{--                            $(".nextStep2").trigger("click"); //this changes the html to Saving...--}}
{{--                            setTimeout(3000);--}}
{{--    --}}
{{--                        }else{--}}
{{--                            $(this).html('حفظ'); //this changes the html to Saving...--}}
{{--    --}}
{{--                        }--}}
{{--                    },--}}
{{--    --}}
{{--                    error: function( response )--}}
{{--                    {--}}
{{--                        if (response.responseJSON.errors.name){--}}
{{--                            $('#nameError').text(response.responseJSON.errors.name)--}}
{{--                            $('#topics').addClass("error").removeClass("correct");--}}
{{--                        }--}}
{{--                        $(".secondStep_submit").html('انضم');--}}
{{--                        $(".secondStep_submit").attr('disabled', false);--}}
{{--    --}}
{{--                    }--}}
{{--                });--}}
{{--            });--}}
{{--    --}}
{{--            /////////////////////////////////ٍStep3/////////////////////////////////////--}}
{{--    --}}
{{--            $(".nextStep3").hide(); //this changes the html to Saving...--}}
{{--    --}}
{{--            $(".thirdStep_submit").click(function(e){--}}
{{--    --}}
{{--                e.preventDefault();--}}
{{--                $(this).html('جاري الحفظ...'); //this changes the html to Saving...--}}
{{--                $(this).attr('disabled', true); //this will disable the button onclick--}}
{{--    --}}
{{--                var teacher_city = $("input[name=teacher_city]").val();--}}
{{--                var user_id = $("input[name=user_id]").val();--}}
{{--                var price_at_teacher = $("input[name=price_at_teacher]").val();--}}
{{--                var price_at_student = $("input[name=price_at_student]").val();--}}
{{--                var price_at_others = $("input[name=price_at_others]").val();--}}
{{--                var price_online = $("input[name=price_online]").val();--}}
{{--                var is_discount = $("input[name=is_discount]").val();--}}
{{--                var discount_percentage = $("input[name=discount_percentage]").val();--}}
{{--                var _token = $("input[name=_token]").val();--}}
{{--                var url = '{{ route('storePriceAndCities') }}';--}}
{{--    --}}
{{--                $.ajax({--}}
{{--                    url:url,--}}
{{--                    method:'POST',--}}
{{--                    data:{--}}
{{--                        teacher_city:teacher_city,--}}
{{--                        user_id:user_id,--}}
{{--                        price_at_teacher:price_at_teacher,--}}
{{--                        price_at_student:price_at_student,--}}
{{--                        price_at_others:price_at_others,--}}
{{--                        price_online:price_online,--}}
{{--                        is_discount:is_discount,--}}
{{--                        discount_percentage:discount_percentage,--}}
{{--                        _token:_token,--}}
{{--                    },--}}
{{--    --}}
{{--                    success:function(response){--}}
{{--                        if(response.status == true){--}}
{{--                            alert(response.msg) //Message come from controller--}}
{{--                            $('#nameError').remove();--}}
{{--                            $("#teacherCompleteProfileStep3").trigger('reset');--}}
{{--                            $(".thirdStep_submit").html('انضم').attr('disabled', false).hide();--}}
{{--                            //  $(".secondStep_submit").attr('disabled', false);--}}
{{--                            //   $(".secondStep_submit").html('انضم').hide(); //this changes the html to Saving...--}}
{{--                            $(".nextStep3").show(); //this changes the html to Saving...--}}
{{--                            setTimeout(3000);--}}
{{--    --}}
{{--                        }else{--}}
{{--                            $(this).html('حفظ'); //this changes the html to Saving...--}}
{{--    --}}
{{--                        }--}}
{{--                    },--}}
{{--    --}}
{{--                    error: function( response )--}}
{{--                    {--}}
{{--                        if (response.responseJSON.errors.name){--}}
{{--                            $('#nameError').text(response.responseJSON.errors.name)--}}
{{--                            $('#stu_name').addClass("error").removeClass("correct");--}}
{{--                        }--}}
{{--                        $(".thirdStep_submit").html('انضم');--}}
{{--                        $(".thirdStep_submit").attr('disabled', false);--}}
{{--    --}}
{{--                    }--}}
{{--                });--}}
{{--            });--}}
{{--    ////////////////////////////step4///////////////////////--}}
{{--    --}}
{{--            $(".nextStep4").hide(); //this changes the html to Saving...--}}
{{--    --}}
{{--            $(".fourthStep_submits").click(function(e){--}}
{{--    --}}
{{--                e.preventDefault();--}}
{{--                $(this).html('جاري الحفظ...'); //this changes the html to Saving...--}}
{{--                $(this).attr('disabled', true); //this will disable the button onclick--}}
{{--                alert('تم تسجيل الخبرات بنجاح. الرجاء الانتقال للخطوة التالية')--}}
{{--                $(".fourthStep_submit").html('انضم').attr('disabled', false).hide();--}}
{{--                $(".nextStep4").show(); //this changes the html to Saving...--}}
{{--                setTimeout(3000);--}}
{{--               // var teacher_city = $("input[name=teacher_city]").val();--}}
{{--               // var user_id = $("input[name=user_id]").val();--}}
{{--                //var price_at_teacher = $("input[name=price_at_teacher]").val();--}}
{{--               // var price_at_student = $("input[name=price_at_student]").val();--}}
{{--              //  var price_at_others = $("input[name=price_at_others]").val();--}}
{{--               // var price_online = $("input[name=price_online]").val();--}}
{{--               // var is_discount = $("input[name=is_discount]").val();--}}
{{--               // var discount_percentage = $("input[name=discount_percentage]").val();--}}
{{--               // var _token = $("input[name=_token]").val();--}}
{{--    --}}
{{--    --}}
{{--                $.ajax({--}}
{{--                    url:url,--}}
{{--                    method:'POST',--}}
{{--                    data:{--}}
{{--                       // teacher_city:teacher_city,--}}
{{--                      //  user_id:user_id,--}}
{{--                      //  price_at_teacher:price_at_teacher,--}}
{{--                      ///  price_at_student:price_at_student,--}}
{{--                      //  price_at_others:price_at_others,--}}
{{--                       // price_online:price_online,--}}
{{--                      //  is_discount:is_discount,--}}
{{--                      //  discount_percentage:discount_percentage,--}}
{{--                        _token:_token,--}}
{{--                    },--}}
{{--    --}}
{{--                    success:function(response){--}}
{{--                        if(response.status == true){--}}
{{--                            alert(response.msg) //Message come from controller--}}
{{--                            $('#nameError').remove();--}}
{{--                            $("#teacherCompleteProfileStep4").trigger('reset');--}}
{{--                            $(".fourthStep_submit").html('انضم').attr('disabled', false).hide();--}}
{{--                            //  $(".secondStep_submit").attr('disabled', false);--}}
{{--                            //   $(".secondStep_submit").html('انضم').hide(); //this changes the html to Saving...--}}
{{--                            $(".nextStep4").show(); //this changes the html to Saving...--}}
{{--                            setTimeout(3000);--}}
{{--    --}}
{{--                        }else{--}}
{{--                            $(this).html('حفظ'); //this changes the html to Saving...--}}
{{--    --}}
{{--                        }--}}
{{--                    },--}}
{{--    --}}
{{--                    error: function( response )--}}
{{--                    {--}}
{{--                        if (response.responseJSON.errors.name){--}}
{{--                            $('#nameError').text(response.responseJSON.errors.name)--}}
{{--                            $('#stu_name').addClass("error").removeClass("correct");--}}
{{--                        }--}}
{{--                        $(".thirdStep_submit").html('انضم');--}}
{{--                        $(".thirdStep_submit").attr('disabled', false);--}}
{{--    --}}
{{--                    }--}}
{{--                });--}}
{{--    --}}
{{--                ////////////////////////////step5///////////////////////--}}
{{--    --}}
{{--                $(".nextStep5").hide(); //this changes the html to Saving...--}}
{{--    --}}
{{--                $(".fivthStep_submits").click(function(e) {--}}
{{--    --}}
{{--                    e.preventDefault();--}}
{{--                    $(this).html('جاري الحفظ...'); //this changes the html to Saving...--}}
{{--                    $(this).attr('disabled', true); //this will disable the button onclick--}}
{{--                    alert('تم تسجيل المواعيد بنجاح. الرجاء الانتقال للخطوة التالية')--}}
{{--                    $(".fivthStep_submits").html('انضم').attr('disabled', false).hide();--}}
{{--                    $(".nextStep5").show(); //this changes the html to Saving...--}}
{{--                    setTimeout(3000);--}}
{{--                });--}}
{{--            });--}}
{{--        </script>--}}
<script>
    $(document).on("keydown", ".subjects", function (e) {
        if (e.keyCode === 8 || e.key === "Delete") {
            return e.preventDefault();
        }
    })


    $($(".rowFrom").find("input")).val("07:00")
    $($(".rowTo").find("input")).val("22:00")


    $(document).on("click", ".popUp .closeX", function () {
        $($(this).parents(".popUp")).removeClass("show")
    })

    $(document).on("click", ".completeDataTeacher .Delete", function () {
        $(this).parents(".rowTitleTeacher").remove()
    })


    $(document).on("click", ".popUpsd .addHours td span", function () {
        $('<tr class="rowFrom">' + $(".rowFrom").html() + '</tr>').insertBefore(".popUpsd tr.addHours");
        $('<tr class="rowTo">' + $(".rowTo").html() + '</tr>').insertBefore(".popUpsd tr.addHours");
    })

    $(document).on("click", ".ulHour", function () {
        $(".dropHour").removeClass("show");
    })


        $('#degree').change(function () {
            if ($(this).val() == '6') {
                $($($(this).parents(".divTS1")).siblings(".divTS2")).hide();
                $($($($(this).parents(".divTS1")).siblings(".divTS2")).find("select, input")).attr("true",
                    "yes");
                $($($(this).parents(".divTS1")).siblings(".divTS3")).hide();
                $($($($(this).parents(".divTS1")).siblings(".divTS3")).find("select, input")).attr("true",
                    "yes");
                $($($(this).parents(".divTS1")).siblings(".divTS4")).hide();
                $($($($(this).parents(".divTS1")).siblings(".divTS4")).find("select, input")).attr("true",
                    "yes");
            } else {
                $($($(this).parents(".divTS1")).siblings(".divTS2")).show();
                $($($($(this).parents(".divTS1")).siblings(".divTS2")).find("select, input")).attr("true",
                    "no");
                $($($(this).parents(".divTS1")).siblings(".divTS3")).show();
                $($($($(this).parents(".divTS1")).siblings(".divTS3")).find("select, input")).attr("true",
                    "no");
                $($($(this).parents(".divTS1")).siblings(".divTS4")).show();
                $($($($(this).parents(".divTS1")).siblings(".divTS4")).find("select, input")).attr("true",
                    "no");
            }
        });


    /***********************************/
    $(document).on("change", "#browser2", function () {
        var value = $(this).val();
        $($(this).siblings("#degree_id")).val($('#browsers2 [value="' + value + '"]').data('value'))
    })

    $(document).on("change", "#universities", function () {
        var value = $(this).val();
        $($(this).siblings("#university_id")).val($('#universities [value="' + value + '"]').data('value'))
    })





    $(document).on("click", ".completeDataTeacher .dataTeacher .step2 .back", function () {
        $(".completeDataTeacher .rowStep").attr("step", "1");
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
    })

    $(document).on("click", ".completeDataTeacher .dataTeacher .step3 .back", function () {
        $(".completeDataTeacher .rowStep").attr("step", "2");
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
    })


    $(document).on("click", ".completeDataTeacher .dataTeacher .step4 .back", function () {
        $(".completeDataTeacher .rowStep").attr("step", "3");
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
    })

    $(document).on("click", ".completeDataTeacher .dataTeacher .step5 .back", function () {
        $(".completeDataTeacher .rowStep").attr("step", "4");
        $('html,body').animate({
            scrollTop: 0
        }, 'slow');
    })
</script>

    <script type="text/javascript">

       var text_marcket = $("#marketing_text").val();

       console.log(text_marcket);
        var myString = "Hello, my email is blabla@blabla.com";
        console.log(myString);
        // Check if there is an email
        if(text_marcket.search(/([^.@\s]+)(\.[^.@\s]+)*@([^.@\s]+\.)+([^.@\s]+)/) !== -1){
            console.log("There is an email !");
            // Remove it...
            text_marcket = text_marcket.replace(/([^.@\s]+)(\.[^.@\s]+)*@([^.@\s]+\.)+([^.@\s]+)/,"");
            console.log(text_marcket); // Hello, my email is
            $("#marketing_text").val(text_marcket);
        }

       $(".cities2").keydown(function (e) {
           var key = e.keyCode || e.charCode;
           if (key === 8 || key === 46) {
               e.preventDefault();
               e.stopPropagation();
           }
       });

       function preventBackspace(e) {
           var evt = e || window.event;
           if (evt) {
               var keyCode = evt.charCode || evt.keyCode;
               if (keyCode === 8 || keyCode === 46) {
                   if (evt.preventDefault) {
                       evt.preventDefault();
                   } else {
                       evt.returnValue = false;
                   }
               }
           }
       }
    </script>
<script>
    {{--function prevImage() {--}}
    {{--    if ({{isset($teacher->image)}}) {--}}
    {{--            $('img.imgFile').attr('src', {{$teacher->image}});--}}
    {{--    }--}}
    {{--}--}}
</script>
@endsection

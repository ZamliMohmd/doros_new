@extends('layouts.app')

@section('header')
    <script src="{{url('/assets/website/js/jquery-1.10.2.min.js')}}"></script>
    <script src="{{url('/assets/website/js/jquery-ui.js')}}"></script>
    <link href = "{{url('/assets/website/css/jquery-ui.css')}}" rel = "stylesheet">

    <style>
       .search_left_card.fixedLeftDiv#scroll_header {
            position: fixed;
            top: 85px;
            border-radius: 0 0 15px 15px;
            right: 511px;
           width: 691px;
           z-index: 99;
           padding: 10px 10px 10px 10px;
        }

        @media (min-width: 0.1px) and (max-width: 991.9px) {
            .search_left_card.fixedLeftDiv#scroll_header {
                width: 100%;
                position: fixed;
                top: 55px;
                border-radius: 0 0 15px 15px;
                right: 0;
            }

        }
       .fixedLeftDiv2{
           top: 0 !important;


       }

       .hide {
           display: none;
       }

       .myDIV:hover + .hide {
           display: block;
           color: #ffa500;
           font-weight: bold;
           direction: rtl;
       }

       .hoverTop {
           position: absolute;
           z-index: 5;
           background-color: #F68213;
           font-size: 12px;
           top: -20px;
           right: 0;
           padding: 2px 4px;
           border-radius: 5px;
           color: #FFF;
           display: none;
       }

       .toHover {
           position: relative;
       }

       .toHover:hover .hoverTop {
           display: inline-block;
       }

       .actionBtnsSearch a:nth-of-type(1){}
       /*.search_left_card:hover {*/
       /*    color: red;*/
       /*}*/

       .actionBtnsSearch small a:nth-of-type(1){
           background: #ffffff;
           color: black;
           border-bottom: 1px solid #F68213;
       }

       #loading
       {
           text-align:center;
           background: url('/assets/website/img/loader.gif') no-repeat center;
           height: 150px;
           color: #F68213;
       }
    </style>
    <!-- CSS -->
{{--    <link rel="stylesheet" type="text/css" href="{{asset('jqueryui/jquery-ui.min.css')}}">--}}
    <!-- Script -->
{{--    <script src="{{asset('jquery-3.3.1.min.js')}}" type="text/javascript"></script>--}}
{{--    <script src="{{asset('jqueryui/jquery-ui.min.js')}}" type="text/javascript"></script>--}}

@endsection

@section('content')

    <section id="search_section" class="py-4 my-0">
        <div class="container">
            <header class="search_section_header">
                <div class="row">
                    <div class="px-3">
                        @lang('main.you_search_topic')
                        &nbsp;
                        <span class=" search_about">
                            bbbb
                        </span>
                    </div>
                    @lang('main.city')
                    &nbsp;
                    <div class=" search_about">
                      xxxxxx
                    </div>
                </div>
                <div class="row px-3 pt-2 pb-3">
                    <div class="col-12 marketing_sentence over_flow">
                        <p class="m-auto">
                          @lang('main.you_see_list')  <span class=" search_about">
                                  &nbsp;
                            xxxxx
                                  &nbsp;
                        </span>
                            @lang('main.city')
                            &nbsp;
                            <span class=" search_about">
                            bbbb
                        </span>
                            @lang('main.reminder_statement')
                        </p>
                    </div>
                </div>
            </header>
            <div class="row body_search">
                <div id="right_div_d_none" class="d-block col-xl-4 col-lg-4">
                    <div id="right_div" class="search_right_section">
                        <form action="">
                            <header class="">
                                <div class="row bbm py-2 pb-3">
                                    <div class="col-6">
                                       @lang('main.search_filters')
                                    </div>
                                    <div class="col-6 text-right empty_the_fields">
                                        <a href="{{url('/search')}}" >   @lang('main.reset_filters')</a>
                                    </div>
                                </div>
                            </header>
                            <div class="row bbm py-2 pb-4">
                                <div class="col-12 py-2">
                                    @lang('main.price_range')
                                </div>
                                <div class="col-6">
                                    <big class="price_prev">
                                        200
                                    </big>
                                    <big>
                                        &#8362;
                                    </big>
                                </div>
                                <div class="col-6 text-right">
                                    <big class="price_prev">
                                        40
                                    </big>
                                    <big>
                                        &#8362;
                                    </big>
                                </div>
                                <div id="NoUiSlider" class="col-12 text-center">
                                    <input type="hidden" name="maximum_price" id="hidden_maximum_price" value="200" />

                                    <input type="hidden" name="minimum_price" id="hidden_minimum_price" value="40" />

                                    <div id="price_range"></div>

                                {{--                                    <input id="ex2" type="text" class="span2 " value="" data-slider-min="40"--}}
{{--                                           data-slider-max="200" name="price" data-slider-step="5" data-slider-value="[200,40]" />--}}
                                </div>

                            </div>
                            <div class="row bbm pt-3">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="position-relative">
                                            <img width="28"
                                                 style="position: absolute; top: 14px; right: 10px; z-index: 9;"
                                                 src="{{url('assets/website/img/school2.svg')}}" alt="">
                                            <select name="universities" id="school" class="select2 form-control text-center school_select"
                                                    id="" style="font-size: inherit;">
                                                <option value="" >
                                                    @lang('main.univercity')
                                                </option>
                                                @foreach($universities as $key => $university)
                                                    <option value="{{$university->id}}" >
                                                        @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                            {{$university->name_ar}}
                                                        @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                            {{$university->name_he}}
                                                        @endif

                                                    </option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="position-relative">
                                            <img width="28"
                                                 style="position: absolute; top: 14px; right: 10px; z-index: 9;"
                                                 src="{{url('assets/website/img/school2.svg')}}" alt="">
                                            <select class="select2 form-control selectpicker text-center beaker_select"
                                                    style="font-size: inherit;" name="specialtion" id="part" data-live-search="true">


                                                <option value="" >
                                                    @lang('main.section')
                                                </option>

                                                @foreach($mainspecialties as $specialty)
                                                    <optgroup label="
                                            @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                    {{$specialty->name_ar}}
                                                    @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                    {{$specialty->name_he}}
                                                    @endif
" style="color: maroon">
                                                        @foreach(getsubSpecialtiesByMainSpecialtiesId($specialty->id) as
                                                        $subSpecialty)
                                                            <option value="{{$subSpecialty->id}}" style="font-weight: bold;margin-right: 20px">
                                                                @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                                    {{$subSpecialty->name_ar}}
                                                                @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                                        {{$subSpecialty->name_he}}
                                                                @endif
                                                               </option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach




                                                {{--                                @foreach($mainspecialties as $key => $mainspecialties)--}}
                                                {{--                                    <option value="{{$mainspecialty->id}}" >{{$mainspecialty->name_ar}}</option>--}}
                                                {{--                                @endforeach--}}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row bbm py-3">
                                <div>
                                    <div class="col">
                                        <div class="pl-1 d-inline-block">  @lang('main.gender')</div>
                                        <label class="label_container mr-3">
                                            <span class=" label_span"> @lang('main.male')</span>
                                            <input type="radio" name="gender" value="1">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="label_container">
                                            <span class="label_span"> @lang('main.female')</span>
                                            <input type="radio" name="gender" value="2" >
                                            <span class="checkmark"></span>
                                        </label>

                                    </div>
                                </div>


                            </div>
                            <div class="row bbm py-3">
                                <div class="col">
                                    <div class="pb-2"> @lang('main.lesson_place')</div>
                                    <div class="col-12">
                                        <div class="row">
                                            <label class="label_container col-6 my-1">
                                                <span class=" label_span mr-3">@lang('main.online')</span>
                                                <input type="radio" name="place" value="1">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="label_container col-6 my-1">
                                                <span class="label_span mr-3">@lang('main.at_student')</span>
                                                <input type="radio" name="place" value="2">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="label_container col-6 my-1">
                                                <span class="label_span mr-3">@lang('main.at_teacher')</span>
                                                <input type="radio" name="place" value="3">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="label_container col-6 my-1">
                                                <span class="label_span mr-3">@lang('main.others')</span>
                                                <input type="radio" name="place" value="4">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row bbm py-3">
                                <div class="col">
                                    <div class="pb-2">@lang('main.experince')</div>
                                    <div class="col-12">
                                        <div class="row">
                                            @foreach($experiences as $key => $experience)
                                            <label class="label_container col-6 my-2">
                                                <span class="mr-2 label_span mr-2">
                                                  @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                    {{$experience->name_ar}}
                                                        @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                        {{$experience->name_he}}
                                                      @endif
                                                </span>
                                                <input type="radio" name="experience"  value="{{$experience->id}}">
                                                <span class="checkmark my-1"></span>
                                            </label>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </form>
                    </div>
                </div>


                <div id="left_div" class="col-xl-8 col-lg-8 overflow_scroll mt-4 mt-lg-0 ">
                    <header id="scroll_header" class="search_left_card">
                        <div class="row">
                            <form  class="col-12">
                                <div class="row" style="align-items: center;">
                                    <div class="position-relative col-md-5">
                                        <div class="form-group search_subtopic" style="width: 285px">
                                            <input type="text" name="subtopic" id="subtopic" class="form-control input-lg" autocomplete="off" value="" placeholder="@lang('main.search_topic')" style="padding-right: 43px;" />
                                            <div id="subtopicList" style="position: absolute;width: 100%">
                                            </div>
                                            {{csrf_field()}}
                                        </div>
                                         <img style="position: absolute; right: 22px; top: 5px;" src="{{asset('assets/website/img/book.svg')}}"
                                             alt="">
                                    </div>

                                    <div class="position-relative col-md-5 mt-3 mt-md-0 ">
                                        <div class="form-group search_cities" style="width: 285px">
                                            <input type="text" name="cities" id="cities" class="form-control input-lg" autocomplete="off" placeholder="@lang('main.search_city')" style="padding-right: 43px;"/>
                                            <div id="citiesList" style="position: absolute;width: 100%">
                                            </div>
                                            {{csrf_field()}}
                                        </div>

                                        <img style="position: absolute; right: 22px; top: 5px;"
                                             src="{{asset('assets/website/img/globe_black.svg')}}" alt="">
                                    </div>
                                    <div class="col-xl-2 col-md-2 mt-3 mt-md-0 text-center mb-1" style="top: -8px;">
                                        <button type="submit" class="btnSubmitSearch btn btn-default mt-1" >
                                            @lang('main.search_button')
                                        </button>
                                    </div>
                                    </div>

                            </form>
                        </div>
                    </header>

                    <div class="filter_data">
                    @if(count($teachers) == 0)
                        <div class="alert alert-warning">

                            @lang('main.no_result_search')
                        </div>
                    @endif

                    @if($teachers != null)
                        @foreach($teachers as $key => $teacher)
                            <div class="search_left_card" id="{{$key}}">
                                <div class="row">
                                    <div class="col-md-2 col-sm-3" style="align-self: center;">
                                        <div class="">
                                            <div class="avatar">
                                                <div class="position-relative">
                                                    @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                        <img class="vipSearch" src="{{url('assets/website/img/vip_ar.png')}}" alt="">
                                                    @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                        <img class="vipSearch" src="{{url('assets/website/img/vip_he.png')}}" alt="">
                                                    @endif

                                                            @if($teacher->image == null)
                                                            <a href="{{url('/teachers/'.$teacher->user->token)}}">    <img src="{{url('assets/website/img/imgperson.png')}}" alt=""> </a>
                                                    @else
                                                            <a href="{{url('/teachers/'.$teacher->user->token)}}">     <img src="{{url($teacher->image)}}" alt=""> </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-9">
                                        <div class="row" style="align-items: center;">
                                            <div class="col-6">
                                                <div class="p-1" style="display: flex; ">
                                                    <img class="ml-2" style="margin-right: -4px;" src="{{asset('assets/website/img/sm person male.svg')}}"
                                                         alt="">
                                                    <a href="{{url('/teachers/'.$teacher->user->token)}}">   {{$teacher->user->name}} {{$teacher->user->last_name}} </a>
                                                </div>

                                            </div>
                                            <div class="col-6 text-right">
                                                <div class="p-1" >
                                                    <b style="font-size: 18px;" class="price">
                                                        {{$teacher->price->price_at_teacher}}
                                                    </b>
                                                    <span style="font-size: 21px;">
                                                                    &#8362;
                                                                </span>
                                                    <div style="font-size: 13px;" class="d-inline-block color48">
                                                        @lang('main.for_hour')
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-auto" style="font-size: 12px">
                                                <div class="d-inline-flex color48 toHover" >
                                                    <img class="ml-2" src="{{asset('assets/website/img/book.svg')}}" alt="">
                                                    @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                        {{getDegreeNameByTeacherId($teacher->id)->name_ar}}
                                                    @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                        {{getDegreeNameByTeacherId($teacher->id)->name_he}}
                                                    @endif
                                                    <span class="hoverTop">
                                                          @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                            {{getDegreeNameByTeacherId($teacher->id)->name_ar}}
                                                        @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                            {{getDegreeNameByTeacherId($teacher->id)->name_he}}
                                                        @endif
                                                    </span>
                                                </div>

                                            </div>
                                            <div class="col-auto" style="font-size: 11px">
                                                <div class="d-inline-flex color48 toHover" >
                                                    <img class="ml-2" src="{{url('assets/website/img/graduate.svg')}}" alt="">
                                                    @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                        {{Illuminate\Support\Str::limit(getSubspecialtiesNameByTeacherId($teacher->id)->name_ar ,20)}}
                                                    @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                        {{Illuminate\Support\Str::limit(getSubspecialtiesNameByTeacherId($teacher->id)->name_he ,20)}}
                                                    @endif
                                                    <span class="hoverTop">
                                                         @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                            {{Illuminate\Support\Str::limit(getSubspecialtiesNameByTeacherId($teacher->id)->name_ar ,20)}}
                                                        @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                            {{Illuminate\Support\Str::limit(getSubspecialtiesNameByTeacherId($teacher->id)->name_he ,20)}}
                                                        @endif
                                                    </span>
                                                </div>


                                            </div>
                                            <div class="col-auto" style="font-size: 12px">
                                                <div class="d-inline-flex color48 toHover">
                                                    <img class="ml-2" src="{{asset('assets/website/img/school2.svg')}}" alt="">
                                                    @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                        {{getUniversityNameByTeacherId($teacher->id)->name_ar}}
                                                    @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                        {{getUniversityNameByTeacherId($teacher->id)->name_he}}
                                                   @endif
                                                    <span class="hoverTop">
                                                       @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                                            {{getUniversityNameByTeacherId($teacher->id)->name_ar}}
                                                        @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                                            {{getUniversityNameByTeacherId($teacher->id)->name_he}}
                                                        @endif
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row  mt-2">
                                            <div class="col-12 coloe8f over_flow" >
                                                <p style="font-size: 13px; font-weight: 100;    padding-right: 11px;" class="mb-0">
                                                    {{$teacher->marketing_text}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 actionBtnsSearch">
                                        <div class="row" style="flex-wrap: nowrap;padding-right: 7px;">
                                            <div class="stars" data-star="{{getTeacherRate($teacher->id)}}">
{{--                                            <div class="stars" style="font-size: 14px">--}}
                                                @if(getTeacherRate($teacher->id) == 0)
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                @elseif(getTeacherRate($teacher->id) == 1)
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                @elseif(getTeacherRate($teacher->id) == 2)
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                @elseif(getTeacherRate($teacher->id) == 3)
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                @elseif(getTeacherRate($teacher->id) == 4)
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star"></i>
                                                @elseif(getTeacherRate($teacher->id) == 5)
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star active"></i>
                                                    <i class="fas fa-star active"></i>
                                                @endif
                                            </div>
                                            <div class="d-flex">
                                                <div class="px-1" style="font-size: 13px">
                                                                <span style="font-size: 16px;" class="price px-1">
                                                                    {{getTeacherRateCount($teacher->id)}}
                                                                </span>
                                                </div>
                                                <div>
                                                    <small style="font-size: 13px;">
                                                        <a href="{{url('/teachers/'.$teacher->user->token)}}"> @lang('main.rate') </a>

                                                        <span></span>
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <a href="{{url('/calender/'.$teacher->user->token)}}" type="submit" style="display: flex;" class="btn btn-default btn-block  ml-2 mt-2 text-left px-2">
                                                            <span class="icon_btn_svg">

                                           <img src="{{asset('assets/website/img/time.svg')}}" alt="">
                                                            </span>
                                                @lang('main.choose_lesson')
                                            </a>

                                            <a id="show_phone_Number"
                                               class="btn btn-default btn-block ml-2 text-left px-2 show_phone_Number">
                                        <span class="icon_btn_svg">
                                            <img src="{{asset('assets/website/img/w phone2.svg')}}" alt="">
                                            <img src="{{asset('assets/website/img/w phone.svg')}}" alt="">
                                        </span>
                                                <span class="phone_span_contant">  @lang('main.show_mobile')</span>
                                                <span class="phone_Number">{{$teacher->user->mobile}}</span>
                                                <span class="loader">
                                            <img src="{{asset('assets/website/img/loader.png')}}" alt="">
                                        </span>
                                            </a>

                                            <button style="background-color: #1A73E8;
    color: #fff;
    border: 1px solid #1A73E8;
    font-size: 13px;
    padding: 5px;" type="button" class="btn btn-primary btn btn-default btn-block btn-blue ml-2 text-left px-2 d-flex" data-toggle="modal" data-target="#exampleModal{{$key}}" data-whatever="@mdo"><span class="icon_btn_svg">
                                                                <img src="{{asset('assets/website/img/message.svg')}}" alt="">
                                                            </span>
                                                @lang('main.send_msg')</button>

                                        </div>
                                    </div>
                                </div>
                            </div>


                                <div class="popUp popUpSearch popUpSearch1">
                                    <div>
                                        <div class="headerPopUp">
                <span>
                    <img src="{{url('assets/website/img/user.png')}}" width="30" alt="">
                    <small>
                       {{$teacher->user->name}}  {{$teacher->user->last_name}}
                    </small>
                </span>
                                            <span class="closeX">
                    <i class="fal fa-times"></i>
                </span>
                                        </div>
                                        <div class="bodyPopUp">
                                            @if(session()->has('success'))
                                                <div id="alert" class="alert alert-success">
                                                    {{session('success')}}
                                                </div>
                                            @endif
                                            <form  method="post" action="{{url('/new_message')}}" class="formAddSubject" numCorrect="0" action="">
                                                {{csrf_field()}}
                                                <input type="hidden" name="user_id" value="{{$teacher->user->id}}">
                                                <textarea style="font-size: 13px; padding: 5px;" name="message" id="" rows="4" placeholder="متى تريد أن تحدد درس وأي سؤال آخر" v-model='messages' @keyup.enter='sendMessage'></textarea>
                                                @error('message')
                                                <span class="text-danger">{{ $message}}</span>
                                                @enderror

                                                <div class="ok">
                                                    <button class="" type="submit">
                                                        @lang('main.send')
                                                    </button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>

                            <div class="modal headerPopUp  fade" style="border-radius: 15px" id="exampleModal{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog headerPopUp" role="document" style="">
                                    <div class="modal-content">
                                        <div class="modal-header ">
                                          <span>
                                            <img src="{{url('assets/website/img/user.png')}}" width="30" alt="">
                                            <small>
                                               {{$teacher->user->name}}  {{$teacher->user->last_name}}
                                            </small>
                                                  </span>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="{{url('/messages')}}" method="post">
                                            {{csrf_field()}}
                                            <input type="hidden" name="receiver_id" value="{{$teacher->user->id}}">
                                        <div class="modal-body">
                                                <div class="form-group">
                                                    <textarea style="font-size: 13px; padding: 5px;" class="form-control" name="message" id="message-text" rows="4" placeholder="@lang('main.msg_placeholder')" v-model='messages'></textarea>
                                                </div>

                                        </div>

                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">    @lang('main.send')</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    @endif

                    </div>
                </div>
            </div>



        </div>
    </section>




@endsection

@section('footer')

    <script>
        $('.select2').select2({
          //  placeholder: "اختر...",
            dir: 'rtl',
            maximumSelectionLength: '2'
        });
    </script>


    <script>// search

        if ($("#ex2").length != 0) {
            $("#ex2").slider({});
        }

        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 50) {
                $("#scroll_header").addClass("fixedLeftDiv fixedLeftDiv2");
            } else {
                $("#scroll_header").removeClass("fixedLeftDiv fixedLeftDiv2");
            }
        });

        $("#left_div").scroll(function () {
            var scroll = $("#left_div").scrollTop();
            if (scroll >= 50) {
                $("#scroll_header").addClass("fixedLeftDiv");
            } else {
                $("#scroll_header").removeClass("fixedLeftDiv");
            }
        });

        // $('.phone_span_contant').click(function () {
        //     $($(this).siblings(".phone_Number")).fadeToggle(10)
        //     $(this).fadeToggle(10)
        // });
        //
        // $('.phone_Number').click(function () {
        //     $($(this).siblings(".phone_span_contant")).fadeToggle(10)
        //     $(this).fadeToggle(10)
        // });


    </script>


    <script>
        $(document).on("change", ".bbm1 #education", function () {
            $(".bbm1 .listEducation").empty();
            for (var i = 0; i < 10; i++) {
                if ($($(this).find("option")[i]).is(':selected')) {
                    $(".bbm1 .listEducation").append('<span value="' + $($(this).find("option")[i]).val() + '" class="nameEducation">' + $($(this).find("option")[i]).text() + '<i class="fal fa-times-circle"></i>' + '</span>');
                }
            }
        })

        $(document).on("click", ".nameEducation i", function () {
            $("#education").find("option[value='" + $($(this).parent()).attr("value") + "']").prop("selected", false)
            $($(this).parents(".nameEducation")).remove();
        })

        $(document).on("change", ".bbm2 #cities", function () {
            $(".bbm2 .listCities").empty();
            for (var i = 0; i < 10; i++) {
                if ($($(this).find("option")[i]).is(':selected')) {
                    $(".bbm2 .listCities").append('<span value="' + $($(this).find("option")[i]).val() + '" class="nameCities">' + $($(this).find("option")[i]).text() + '<i class="fal fa-times-circle"></i>' + '</span>');
                }
            }
        })

        $(document).on("click", ".nameCities i", function () {
            $("#cities").find("option[value='" + $($(this).parent()).attr("value") + "']").prop("selected", false)
            $($(this).parents(".nameCities")).remove();
        })


        // $(document).on("click", ".sendMsg_button", function () {
        //     $(".popUpSearch1").addClass("show")
        // })
        //
        // $(document).on("click", ".popUpSearch1 .closeX, .popUpSearch1 .ok .submit", function () {
        //     $(".popUpSearch1").removeClass("show")
        // })


    </script>

    <script>
        $(document).on("click", ".sendMsg_button{{--$key--}}", function () {
            $(".popUpSearch1").addClass("show")
        })

        $(document).on("click", ".popUpSearch1 .closeX, .popUpSearch1.ok .submit", function () {
            $(".popUpSearch1").removeClass("show")
        })
    </script>


{{--    <script type="text/javascript">--}}
{{--        var route = "{{ url('autocomplete') }}";--}}
{{--        $('#search').typeahead({--}}
{{--            source:  function (term, process) {--}}
{{--                return $.get(route, { term: term }, function (data) {--}}
{{--                    return process(data);--}}
{{--                });--}}
{{--            }--}}
{{--        });--}}
{{--    </script>--}}


    <!-- Script -->
{{--    <script type="text/javascript">--}}

{{--        // CSRF Token--}}
{{--        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');--}}
{{--        $(document).ready(function(){--}}

{{--            $( "#employee_search" ).autocomplete({--}}
{{--                source: function( request, response ) {--}}
{{--                    // Fetch data--}}
{{--                    $.ajax({--}}
{{--                        url:"{{route('autocomplete')}}",--}}
{{--                        type: 'post',--}}
{{--                        dataType: "json",--}}
{{--                        data: {--}}
{{--                            _token: CSRF_TOKEN,--}}
{{--                            search: request.term--}}
{{--                        },--}}
{{--                        success: function( data ) {--}}
{{--                            response( data );--}}
{{--                        }--}}
{{--                    });--}}
{{--                },--}}
{{--                select: function (event, ui) {--}}
{{--                    // Set selection--}}
{{--                    $('#employee_search').val(ui.item.label); // display the selected text--}}
{{--                    $('#employeeid').val(ui.item.value); // save selected id to input--}}
{{--                    return false;--}}
{{--                }--}}
{{--            });--}}

{{--        });--}}
{{--    </script>--}}

    <script>
        $(document).ready(function(){

            $('#subtopic').keyup(function(){
                var query = $(this).val();
                if(query != '')
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url:"{{ route('autocomplete.fetchSubtopic') }}",
                        method:"POST",
                        data:{query:query},
                        success:function(data){
                            $('#subtopicList').fadeIn();
                            $('#subtopicList').html(data);
                        }
                    });
                }
            });

            $(document).on('click', 'li', function(){
                $($($(this).parents(".search_subtopic")).find("input")).val($(this).text());
                $('#subtopicList').fadeOut();
                // console.log($(this))

            });

        });

    </script>
    <script>
        $(document).ready(function(){
            $('#cities').keyup(function(){
                var query = $(this).val();
                if(query != '')
                {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url:"{{ route('autocomplete.fetchCities') }}",
                        method:"POST",
                        data:{query:query},
                        success:function(data){
                            $('#citiesList').fadeIn();
                            $('#citiesList').html(data);
                        }
                    });
                }
            });

            $(document).on('click', 'li', function(){
                $($($(this).parents(".search_cities")).find("input")).val($(this).text());
                $('#citiesList').fadeOut();
            });

        });
    </script>

    <script>
        $(document).ready(function(){

            filter_data();

            function filter_data()
            {
                // $('.filter_data').html('<div id="loading" style="" ></div>');
                var action = 'fetch_data';
                var minimum_price = $('#hidden_minimum_price').val();
                var maximum_price = $('#hidden_maximum_price').val();
                var gender = get_filter('gender');
                var ram = get_filter('ram');
                var storage = get_filter('storage');
                $.ajax({
                    url:"fetch_data.php",
                    method:"POST",
                    data:{action:action, minimum_price:minimum_price, maximum_price:maximum_price, gender:gender, ram:ram, storage:storage},
                    success:function(data){
                        $('.filter_data').html(data);
                    }
                });
            }

            function get_filter(class_name)
            {
                var filter = [];
                $('.'+class_name+':checked').each(function(){
                    filter.push($(this).val());
                });
                return filter;
            }

            $('.common_selector').click(function(){
                filter_data();
            });

            $('#price_range').slider({
                range:true,
                min:40,
                max:200,
                values:[40, 200],
                step:5,
                stop:function(event, ui)
                {
                    $('#price_show').html(ui.values[0] + ' - ' + ui.values[1]);
                    $('#hidden_minimum_price').val(ui.values[0]);
                    $('#hidden_maximum_price').val(ui.values[1]);
                    filter_data();
                }
            });

        });
    </script>

@endsection

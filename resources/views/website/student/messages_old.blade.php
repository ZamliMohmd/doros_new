
@extends('layouts.app')

@section('header')
    <link rel="stylesheet" href="{{asset('assets/css/messages.css')}}">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('website.student.sidebar')
            <div class="col colContentChat content">

                <div class="row">
                    <div class="col-lg-12 colBoxChat">
                        <div class="box boxChat">
                            <div class="title">
                                <span class="btnBack">
                                    <i class="fal fa-arrow-right"></i>
                                </span>
                                <span class="icon">
                                    <i class="fal fa-comment-lines"></i>
                                </span>
                               @lang('student.all_teachers_msg')
                            </div>
                            <div class="row">

                                <div class="colList activeMob">
                                    <ul class="listUsers">
                                        @if(count($messages) == 0)
                                            <li class="" data-chat="chatPerson">
                                                <div class="alert alert-danger">
                                                 @lang('student.no_message')
                                                </div>
                                            </li>
                                        @endif
                                        @foreach($messages as $key => $message)
                                        <li class="" data-chat="chatPerson{{$key}}">
                                            <a href="javascript:void(0)">
                                                <span class="imgStu">
                                                     <img src="{{asset('assets/website/img/imgStudent.png')}}" alt="">
                                                     @if(unSeenRepliesMessagesCount($message->id) > 0)
                                                        <small>{{unSeenRepliesMessagesCount($message->id)}}</small>
                                                    @endif
                                                </span>
                                                <span>
                                                     <p class="name">
                                                      @if($message->senderUser->id == \Illuminate\Support\Facades\Auth::id())
                                                        {{$message->receiverUser->name}} {{$message->receiverUser->last_name}}
                                                          @else
                                                             {{$message->senderUser->name}} {{$message->senderUser->last_name}}
                                                      @endif

                                                    </p>
                                                    <p class="date">
                                                        {{$message->updated_at->format('d-m-Y')}}
                                                    </p>
                                                </span>
                                                <span>
                                                    <span class="icon">
                                                        <i class="fal fa-calendar-star"></i>
                                                        <div class="overUserChat">


                                                        </div>
                                                    </span>
                                                    <span class="icon">
                                                        <i class="fal fa-comment-alt-lines"></i>
                                                        <div class="overUserChat"><p>
                                                                <i class="fal fa-phone-alt"></i>
                                                                {{$message->receiverUser->mobile}}
                                                            </p>
                                                            <p>
                                                                <i class="fal fa-envelope"></i>
                                                               {{$message->receiverUser->email}}
                                                            </p>
                                                        </div>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>

                                <div class="colTextChat">
                                    @foreach($messages as $key => $message)
                                    <div class="chatPerson chatPerson{{$key}}">

                                        <div class="msgs">
                                            <div class="timeMsg">
                                                <span>
                                                    <i class="fal fa-calendar"></i>
                                                    <span>{{$message->created_at->format('d-m-Y')}}</span>
                                                </span>
                                            </div>

                                            @if($message->user_id == \Illuminate\Support\Facades\Auth::id())
                                                <div class="msgLeft msgRight">
                                                    <div>
                                                        <img src="{{asset('assets/website/img/imgStudent.png')}}" alt="">
                                                    </div>
                                                    <div>
                                                        <p class="name">
                                                            <b>

                                                            </b>
                                                        <p class="text">
                                                            {{$message->message}}
                                                        </p>
                                                        <p class="time">
                                                            <i class="fal fa-clock"></i>
                                                            <b>
                                                                {{$message->created_at->format('H:i')}}
                                                            </b>

                                                        </p>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="msgLeft">
                                                    <div>
                                                        <img src="{{asset('assets/website/img/imgStudent.png')}}" alt="">
                                                    </div>
                                                    <div>
                                                        <p class="name">
                                                            <b>

                                                            </b>
                                                        </p>
                                                        <p class="text">
                                                            {{$message->message}}
                                                        </p>
                                                        <p class="time">
                                                            <i class="fal fa-clock"></i>
                                                            <b>
                                                                {{$message->created_at->format('H:i')}}
                                                            </b>

                                                        </p>
                                                    </div>
                                                </div>
                                            @endif

                                            @foreach(getAllRepliesForOneMessageByMessageId($message->id) as $key => $reply)
                                                @if($reply->sender_id == \Illuminate\Support\Facades\Auth::id())
                                                    <div class="msgLeft msgRight">
                                                        <div>
                                                            <img src="{{asset('assets/website/img/imgStudent.png')}}" alt="">
                                                        </div>
                                                        <div>
                                                            <p class="name">
                                                                <b>

                                                                </b>
                                                            <p class="text">
                                                                {{$reply->reply}}
                                                            </p>
                                                            <p class="time">
                                                                <i class="fal fa-clock"></i>
                                                                <b>
                                                                    {{$reply->created_at->format('H:i')}}
                                                                </b>

                                                            </p>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="msgLeft">
                                                        <div>
                                                            <img src="{{asset('assets/website/img/imgStudent.png')}}" alt="">
                                                        </div>
                                                        <div>
                                                            <p class="name">
                                                                <b>

                                                                </b>
                                                            </p>
                                                            <p class="text">
                                                                {{$reply->reply}}
                                                            </p>
                                                            <p class="time">
                                                                <i class="fal fa-clock"></i>
                                                                <b>
                                                                    {{$reply->created_at->format('H:i')}}
                                                                </b>

                                                            </p>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach


                                        </div>

                                        <form class="write" action="{{url('/message/reply')}}" method="post" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <input type="hidden" name="message_id" value="{{$message->id}}">
                                            <div>
                                                <input type="text" class="textMsg" name="reply"
                                                       placeholder="@lang('student.write_reply')">
                                                <label class="fileMsg" for="fileMsg1">
                                                    <i class="fal fa-paperclip"></i>
                                                </label>
                                                <input hidden type="file" name="m" id="fileMsg1">
                                                <button type="submit">
                                                   @lang('student.send')
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection


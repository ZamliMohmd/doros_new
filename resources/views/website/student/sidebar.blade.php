
<div class="col-lg-2 sideRightStudent">
    <ul>
        <li style="font-weight: bold">
            <a href="javascript:void(0)" class="keyList">
                            <span class="icon">
                                <i class="droosvip-2-small-caps-1"></i>
                            </span>
                @lang('student.my_account_as_stu')
                <div class="iconDown">
{{--                    <i class="fas fa-chevron-down"></i>--}}
                </div>
            </a>
            <ul class="listSideRight">
                <li>
                    <a href="" class="">
                                    <span class="icon">
                                        <i class="fad fa-user-tie"></i>
                                    </span>
                        @lang('student.my_account')
                    </a>
                </li>
                <li>
                    <a href="{{route('student_messages')}}" class="@if (url()->current() == route('student_messages'))active @endif">
                                    <span class="icon">
                                        <i class="fal fa-comment-lines"></i>
                                    </span>
                        @lang('student.messages')
                    </a>
                </li>
                <li>
                    <a href="" class="">
                                    <span class="icon">
                                        <i class="fal fa-calendar"></i>
                                    </span>
                        @lang('student.calender')
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>

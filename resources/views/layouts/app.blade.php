<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Doros Vip') }}</title>



        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        @livewireStyles

    <link rel="stylesheet" href="{{asset('assets/website/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/website/css/bootstrap-rtl.css')}}">
    <link rel="stylesheet" href="{{url('assets/website/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{url('assets/website/css/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/website/css/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/website/css/slick-theme.css')}}" >
    <link rel="stylesheet" href="{{url('assets/website/css/bootstrap-slider.css')}}">

    <link rel="stylesheet" href="{{url('assets/website/css/main.css')}}">
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    <style>
        .alert-message{
            color: red;
        }

        .tableHour input:disabled {
            background-color: rgba(239, 239, 239, 0.3);
            opacity: 0.5;
        }

    </style>
    @yield('header')
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>

</head>
<body>

    <header
        @if(\Illuminate\Support\Facades\Auth::guest())
        class="headerVisitor"
        @elseif(\Illuminate\Support\Facades\Auth::user()->usertypeID() == 3)
        class="headerTeacher"
        @elseif(\Illuminate\Support\Facades\Auth::user()->usertypeID() == 4)
        class="headerStudent"
        @endif
    >
        <div class="container">
            <div class="row">
                <div class="first">
                    <div class="logo">
                        <a href="{{url('/')}}">
                            <img src="{{asset('assets/website/img/logo.png')}}" alt="">
                        </a>
                    </div>
                    <div class="linkSearch">
                        <a href="{{url('/search')}}">
                            @lang('main.search_page')
                        </a>
                    </div>
                </div>

                @if(\Illuminate\Support\Facades\Auth::user())
                    <div class="last">
                        @if(\Illuminate\Support\Facades\Auth::user()->usertypeID() == 4)
                            <div class="user">
                                <div class="img">
                                    <img src="{{asset('assets/website/img/user.png')}}" alt="">
                                </div>
                                <div class="name">
                                    @lang('student.my_account')
                                </div>
                                <div class="iconDown">
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                                <div class="menuUser">
                                    <ul>
                                        <li>
                                            <a href="" class="">
                                        <span class="icon">
                                            <i class="fad fa-user-tie"></i>
                                        </span>
                                                @lang('student.my_account')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('student_messages')}}">
                                        <span class="icon">
                                            <i class="fal fa-comment-lines"></i>
                                        </span>
                                                @lang('student.messages')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="">
                                        <span class="icon">
                                            <i class="fal fa-calendar"></i>
                                        </span>
                                                @lang('student.calender')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('logout')}}"
                                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                            ><i class="fa fa-sign-in fa-lg" ></i>  @lang('student.logout')</a>
                                        </li>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}

                                        </form>
                                    </ul>
                                    <div class="over"></div>
                                </div>
                            </div>
                        @elseif(\Illuminate\Support\Facades\Auth::user()->usertypeID() == 3)
                            <div class="user">
                                <div class="img">

                                    <img style="width: 44px;height: 44px;" src="
                                {{asset('assets/website/img/user.png')}}
                                    {{--                            @if(\Illuminate\Support\Facades\Auth::user()->teacher->image != null)--}}
                                    {{--                                {{url(\Illuminate\Support\Facades\Auth::user()->teacher->image)}}--}}
                                    {{--                            @else--}}
                                    {{--                                --}}
                                    {{--                            @endif--}}

                                        " alt="">

                                </div>
                                <div class="name">
                                    @lang('teacher.my_account')
                                </div>
                                <div class="iconDown">
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                                <div class="menuUser">
                                    <ul>
                                        <li>
                                            <a href="" class="">
                                        <span class="icon">
                                            <i class="fad fa-user-tie"></i>
                                        </span>
                                                @lang('teacher.my_account')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('teacher_messages')}}">
                                        <span class="icon">
                                            <i class="fal fa-comment-lines"></i>
                                        </span>
                                                @lang('teacher.messages')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="">
                                        <span class="icon">
                                            <i class="fal fa-calendar"></i>
                                        </span>
                                                @lang('teacher.calender')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="">
                                        <span class="icon">
                                            <i class="fal fa-sticky-note"></i>
                                        </span>
                                                @lang('teacher.my_blogs')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="">
                                        <span class="icon">
                                            <i class="fad fa-wallet"></i>
                                        </span>
                                                @lang('teacher.my_payments')
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('logout')}}"
                                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                            ><i class="fa fa-sign-in fa-lg" ></i>   @lang('teacher.logout')</a>
                                        </li>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}

                                        </form>
                                    </ul>
                                    <div class="over"></div>
                                </div>
                            </div>
                        @endif

                            <div class="notification">
                                <div class="icon">
                                    <i class="fal fa-bell-on"></i>
                                    <div class="num">
                                        3
                                    </div>
                                </div>
                                <div class="text">
                                    الإشعارات
                                </div>
                                <div class="iconDown">
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                                <div class="menuNotification">
                                    <div>
                                        <div class="note unRead">
                                    <span class="from">
                                        <span class="icon">
                                            <i class="fad fa-user-tie"></i>
                                        </span>
                                        <small>
                                            حسابي
                                        </small>
                                    </span>
                                            <span>
                                        <b>
                                            تنويه
                                        </b>
                                        بياناتك الشخصية مكتملة بنسبة
                                        <b>
                                            70%
                                        </b>
                                        .. قم
                                        <a href="">
                                            بإكمال بياناتك الأن
                                        </a>
                                    </span>
                                        </div>
                                        <div class="note">
                                    <span class="from">
                                        <span class="icon">
                                            <i class="fal fa-calendar"></i>
                                        </span>
                                        <small>
                                            التقويم
                                        </small>
                                    </span>
                                            <img src="img/user.png" alt="">
                                            <span>
                                        عبد الرحمن الطهرواي قام
                                        <b>
                                            بطلب درس
                                        </b>
                                        يوم الأربعاء بتاريخ 17/11/2020 الساعة 14:30
                                    </span>
                                        </div>
                                        <div class="note unRead">
                                    <span class="from">
                                        <span class="icon">
                                            <i class="fad fa-user-tie"></i>
                                        </span>
                                        <small>
                                            حسابي
                                        </small>
                                    </span>
                                            <span>
                                        <b>
                                            تنويه
                                        </b>
                                        بياناتك الشخصية مكتملة بنسبة
                                        <b>
                                            70%
                                        </b>
                                        .. قم
                                        <a href="">
                                            بإكمال بياناتك الأن
                                        </a>
                                    </span>
                                        </div>
                                        <div class="note">
                                    <span class="from">
                                        <span class="icon">
                                            <i class="fal fa-calendar"></i>
                                        </span>
                                        <small>
                                            التقويم
                                        </small>
                                    </span>
                                            <img src="img/user.png" alt="">
                                            <span>
                                        عبد الرحمن الطهرواي قام
                                        <b>
                                            بطلب درس
                                        </b>
                                        يوم الأربعاء بتاريخ 17/11/2020 الساعة 14:30
                                    </span>
                                        </div>
                                        <div class="note unRead">
                                    <span class="from">
                                        <span class="icon">
                                            <i class="fad fa-user-tie"></i>
                                        </span>
                                        <small>
                                            حسابي
                                        </small>
                                    </span>
                                            <span>
                                        <b>
                                            تنويه
                                        </b>
                                        بياناتك الشخصية مكتملة بنسبة
                                        <b>
                                            70%
                                        </b>
                                        .. قم
                                        <a href="">
                                            بإكمال بياناتك الأن
                                        </a>
                                    </span>
                                        </div>
                                        <div class="note">
                                    <span class="from">
                                        <span class="icon">
                                            <i class="fal fa-calendar"></i>
                                        </span>
                                        <small>
                                            التقويم
                                        </small>
                                    </span>
                                            <img src="img/user.png" alt="">
                                            <span>
                                        عبد الرحمن الطهرواي قام
                                        <b>
                                            بطلب درس
                                        </b>
                                        يوم الأربعاء بتاريخ 17/11/2020 الساعة 14:30
                                    </span>
                                        </div>
                                    </div>
                                    <div class="over"></div>
                                </div>
                            </div>



                        @if(\Illuminate\Support\Facades\Auth::user()->usertypeID() == 4)
                            <div class="addRateTeacher">
                                <a href="javascript:void(0)">
                            <span class="icon">
                                <i class="fas fa-star"></i>
                            </span>
                                    <span>
                                @lang('student.rate_teacher')
                            </span>
                                </a>
                            </div>
                        @endif


                        <div class="menuMobile">
                        <span class="icon">
                            <i class="fal fa-bars"></i>
                        </span>
                        </div>
                    </div>

                @endif
            </div>
                @if(\Illuminate\Support\Facades\Auth::guest())
                    <div class="last">
                        <div class="addToUs">
                            <div>
                                <div class="icon">
                                    <i class="fal fa-user-graduate"></i>
                                </div>
                                <div class="name">
                                    @lang('main.join')
                                </div>
                                <div class="iconDown">
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                            </div>
                            <div class="menuAddToUs">
                                <ul>
                                    <li>
                                        <a href="{{route('teacher_register_page')}}" class="joinAsTeacher active">
                                            @lang('main.join_teacher')
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="joinAsStudent">
                                            @lang('main.join_student')
                                        </a>
                                    </li>
                                </ul>
                                <div class="over"></div>
                            </div>
                        </div>
                        <div class="linkLogin">
                            <a href="#">
                            <span>
                                @lang('main.login')
                            </span>
                            </a>
                        </div>
                        @endif




                    <div class="lang" style="margin-left: -57px;margin-right: 22px;">
                        <a href="#">
                            <span>
                                   @php
                                       use Illuminate\Support\Facades\App;
                                 \Illuminate\Support\Facades\Session::put('locale','ar');
                                App::currentLocale()
                                   @endphp

                                @if(\Illuminate\Support\Facades\App::isLocale('ar'))
                                    {{ \Illuminate\Support\Facades\Session::put('locale','ar')}}
                                @elseif(\Illuminate\Support\Facades\App::isLocale('he'))
                                    {{ \Illuminate\Support\Facades\Session::put('locale','he')}}
                                    <a href="{{url('/lang/ar')}}"><span style="color: #F68213;font-size: 18px;">عربي</span></a>
                                @endif
                            </span>
                        </a>
                    </div>
                    </div>

        </div>
    </header>

    @yield('teacherAlert')

    @include('show_meassage')

    @yield('content')

    <footer>
        <section class="container">
            <div class="row">

                <div class="footer_right_section mb-5 col-md-5 col-sm-12">
                    <div class="">
                        <div class="mb-5">
                            <h3>
                                <span>@lang('main.first_platform')</span>
                                @lang('main.footer_market_statement')
                            </h3>
                        </div>
                        <a href="javascript:void(0)" class="btnGet btnJoin">
                            @lang('main.join')
                        </a>
                    </div>
                </div>
                <div class="footer_left_section col-md-7 col-sm-12">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-6">
                            <nav class="nav flex-column">
                                <h5>@lang('main.sections')</h5>
                                <a class="nav-link" href="{{url('/')}}">@lang('main.main')</a>
                                <a class="nav-link" href="{{url('/search')}}">@lang('main.teachers')</a>
                            </nav>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <nav class="nav flex-column">
                                <h5>@lang('main.quick_links')</h5>
                                <a class="nav-link" href="{{route('teacher_register_page')}}"> @lang('main.join_teacher')</a>

                                <a class="nav-link joinAsStudent studentReg" href="javascript:void(0)"> @lang('main.join_student')</a>
                                <a class="nav-link linkLogin" href="javascript:void(0)">@lang('main.login')</a>
                                <a class="nav-link" href="">@lang('main.use_terms')</a>
                                <a class="nav-link" href="">@lang('main.privacy')</a>
                            </nav>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="row">
                                <div class="col-6 col-sm-12 col-lg-12 col-xl-12">
                                    <nav class="nav flex-column">
                                        <h5>  @lang('main.contactus')</h5>
                                        <a class="nav-link" href="#">info@dorosvip.com</a>
                                    </nav>
                                </div>
                                <div class="col-6 col-sm-12 col-lg-12 col-xl-12">
                                    <div class="follow_us">
                                        <h5 class="d-inline-block">follow us</h5>
                                        <div class="social_media_bottom d-inline-block">

                                            <a target="_blank" href="https://www.instagram.com/dorosvip.co.il" class="right">
                                                <img src="{{asset('assets/website/img/Instagram.svg')}}" alt="Instagram">
                                            </a>

                                            <a target="_blank" href="https://www.facebook.com/dorosvip.co.il">
                                                <img src="{{asset('assets/website/img/facebook.svg')}}" alt="facebook" style="padding-right: 5px;">
                                            </a>
                                            <div>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row copy_right">
                <h6>&copy; 2020,droos vip.</h6>
                <a href="javascript:void(0)" class="btn-default go_for_higher tran3s">
                    <i class="fas fa-angle-double-up"></i>
                </a>
            </div>

        </section>
    </footer>


    <div class="popUp popJoinX">
        <div>
            <div class="headerPopUp" style="background: #F68213">
                <i class="fal fa-user-circle"></i>
                @lang('main.join')
                <span class="closeX" >
                    <i class="fal fa-times" style="color: black"></i>
                </span>
            </div>
            <div class="bodyPopUp">
                <a href="{{route('teacher_register_page')}}">
                    @lang('main.join_teacher')
                </a>
                <a class="joinAsStudent" href="javascript:void(0)">
                    @lang('main.join_student')
                </a>
            </div>
        </div>
    </div>
    <div class="popUp loginHome formPublic login">
        <div style="width: 400px">
            <div class="headerPopUp">
                <i class="fal fa-user-circle"></i>
                @lang('main.login')
                <span class="closeX">
                    <i class="fal fa-times"></i>
                </span>
            </div>
            <div class="bodyPopUp">
                <form method="post" action="{{url('/loginuser')}}" class="formToJoin loginX">
                    <div class="waysToJoin">
                        <div>
                            <a class="join1" href="">
                                @lang('main.login_by')
                                <span class="img">
                                <img src="/assets/website/img/google.png" alt="">
                            </span>
                            </a>
                        </div>
                        <div>
                            <a class="join2" href="">
                                @lang('main.login_by')

                                <span class="img">
                                <img src="/assets/website/img/facebook.png" alt="">
                            </span>
                            </a>
                        </div>
                    </div>
                    {{csrf_field()}}
                    <div>
                        <label for="email">
                            @lang('main.email')
                        </label>
                        <div class="divInput">
                            <input class="input" requiredX="yes" min="6" max="30" errorMin="@lang('main.email_error')"
                                   errorMax="@lang('main.email_error')" errorEmpty="@lang('main.email_error')"
                                   errorType="@lang('main.email_error')" autocomplete="off" id="email" name="email" type="email"
                                   value="" placeholder="@lang('main.email_placeholder')">
                            <i class="fal fa-envelope"></i>
                        </div>
                    </div>
                    <div>
                        <label for="password">
                            @lang('student.password')
                        </label>
                        <div class="divInput">
                            <input class="input" requiredX="yes" min="8" max="39" errorMin="@lang('main.password_error')"
                                   errorMax="@lang('main.password_error')" errorEmpty="@lang('main.password_error')"
                                   autocomplete="off" id="password" name="password" type="password" value="" placeholder="@lang('main.password_placeholder')">
                            <i class="fal fa-lock-alt"></i>
                        </div>
                    </div>
                    <div class="agree">
                        <input requiredX="no" id="agree" hidden type="checkbox">
                        <label for="agree">
                            @lang('main.remember_me')
                        </label>
                    </div>
                    <div class="ok">
                        <button class="submit" type="button">
                            @lang('main.login')
                        </button>
                    </div>
                    <div class="login">
                        <p>
                            @lang('main.donot_have_account')
                            <a href="javascript:void(0)" class="btnGet btnJoinNew btnJoin">
                                @lang('main.join')
                            </a>
                        </p>
                        <p class="forgetPass">
                            @lang('main.forget_password')
                            <a href="javascript:void(0)">
                                @lang('main.reset_now')
                            </a>
                        </p>
                    </div>
                </form>
                <form action="{{url('reset-password')}}" method="post" class="formToJoin remmberPassord">
                    {{csrf_field()}}
                    <div>
                        <label for="email">
                            @lang('main.email')
                        </label>
                        <div class="divInput">
                            <input requiredX="yes" min="3" max="12" errorMin="يجب على الأقل أن يكون 3 حروف"
                                   errorMax="يجب ألا يزيد عن 12 حرف" errorEmpty="يجب ألا يترك الحقل فارغ"
                                   errorType="يجب إدخال بريد إلكتروني صحيح" autocomplete="off" id="email" type="email"
                                   value="" required>
                            <i class="fal fa-envelope"></i>
                        </div>
                    </div>
                    <div class="ok">
                        <button class="submit" type="button">
                            @lang('main.reset_now')
                        </button>
                    </div>
                    <div class="login" style="cursor: pointer">
                        <p class="btnLog">
                            @lang('main.login')
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="popUp popUpFromVisitor1 formPublic popUpJoinAsTeacher">
        <div>
            <div class="headerPopUp">
                <i class="fal fa-user-circle"></i>
                @lang('student.join_student')
                <span class="closeX">
                    <i class="fal fa-times"></i>
                </span>
            </div>
            <div class="bodyPopUp">
                <div class="waysToJoin">
                    <div>
                        {{ \Illuminate\Support\Facades\Session::put('usertype','student')}}
                        <a class="join1" href="{{url('auth/google')}}">
                            @lang('student.join_as')
                            <span class="img">
                                <img src="{{asset('assets/website/img/google.png')}}" alt="">
                            </span>
                        </a>
                    </div>
                    <div>
                        <a class="join2" href="{{url('auth/facebook')}}">
                            @lang('student.join_as')
                            <span class="img">
                                <img src="{{asset('assets/website/img/facebook.png')}}" alt="">
                            </span>
                        </a>
                    </div>
                </div>
                <form action="{{route('student_register')}}" class="formToJoin " method="post" numCorrect="0" action="#">
                    @if(session()->has('success'))
                        <div id="alert" class="alert alert-success">
                            {{session('success')}}
                        </div>
                    @endif
                    {{csrf_field()}}
                    <div>
                        <label for="name">
                            @lang('student.full_name')
                        </label>
                        <div class="divInput">
                            <input requiredX="yes" errorMin="يجب على الأقل أن يكون 3 حروف"
                                   errorMax="يجب ألا يزيد عن 12 حرف" name="name"  errorEmpty="@lang('student.enter_fullname')"

                                   autocomplete="off"  type="text"  >
                            <i class="fal fa-user-circle"></i>
                        </div>
                        @error('name')
                        <span class="text-danger">{{ $message}}</span>
                        @enderror
                    </div>
                    <div>
                        <label for="email">
                            @lang('student.email')
                        </label>
                        <div class="divInput">
                            <input requiredX="yes" name="email"
                                   errorEmpty="@lang('student.enter_correct_email')"
                                   autocomplete="off"  type="email"
                                   value="{{old('email')}}" required>
                            <i class="fal fa-envelope"></i>
                        </div>
                        @error('email')
                        <span class="text-danger">{{ $message}}</span>
                        @enderror
                    </div>
                    <div>
                        <label for="phone2">
                            @lang('student.mobile')
                        </label>
                        <div class="divInput">
                            <input class="input" requiredX="yes" min="10" max="9999999999999" required
                                   errorMax="@lang('student.enter_correct_mobile')" error05="@lang('student.enter_correct_mobile')"
                                   errorEmpty="@lang('student.enter_correct_mobile')" errorMin="@lang('student.enter_correct_mobile')"
                                   autocomplete="off" name="mobile" id="teacher_mobile" type="number" value="{{old('mobile')}}">
                            <i class="fal fa-phone"></i>
                        </div>
                        @error('mobile')
                        <span class="text-danger">{{ $message}}</span>
                        @enderror
                    </div>
                    <div>
                        <label for="password">
                            @lang('student.password')
                        </label>

                        <div class="divInput">
                            <input requiredX="yes"
                                   min="8"   autocomplete="off" name="password"  errorEmpty=" @lang('student.enter_password')"  errorMin="@lang('student.min_password')" type="password" value="{{old('password')}}">
                            <i class="fal fa-lock-alt"></i>
                        </div>
                        @error('password')
                        <span class="text-danger">{{ $message}}</span>
                        @enderror
                    </div>
                    <div class="divInput">
                        <div class="s agree">
                            <input requiredX="yes"  id="agree" name="accept_use_terms"
                                   hidden type="checkbox"  value="{{old('accept_use_terms' ,  1 )}}">
                            <label class="colorText" for="agree1">
                                @lang('student.agree_to') <a href="{{url('/terms')}}">&nbsp;@lang('main.use_terms')</a>
                            </label>
                        </div>
                        @error('accept_use_terms')
                        <span class="text-danger">{{ $message}}</span>
                        @enderror
                    </div>
                    <div class="ok">
                        <button  type="submit" >
                            @lang('student.join')
                        </button>
                    </div>
                    <div class="login">
                        <p>
                            @lang('student.have_account')

                            <a href="javascript:void(0)" class="linkLogin">
                                @lang('student.sign_in')
                            </a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="popUp popUpFromTeacher popUpAddSubject">
        <div>
            <div class="headerPopUp">
                <i class="fal fa-user-circle"></i>
                @lang('teacher.add_topic_you_want')
                <span class="closeX">
                    <i class="fal fa-times"></i>
                </span>
            </div>
            <div class="bodyPopUp">
                {{--                <form class="formAddSubject" numCorrect="0" action="">--}}
                {{--                    <div class="inputSub">--}}
                {{--                        <label for="title">--}}
                {{--                            @lang('teacher.write_topic_here')--}}
                {{--                        </label>--}}
                {{--                        <div class="divInput">--}}
                {{--                            <input id="title" type="text" value="">--}}
                {{--                            <i class="fal fa-user-circle"></i>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    <div class="addAnotherInputSub">--}}
                {{--                        <i style="position: static!important;" class="fal fa-plus"></i>--}}
                {{--                        @lang('teacher.add_another_topic')--}}
                {{--                    </div>--}}
                {{--                    <p class="note">--}}
                {{--                        @lang('teacher.topic_will_add_after_accept')--}}
                {{--                    </p>--}}
                {{--                    <div class="ok">--}}
                {{--                        <button type="button" class="closeX">--}}
                {{--                            @lang('teacher.cancel')--}}
                {{--                        </button>--}}
                {{--                        <button class="submit" type="button">--}}
                {{--                            @lang('teacher.add')--}}
                {{--                        </button>--}}
                {{--                    </div>--}}
                {{--                </form>--}}

                <form action="{{route('teacher.addNewTopic')}}" class="formAddSubject" numCorrect="0" method="post">
                   @csrf
                    <div class="inputSub">
                        <label for="title">
                            @lang('teacher.write_topic_here')
                        </label>
                        <div class="divInput">
                            <input id="title" name="topic[]" type="text" value="">
                            <i class="fal fa-user-circle"></i>
                            <a class="Delete" href="javascript:void(0)">
                                <i class="far fa-minus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="addAnotherInputSub">
                        <i style="position: static!important;" class="fal fa-plus"></i>
                        @lang('teacher.add_another_topic')
                    </div>
                    <p class="note">
                        @lang('teacher.topic_will_add_after_accept')
                    </p>
                    <div class="ok">
                        <button type="button" class="closeX">
                            @lang('teacher.cancel')
                        </button>
                        <button class="submit" type="submit">
                            @lang('teacher.add')
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stack('modals')

@livewireScripts
<script src="{{ asset('js/app.js') }}" ></script>
<script>
    {{--let CURRENT_USER_ID = {{\Illuminate\Support\Facades\Auth::id()}};--}}
    // let BASE_URL = "http://127.0.0.1:8000/api/";
    {{--let PUSHER_KEY = '{{ env('PUSHER_APP_KEY') }}';--}}
</script>

    <script type="application/javascript"
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
 <script src="https://js.pusher.com/7.0/pusher.min.js"></script>

    <script type="module">
        import Vue from 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.esm.browser.js'
    </script>
    <script type="application/javascript" src="{{asset('assets/website/js/jquery.min.js')}}"></script>
<script type="application/javascript" src="{{asset('assets/website/js/popper.min.js')}}"></script>
<script type="application/javascript" src="{{asset('assets/website/js/bootstrap.min.js')}}"></script>
{{--<script src="/assets/website/js/main_teacher.js"></script>--}}
<script src="{{url('assets/website/js/bootstrap-slider.js')}}"></script>

<script type="application/javascript" src="{{asset('assets/website/js/select2.min.js')}}"></script>

<script type="application/javascript" src="{{asset('assets/website/js/slick.js')}}"></script>
<script type="application/javascript" src="{{asset('assets/website/js/main2.js')}}"></script>
<script type="application/javascript">
    $(document).on("click", ".addRateTeacher a", function () {
        $(".popUpRateTeacher").addClass("show")
    })

    $(document).on("click", ".popUpRateTeacher .closeX", function () {
        $(".popUpRateTeacher").removeClass("show")
    })

    $(document).mousedown(function (e) {
        var container = $(".popUpRateTeacher>div");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".popUpRateTeacher").removeClass("show");
        }
    });

    $(document).on("click", ".btnJoin", function () {
        $(".popJoinX").addClass("show")
        $(".login").removeClass("show")
    })

    $(document).on("click", ".popJoinX .closeX", function () {
        $(".popJoinX").removeClass("show")
    })

    $(document).on("click", ".popJoinX .joinAsStudent", function () {
        $(".popJoinX").removeClass("show")
        $(".popUpJoinAsTeacher.popUpFromVisitor1").addClass("show");
    })
    $(document).on("click", ".linkLogin", function () {
        $(".popUpJoinAsTeacher.popUpFromVisitor1").removeClass("show");
    })

    $(document).mousedown(function (e) {
        var container = $(".popJoinX>div");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".popJoinX").removeClass("show");
        }
    });

    $(document).on("click", ".btnJoin", function () {
        $(".popJoinX").addClass("show")
        $(".login").removeClass("show")
    })

    $(document).on("click", ".popJoinX .closeX", function () {
        $(".popJoinX").removeClass("show")
    })

    $(document).on("click", ".popJoinX .joinAsStudent", function () {
        $(".popJoinX").removeClass("show")
        $(".popUpJoinAsTeacher.popUpFromVisitor1").addClass("show");
    })

    $(document).mousedown(function (e) {
        var container = $(".popJoinX>div");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".popJoinX").removeClass("show");
        }
    });

</script>

<script>

</script>

@yield('footer')



</body>
</html>

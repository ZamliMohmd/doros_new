{{--@if ($errors->any())--}}
{{--    <div class="alert alert-danger">--}}
{{--        <ul>--}}
{{--            @foreach ($errors->all() as $error)--}}
{{--                <li>{{ $error }}</li>--}}
{{--            @endforeach--}}
{{--        </ul>--}}
{{--        @if ($errors->has('email'))--}}
{{--        @endif--}}
{{--    </div>--}}
{{--@endif--}}

@if(Session::has('success'))
    <div class="alert alert-success" style="background-color: #fdb228">
       <span style="font-weight: bold;color: black"> {{ Session::get('success') }}</span>
        @php
            Session::forget('success');
        @endphp
    </div>
@endif


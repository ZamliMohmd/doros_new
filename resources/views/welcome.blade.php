@extends('layouts.app')

@section('header')
    <style>
        .box{

            margin:0 auto;
            position: relative;
        }
        li {
            display: block;
            transition-duration: 0.5s;
        }

        li:hover {
            cursor: pointer;
            background: #F58112;
        }

        ul li ul {
            visibility: hidden;
            opacity: 0;
            position: absolute;
            transition: all 0.5s ease;
            margin-top: 1rem;
            left: 0;
            display: none;
        }

        ul li:hover > ul,
        ul li ul:hover {
            visibility: visible;
            opacity: 1;
            display: block;
        }

        ul li ul li {
            clear: both;
            width: 100%;
        }
        .p_right {
            direction: rtl;
            text-align: right;
            font-weight: bold;
        }

        .univercites_section{
            width: 172px;
            height: 33px;
        }
    </style>
@endsection
@section('content')
    <section class="slider_section">

        <div class="slick slider">
            <img src="{{url('assets/website/img/5.png')}}" alt="">
            {{--
             <img src="{{url('assets/website/img/3.png')}}" alt="">
             <img src="{{url('assets/website/img/3.png')}}" alt="">
             <img src="{{url('assets/website/img/3.png')}}" alt="">
             <img src="{{url('assets/website/img/3.png')}}" alt="">
            --}}
        </div>

        <div class="social_media">

            <div class="position-relative d-inline-block right">
                <div><span></span></div>
                <a target="_blank" href="#">
                    <img src="{{url('/assets/website/img/Instagram.svg')}}" alt="Instagram">
                </a>
            </div>
            <div class="position-relative d-inline-block left">
                <a target="_blank" href="#">
                    <img src="{{url('/assets/website/img/facebook.svg')}}" alt="facebook">
                </a>
                <div><span></span></div>
            </div>

        </div>

        <div class="text-center search_div">
            <div class="black_search_div">
                <div class="search_text " style="padding-left: 80px;">
                    <span class="ml-2">@lang('main.slider_text1')</span>
                    <span class="">@lang('main.slider_text2')</span>
                </div>
                <div class="search_form text-center">
                    {{--                       <form action="">--}}
                    <div class="row">
                        <div class="col-md-5 p-2">

                                <div class="form-group search_subtopic" style="width: 100%">
                                    <input type="text" name="subtopic" id="subtopic" class="form-control input-lg" autocomplete="off" value="" placeholder="@lang('main.search_city')" />
                                    <div id="subtopicList" style="position: absolute;width: 100%">
                                    </div>
                                    {{csrf_field()}}
                                </div>

                        </div>
                        <div class="col-md-5 p-2">
                            <div class="form-group search_cities" style=" width: 100%">
                                <input type="text" name="cities" id="cities" class="form-control input-lg" autocomplete="off" placeholder="@lang('main.search_topic')" />
                                <div id="citiesList" style="position: absolute;width: 100%">
                                </div>
                                {{csrf_field()}}
                            </div>
                        </div>
                        <div class="col-md-2 p-2">
                            <a href="{{url('/search')}}"> <button type="submit" class="btn btn-default">  @lang('main.search_button')</button></a>
                        </div>
                    </div>
                    {{--                       </form>--}}
                </div>
            </div>
        </div>


    </section>



    <section class="numbers_section">
        <div class="container number">
            <div class="row">
                <div class="numbers_content col-md-3">
                    <span class="d-block fr_numbers timer">{{$teachers}}</span>
                    <div><span></span></div>
                    <span class="d-block title">@lang('main.teachers')</span>
                </div>
                <div class="numbers_content col-md-3">
                    <span class="d-block fr_numbers timer">{{$subtopics}}</span>
                    <div><span></span></div>
                    <span class="d-block title">@lang('main.topics')</span>
                </div>
                <div class="numbers_content col-md-3">
                    <span class="d-block fr_numbers timer">{{$universities}}</span>
                    <div><span></span></div>
                    <span class="d-block title">@lang('main.universities')</span>
                </div>
                <div class="numbers_content col-md-3">
                    <span class="d-block fr_numbers timer">{{$cities}}</span>
                    <div><span></span></div>
                    <span class="d-block title">@lang('main.cities')</span>
                </div>

            </div>
        </div>
    </section>





    <div id="have_question_button" class="have_question">
        <div class="head">
            <span>
                <h6 style="font-size: 13px;">@lang('main.question1')@lang('main.question2')</h6>

            </span>
            <span class="i">
                <i class="fas fa-angle-up"></i>
            </span>
        </div>
{{--        <div class="formHaveQuestion">--}}

{{--            <form  action="#" class="formPublic">--}}

{{--                <div>--}}
{{--                    @if(session()->has('success'))--}}
{{--                        <div id="alert" class="alert alert-success">--}}
{{--                            {{session('success')}}--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                    <div>--}}
{{--                        <label for="email">--}}
{{--                            @lang('main.email')--}}
{{--                        </label>--}}
{{--                        <div class="input">--}}
{{--                            <input type="email"  requiredX="yes" wire:model="email" placeholder="    @lang('main.enter_email')" required style="    padding-right: 10px;">--}}
{{--                            <i class="fal fa-envelope"></i>--}}
{{--                        </div>--}}
{{--                        @error('email')--}}
{{--                        <span class="text-danger">{{ $message}}</span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        <textarea  requiredX="yes" wire:model="question"  placeholder=" @lang('main.enter_question')" rows="5" required></textarea>--}}
{{--                    </div>--}}
{{--                    @error('question')--}}
{{--                    <span class="text-danger">{{ $message}}</span>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--                <div>--}}
{{--                    <p>--}}
{{--                        @lang('main.will_reply_quickly')--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--                <div>--}}
{{--                    <button wire:click.prevent="addQuestion">--}}
{{--                        @lang('main.send')--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--            </form>--}}
{{--        </div>--}}
        @livewire('question')
    </div>

    <section class="Basic_topics_section">
        <div class="container-fluid" >
            <div id="Basic_topics_slider" class="row">

                <div class="col Basic_topics">
                    <div class="row Basic_topics_row">
                        <div class="Basic_topics_content text-center col-md-12">
                            <h4>@lang('main.teacher')<span class="vip_word"> vip</span></h4>
                            <h5>@lang('main.univercies')</h5>
                            <div><span></span></div>
                        </div>
                        <div class="Basic_topics_bottoms col-12">
                            <div class="row Basic_topics_bottoms_row">
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="">
                                        <span class="univercites_section">
                                            <span class="icon_btn_svg">
                                                   <img src="{{url('/assets/website/img/school.svg')}}" alt="">
                                            </span>
                                          جامعه النجاح

                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                   <img src="{{url('/assets/website/img/school.svg')}}" alt="">
                                            </span>
                                            @lang('main.tikhnion')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                               <img src="{{url('/assets/website/img/school.svg')}}" alt="">
                                            </span>
                                            @lang('main.heifa')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                   <img src="{{url('/assets/website/img/school.svg')}}" alt="">
                                            </span>
                                            @lang('main.qudes')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                               <img src="{{url('/assets/website/img/school.svg')}}" alt="">
                                            </span>
                                              @lang('main.ber_subaa')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center" >
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                     <img src="{{url('/assets/website/img/school.svg')}}" alt="">

                                            </span>
                                           الجامعه الاميركيه جنين
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col Basic_topics">
                    <div class="row Basic_topics_row">
                        <div class="Basic_topics_content text-center col-md-12">
                            <h4>  @lang('main.teacher')<span class="vip_word"> vip</span></h4>
                            <h5>  @lang('main.other_topics')</h5>
                            <div><span></span></div>
                        </div>
                        <div class="Basic_topics_bottoms col-12">
                            <div class="row Basic_topics_bottoms_row">
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                               @lang('main.turkiesh')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                            @lang('main.driven_learning')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                             @lang('main.personal_sport')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                         @lang('main.quran_rules')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                              @lang('main.design_program')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/book2.svg')}}" alt="">
                                            </span>
                                             @lang('main.bsekhometry')
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col Basic_topics">
                    <div class="row Basic_topics_row">
                        <div class="Basic_topics_content text-center col-md-12">
                            <h4>  @lang('main.teacher')<span class="vip_word"> vip</span></h4>
                            <h5>  @lang('main.main_cities')</h5>
                            <div><span></span></div>
                        </div>
                        <div class="Basic_topics_bottoms col-12">
                            <div class="row Basic_topics_bottoms_row">
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                 <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                            @lang('main.om_alfahem')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                             @lang('main.nasera')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                               <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                             @lang('main.sekhneen')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                 <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                             @lang('main.teba')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                 <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                              @lang('main.west_backa')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="{{url('/assets/website/img/globe-white.svg')}}" alt="">
                                            </span>
                                          @lang('main.shafa_amro')
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col Basic_topics">
                    <div class="row Basic_topics_row">
                        <div class="Basic_topics_content text-center col-md-12">
                            <h4>
                                @lang('main.teacher')
                                <span class="vip_word">
                                    vip
                                </span>
                            </h4>
                            <h5>
                                @lang('main.main_topics')
                            </h5>
                            <div>
                                <span>
                                </span>
                            </div>
                        </div>
                        <div class="Basic_topics_bottoms col-12">
                            <div class="row Basic_topics_bottoms_row">
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                             @lang('main.math')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                                    @lang('main.biologically')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                                    @lang('main.chemistry')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                                    @lang('main.accounting')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                             @lang('main.computer')
                                        </span>
                                    </a>
                                </div>
                                <div class="col-lg-6 px-1 text-center">
                                    <a href="">
                                        <span  class="univercites_section">
                                            <span class="icon_btn_svg">
                                                <img src="assets/website/img/book2.svg" alt="">
                                            </span>
                                            @lang('main.english')
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection

@section('footer')
    <script>
        setTimeout(function() {
            $('#alert').fadeOut('fast');
        }, 30000);
    </script>

    <script>
        $(document).ready(function(){

            $('#subtopic').keyup(function(){
                var query = $(this).val();
                if(query != '')
                {
                    //  var _token = $('input[name="_token"]').val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url:"{{ route('autocomplete.fetchSubtopic') }}",
                        method:"POST",
                        data:{query:query},
                        success:function(data){
                            $('#subtopicList').fadeIn();
                            $('#subtopicList').html(data);
                        }
                    });
                }
            });

            $(document).on('click', 'li', function(){
                $($($(this).parents(".search_subtopic")).find("input")).val($(this).text());
                $('#subtopicList').fadeOut();
                // console.log($(this))

            });

        });

    </script>
    <script>
        $(document).ready(function(){
            $('#cities').keyup(function(){
                var query = $(this).val();
                if(query != '')
                {
                    // var _token = $('input[name="_token"]').val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url:"{{ route('autocomplete.fetchCities') }}",
                        method:"POST",
                        data:{query:query},
                        success:function(data){
                            $('#citiesList').fadeIn();
                            $('#citiesList').html(data);
                        }
                    });
                }
            });

            $(document).on('click', 'li', function(){
                $($($(this).parents(".search_cities")).find("input")).val($(this).text());
                $('#citiesList').fadeOut();
            });

        });
    </script>
@endsection

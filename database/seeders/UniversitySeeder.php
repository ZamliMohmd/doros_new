<?php

namespace Database\Seeders;

use App\Models\University;
use Illuminate\Database\Seeder;

class UniversitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $university = new University();
        $university->name_ar = 'معهد التخنيون';
        $university->name_he = 'המרכז האקדמי כרמל';
        $university->user_id = null;
        $university->save();

        $university = new University();
        $university->name_ar = 'المؤسسة الأكاديمية الناصرة';
        $university->name_he = 'מכללת נצרת';
        $university->user_id = null;
        $university->save();

        $university = new University();
        $university->name_ar = 'الكلية الأكاديمية للهندسة';
        $university->name_he = 'מכללת אורט הרמלין נתניה';
        $university->user_id = null;
        $university->save();

        $university = new University();
        $university->name_ar = 'الجامعه الاميركيه جنين';
        $university->name_he = '';
        $university->user_id = null;
        $university->save();

        $university = new University();
        $university->name_ar = 'جامعه النجاح';
        $university->name_he = '';
        $university->user_id = null;
        $university->save();

        $university = new University();
        $university->name_ar = 'جامعه القدس';
        $university->name_he = '';
        $university->user_id = null;
        $university->save();

        $university = new University();
        $university->name_ar = 'جامعة حيفا';
        $university->name_he = 'אוניברסיטה/מכללה בחו"ל';
        $university->user_id = null;
        $university->save();

        $university = new University();
        $university->name_ar = 'جامعة بئر السبع';
        $university->name_he = 'אוניברסיטה/מכללה בחו"ל';
        $university->user_id = null;
        $university->save();

        $university = new University();
        $university->name_ar = 'اخر';
        $university->name_he = 'אחר';
        $university->user_id = null;
        $university->save();


    }
}

<?php

namespace Database\Seeders;

use App\Models\Day;
use Illuminate\Database\Seeder;

class DaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $degree = new Day();
        $degree->name_ar = 'الاحد';
        $degree->name_he = '';
        $degree->slug = 'sun';
        $degree->save();

        $degree = new Day();
        $degree->name_ar = 'الاثنين';
        $degree->name_he = '';
        $degree->slug = 'mon';
        $degree->save();

        $degree = new Day();
        $degree->name_ar = 'الثلاثاء';
        $degree->name_he = '';
        $degree->slug = 'tues';
        $degree->save();

        $degree = new Day();
        $degree->name_ar = 'الاربعاء';
        $degree->name_he = '';
        $degree->slug = 'wed';
        $degree->save();

        $degree = new Day();
        $degree->name_ar = 'الخميس';
        $degree->name_he = '';
        $degree->slug = 'thr';
        $degree->save();

        $degree = new Day();
        $degree->name_ar = 'الجمعة';
        $degree->name_he = '';
        $degree->slug = 'fri';
        $degree->save();

        $degree = new Day();
        $degree->name_ar = 'السبت';
        $degree->name_he = '';
        $degree->slug = 'sat';
        $degree->save();
    }
}

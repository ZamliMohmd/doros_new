<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = new City();
        $city->name_ar = 'ابطن';
        $city->name_he = 'אבטן';
        $city->user_id = null;
        $city->save();

        $city = new City();
        $city->name_ar = 'ابو سنان';
        $city->name_he = 'אבו סנאן';
        $city->user_id = null;
        $city->save();



        $city = new City();
        $city->name_ar = 'ابو غوش';
        $city->name_he = 'אבו גוש';
        $city->user_id = null;
        $city->save();

        $city = new City();
        $city->name_ar = 'اكسال';
        $city->name_he = 'אכסאל';
        $city->user_id = null;
        $city->save();

        $city = new City();
        $city->name_ar = 'البطوف';
        $city->name_he = 'אלבטוף';
        $city->user_id = null;
        $city->save();

        $city = new City();
        $city->name_ar = 'البعنة';
        $city->name_he = 'בענה';
        $city->user_id = null;
        $city->save();

        $city = new City();
        $city->name_ar = 'شفا عمرو';
        $city->name_he = '';
        $city->user_id = null;
        $city->save();


        $city = new City();
        $city->name_ar = 'باقا الغربية';
        $city->name_he = '';
        $city->user_id = null;
        $city->save();


        $city = new City();
        $city->name_ar = 'الطيبة';
        $city->name_he = '';
        $city->user_id = null;
        $city->save();


        $city = new City();
        $city->name_ar = 'سخنين';
        $city->name_he = '';
        $city->user_id = null;
        $city->save();


        $city = new City();
        $city->name_ar = 'الناصرة';
        $city->name_he = '';
        $city->user_id = null;
        $city->save();


        $city = new City();
        $city->name_ar = 'أم الفحم';
        $city->name_he = '';
        $city->user_id = null;
        $city->save();




    }


}

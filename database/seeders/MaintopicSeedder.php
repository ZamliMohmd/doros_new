<?php

namespace Database\Seeders;

use App\Models\Maintopic;
use Illuminate\Database\Seeder;

class MaintopicSeedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $maintopic = new Maintopic();
        $maintopic->name_ar = 'GMAT';
        $maintopic->name_he = 'GMAT';
        $maintopic->user_id = null;
        $maintopic->save();

        $maintopic = new Maintopic();
        $maintopic->name_ar = 'اللغة اليابانية';
        $maintopic->name_he = 'יפנית';
        $maintopic->user_id = null;
        $maintopic->save();


        $maintopic = new Maintopic();
        $maintopic->name_ar = 'صناعة';
        $maintopic->name_he = 'יצירה';
        $maintopic->user_id = null;
        $maintopic->save();


        $maintopic = new Maintopic();
        $maintopic->name_ar = 'رياضة شخصية';
        $maintopic->name_he = 'ספורט אישי';
        $maintopic->user_id = null;
        $maintopic->save();

        $maintopic = new Maintopic();
        $maintopic->name_ar = 'كيمياء';
        $maintopic->name_he = 'כימיה';
        $maintopic->user_id = null;
        $maintopic->save();

        $maintopic = new Maintopic();
        $maintopic->name_ar = 'اقتصاد';
        $maintopic->name_he = 'כלכלה';
        $maintopic->user_id = null;
        $maintopic->save();

        $maintopic = new Maintopic();
        $maintopic->name_ar = 'العلوم الصحية';
        $maintopic->name_he = 'מדעי הבריאות';
        $maintopic->user_id = null;
        $maintopic->save();

        $maintopic = new Maintopic();
        $maintopic->name_ar = 'علم وبرمجة الحاسوب';
        $maintopic->name_he = 'מדעי המחשב תכנות';
        $maintopic->user_id = null;
        $maintopic->save();

        $maintopic = new Maintopic();
        $maintopic->name_ar = 'ادارة اعمال';
        $maintopic->name_he = 'מנהל עסקים';
        $maintopic->user_id = null;
        $maintopic->save();


        $maintopic = new Maintopic();
        $maintopic->name_ar = 'حقوق';
        $maintopic->name_he = 'משפטים';
        $maintopic->user_id = null;
        $maintopic->save();


        $maintopic = new Maintopic();
        $maintopic->name_ar = 'رياضيات';
        $maintopic->name_he = 'מתמטיקה';
        $maintopic->user_id = null;
        $maintopic->save();


        $maintopic = new Maintopic();
        $maintopic->name_ar = 'علم الاجتماع';
        $maintopic->name_he = 'סוציולוגיה';
        $maintopic->user_id = null;
        $maintopic->save();


        $maintopic = new Maintopic();
        $maintopic->name_ar = 'الاحصاء';
        $maintopic->name_he = 'סטטיסטיקה';
        $maintopic->user_id = null;
        $maintopic->save();




    }
}

<?php

namespace Database\Seeders;

use App\Models\Place;
use Illuminate\Database\Seeder;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $place = new Place();
        $place->name_ar = 'أونلاين';
        $place->name_he = '';
        $place->user_id = null;
        $place->save();

        $place = new Place();
        $place->name_ar = 'بيت الطالب';
        $place->name_he = '';
        $place->user_id = null;
        $place->save();


        $place = new Place();
        $place->name_ar = 'بيت المعلم';
        $place->name_he = '';
        $place->user_id = null;
        $place->save();


        $place = new Place();
        $place->name_ar = 'مكتبة/أخرى';
        $place->name_he = '';
        $place->user_id = null;
        $place->save();
    }
}

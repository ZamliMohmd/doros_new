<?php

namespace Database\Seeders;

use App\Models\Experience;
use Illuminate\Database\Seeder;

class ExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $experience = new Experience();
        $experience->name_ar = 'تعليم طلاب مدارس';
        $experience->name_he = 'תלמידי בית ספר';
        $experience->user_id = null;
        $experience->save();

        $experience = new Experience();
        $experience->name_ar = 'تحضير لبسيخومتري';
        $experience->name_he = 'הכנה לפסיכומטרי';
        $experience->user_id = null;
        $experience->save();

        $experience = new Experience();
        $experience->name_ar = 'تحضير لبجروت';
        $experience->name_he = 'הכנה לבגרויות';
        $experience->user_id = null;
        $experience->save();

        $experience = new Experience();
        $experience->name_ar = 'تحضير لإمتحانات';
        $experience->name_he = 'הכנה לבחינות';
        $experience->user_id = null;
        $experience->save();


        $experience = new Experience();
        $experience->name_ar = 'محاضر (מרצה)';
        $experience->name_he = 'מרצה';
        $experience->user_id = null;
        $experience->save();

        $experience = new Experience();
        $experience->name_ar = 'معيد (מתרגל)';
        $experience->name_he = 'מתרגל';
        $experience->user_id = null;
        $experience->save();

        $experience = new Experience();
        $experience->name_ar = 'لا يوجد خبرة';
        $experience->name_he = 'אין ניסיון';
        $experience->user_id = null;
        $experience->save();

    }
}

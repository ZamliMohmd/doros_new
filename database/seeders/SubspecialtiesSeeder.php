<?php

namespace Database\Seeders;

use App\Models\Subspecialties;
use Illuminate\Database\Seeder;

class SubspecialtiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /******************************* الهندسة **************************************/
        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'الهندسة الطبية الحيوية';
        $subspecialties->name_he = 'הנדסה ביורפואית';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();

        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'الهندسة الكيميائية';
        $subspecialties->name_he = 'הנדסה כימית';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();

        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'الهندسة البيوتكنولوجية';
        $subspecialties->name_he = 'הנדסת ביוטכנולוגיה';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();

        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'هندسة البناء';
        $subspecialties->name_he = 'הנדסת בניין';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();


        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'هندسة المواد';
        $subspecialties->name_he = 'הנדסת חומרים';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();


        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'الهندسة الكهربائية';
        $subspecialties->name_he = 'הנדסת חשמל';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();


        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'هندسة الحاسوب';
        $subspecialties->name_he = 'הנדסת מחשבים';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();


        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'الهندسة الميكانيكية';
        $subspecialties->name_he = 'הנדסת מכונות';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();


        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'هندسة نظم البيانات';
        $subspecialties->name_he = 'הנדסת מערכות מידע';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();


        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'هندسة برامج';
        $subspecialties->name_he = 'הנדסת תוכנה';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();

        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'بيولوجيا';
        $subspecialties->name_he = 'הנדסת תוכנה';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();

        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'رياضيات';
        $subspecialties->name_he = 'הנדסת תוכנה';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();

        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'حسابات';
        $subspecialties->name_he = 'הנדסת תוכנה';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();

        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'كيمياء';
        $subspecialties->name_he = 'הנדסת תוכנה';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();

        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'علوم الحاسوب';
        $subspecialties->name_he = 'הנדסת תוכנה';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();

        $subspecialties = new Subspecialties();
        $subspecialties->name_ar = 'اللغة الإنجليزية';
        $subspecialties->name_he = '';
        $subspecialties->mainspecialtie_id = 1;
        $subspecialties->user_id = null;
        $subspecialties->save();



    }
}

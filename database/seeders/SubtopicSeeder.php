<?php

namespace Database\Seeders;

use App\Models\Subtopic;
use Illuminate\Database\Seeder;

class SubtopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*********************************  GMAT  **************************************/
        $subtopic = new Subtopic();
        $subtopic->name_ar = 'كمي';
        $subtopic->name_he = 'כמותי';
        $subtopic->maintopic_id = 1;
        $subtopic->user_id = null;
        $subtopic->save();

        $subtopic = new Subtopic();
        $subtopic->name_ar = 'كلامي';
        $subtopic->name_he = 'מילולי';
        $subtopic->maintopic_id = 1;
        $subtopic->user_id = null;
        $subtopic->save();

        $subtopic = new Subtopic();
        $subtopic->name_ar = 'انشاء';
        $subtopic->name_he = 'חיבור';
        $subtopic->maintopic_id = 1;
        $subtopic->user_id = null;
        $subtopic->save();


        /*********************************  هندسة معمارية  **************************************/

        $subtopic = new Subtopic();
        $subtopic->name_ar = 'اكاديمية';
        $subtopic->name_he = 'אקדמיה';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();

        $subtopic = new Subtopic();
        $subtopic->name_ar = 'REVIT';
        $subtopic->name_he = 'רוויט REVIT';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();

        $subtopic = new Subtopic();
        $subtopic->name_ar = 'TURBOCAD';
        $subtopic->name_he = 'טורבוקאד TURBOCAD';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();

        $subtopic = new Subtopic();
        $subtopic->name_ar = 'ARCHICAD';
        $subtopic->name_he = 'ארכיקאד ARCHICAD';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();

        $subtopic = new Subtopic();
        $subtopic->name_ar = 'WINDOOR';
        $subtopic->name_he = 'ווינדור WINDOOR';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();

        $subtopic = new Subtopic();
        $subtopic->name_ar = 'الهندسة النظرية';
        $subtopic->name_he = 'הנדסה תיאורית';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();

        $subtopic = new Subtopic();
        $subtopic->name_ar = 'التواصل الجرافيكي';
        $subtopic->name_he = 'תקשורת גרפית';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();

        $subtopic = new Subtopic();
        $subtopic->name_ar = 'تعليم السياقة';
        $subtopic->name_he = 'תקשורת גרפית';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();


        $subtopic = new Subtopic();
        $subtopic->name_ar = 'اللغة التركية';
        $subtopic->name_he = 'תקשורת גרפית';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();


        $subtopic = new Subtopic();
        $subtopic->name_ar = 'أحكام تجويد القرأن';
        $subtopic->name_he = 'תקשורת גרפית';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();

        $subtopic = new Subtopic();
        $subtopic->name_ar = 'الرياضة الشخصية';
        $subtopic->name_he = 'תקשורת גרפית';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();


        $subtopic = new Subtopic();
        $subtopic->name_ar = 'برامج تصميم';
        $subtopic->name_he = 'תקשורת גרפית';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();


        $subtopic = new Subtopic();
        $subtopic->name_ar = 'بسيخومتري';
        $subtopic->name_he = 'תקשורת גרפית';
        $subtopic->maintopic_id = 2;
        $subtopic->user_id = null;
        $subtopic->save();


    }
}
